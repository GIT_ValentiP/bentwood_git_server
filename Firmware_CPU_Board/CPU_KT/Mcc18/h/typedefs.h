/*********************************************************************
 *
 *                Microchip USB C18 Firmware Version 1.0
 *
 *********************************************************************
 * FileName:        typedefs.h
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18
 * Compiler:        C18 2.30.01+
 * Company:         Microchip Technology, Inc.
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Rawin Rojvanit       7/21/04     Original.
 * Dario Greggio				1/26/06     Aggiunte varie 
 * Dario Greggio			10/26/06     Altre aggiunte 
 * Dario Greggio			7/03/07      sync con matteo
 ********************************************************************/

#ifndef TYPEDEFS_H
#define TYPEDEFS_H

typedef unsigned char   byte;           // 8-bit
typedef unsigned int    word;           // 16-bit
typedef unsigned long   dword;          // 32-bit
typedef unsigned long long	qword;

// da cc2420...
typedef unsigned char		UINT8;
typedef unsigned short	UINT16;
typedef unsigned long		UINT32;
typedef unsigned long long	UINT64;

typedef signed char			INT8;
typedef signed short		INT16;
typedef signed long			INT32;
typedef signed long long	INT64;

#ifdef __18F1320 		//... se ci sono pi� di 256 byte RAM, NO!
typedef unsigned char SHORTPTR;
// servirebbe un byte near * di soli 8 bit, per indirizzare RAM... su 18F1320 c'� un solo banco!
#else 
typedef unsigned int SHORTPTR;
#endif

typedef union _BYTE {
  byte _byte;
  struct {
    unsigned b0:1;
    unsigned b1:1;
    unsigned b2:1;
    unsigned b3:1;
    unsigned b4:1;
    unsigned b5:1;
    unsigned b6:1;
    unsigned b7:1;
    };
	} BYTE;

typedef union _WORD {
  word _word;
  struct {
    byte byte0;
    byte byte1;
    };
  struct {
    BYTE Byte0;
    BYTE Byte1;
    };
  struct {
    BYTE LowB;
    BYTE HighB;
    };
  struct {
    byte v[2];
    };
	} WORD;
#define LSB(a)      ((a).v[0])
#define MSB(a)      ((a).v[1])

typedef union _DWORD {
  dword _dword;
  struct {
    byte byte0;
    byte byte1;
    byte byte2;
    byte byte3;
    };
  struct {
    word word0;
    word word1;
    };
  struct {
    BYTE Byte0;
    BYTE Byte1;
    BYTE Byte2;
    BYTE Byte3;
    };
  struct {
    WORD Word0;
    WORD Word1;
    };
  struct {
    byte v[4];
    };
	} DWORD;
#define LOWER_LSB(a)    ((a).v[0])
#define LOWER_MSB(a)    ((a).v[1])
#define UPPER_LSB(a)    ((a).v[2])
#define UPPER_MSB(a)    ((a).v[3])

typedef void(*pFunc)(void);

typedef union _POINTER {
  struct {
	  byte bLow;
	  byte bHigh;
	  //byte bUpper;
    };
  word _word;                         // bLow & bHigh
    
  //pFunc _pFunc;                       // Usage: ptr.pFunc(); Init: ptr.pFunc = &<Function>;

  byte* bRam;                         // Ram byte pointer: 2 bytes pointer pointing
                                        // to 1 byte of data
  word* wRam;                         // Ram word poitner: 2 bytes poitner pointing
                                        // to 2 bytes of data

  rom byte* bRom;                     // Size depends on compiler setting
  rom word* wRom;
  //rom near byte* nbRom;               // Near = 2 bytes pointer
  //rom near word* nwRom;
  //rom far byte* fbRom;                // Far = 3 bytes pointer
  //rom far word* fwRom;
	} POINTER;

typedef enum _BOOL { FALSE = 0, TRUE } BOOL;

#define OK      TRUE
#define FAIL    FALSE




#define MAKEWORD(a, b)   ((word) (((byte) (a)) | ((word) ((byte) (b))) << 8)) 
#define MAKELONG(a, b)   ((unsigned long) (((word) (a)) | ((dword) ((word) (b))) << 16)) 
//#define HIBYTE(w)   ((byte) ((((word) (w)) >> 8) /* & 0xFF*/)) 
#define HIBYTE(w)   ((byte) (*((char *)&w+1)))		// molto meglio :)
#define HIWORD(l)   ((word) (((dword) (l) >> 16) & 0xFFFF)) 
#define LOBYTE(w)   ((byte) (w)) 
#define LOWORD(l)   ((word) (l)) 

#define MY_CLRWDT { /*ClrWdt();*/ OSCCONbits.IRCF0^=1; /*OSCCONbits.IRCF0=0;*/ }
// WDTCON=0;WDTCON=1; provare! da forum.microchip.com?296350 
#define MY_NOP { RCONbits.TO=0; /*PCLATU &= ~1;*/ /*PCL=PCL+0;*/	/*PCL+=2; main();*/ }

#define NaN(f) ( ((((BYTE *)&f)[3] & 0x7f) == 0x7f ) && (((BYTE *)&f)[2] & 0x80) )
// http://forum.microchip.com/fb.aspx?m=324031 - bixelps

#endif //TYPEDEFS_H


