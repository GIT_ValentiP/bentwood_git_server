/**
  Loads File

  @Company
    K-TRONIC

  @File Name
    loads.c

  @Summary
    This is the C file regarding the management of serial monitor

  @Description
    This C file provides APIs for the management of serial monitor.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC24EP512GU810
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/

#include "Driver_MV/pin_manager.h"
//#include "oc1.h"
#include "Driver_MV/tmr2.h"

#include <ctype.h>
#include <stdlib.h>
#include "sd-spi.h"
#include "MainDemo.h"
#include <math.h>


void CountdownLCD(unsigned short time, unsigned char maxtime_s, unsigned char refreshtime){
    
    //refresh time 20ms
    char timestring_result[10];
    char timestring_s[3]="00";
    char timestring_ms[3] ="00";
    unsigned short maxtime_ms = maxtime_s*1000;
    unsigned long diff_time_ms =0;//means--> time * minimuntime /10
    unsigned short time_ms =0;
    int time_s =0;
    
    
    diff_time_ms = maxtime_ms - (time*refreshtime); //get count down value
    time_ms = (diff_time_ms%1000)/10; 
    time_s = (int)(diff_time_ms/1000);
    
    sprintf(timestring_ms, "%d", time_ms);
    sprintf(timestring_s, "%d", time_s);
    
    if(time_s<1){
        strcpy(timestring_result, "00,");
        strcat(timestring_result, timestring_ms);
    }else if(time_s<10){
        strcpy(timestring_result, "0");
        strcat(timestring_result, timestring_s);
        strcat(timestring_result, ",");
        strcat(timestring_result, timestring_ms);
    }else{
        strcpy(timestring_result, timestring_s);
        strcat(timestring_result, ",");
        strcat(timestring_result, timestring_ms);
    }

    if(diff_time_ms> 0 ){
        LCDPutStringXY(10,10,timestring_result);
    }else{
        LCDPutStringXY(10,10,"00000");
    }
}

void CountIncLCD(unsigned short time, unsigned char refreshtime){
    
    //refresh time 20ms
    char timestring_result[10];
    char timestring_s[4]="000";
    char timestring_ms[3] ="00";
//    char temp[10] ="00";
    long diff_time_ms =0;//means--> time * minimuntime /10
    unsigned short time_ms =0;
    int time_s =0;
    
    
    diff_time_ms = (long)(time*refreshtime); //get count down value
    time_ms = (diff_time_ms%1000)/10;
    time_s = diff_time_ms/1000;
    
//    sprintf(temp, "%ld", diff_time_ms);
//    LCDPutStringXY(10,8,temp);
    
    sprintf(timestring_ms, "%d", time_ms);
    sprintf(timestring_s, "%d", time_s);

    

    if(time_s<1){
        strcpy(timestring_result, "000,");
        strcat(timestring_result, timestring_ms);
    }else if(time_s<10){
        strcpy(timestring_result, "00");
        strcat(timestring_result, timestring_s);
        strcat(timestring_result, ",");
        strcat(timestring_result, timestring_ms);
    }else if(time_s<100){
        strcpy(timestring_result, "0");
        strcat(timestring_result, timestring_s);
        strcat(timestring_result, ",");
        strcat(timestring_result, timestring_ms);
    }else{
        strcpy(timestring_result, timestring_s);
        strcat(timestring_result, ",");
        strcat(timestring_result, timestring_ms);
    }
    
    if(time_s>999){
         LCDPutStringXY(10,10,"999,99");
    }else{
        LCDPutStringXY(10,10,timestring_result);
    }
}