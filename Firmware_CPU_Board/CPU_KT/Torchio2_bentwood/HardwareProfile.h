/*********************************************************************
 *
 *	Hardware specific definitions
 *
 *********************************************************************
 * FileName:        HardwareProfile.h
 * Dependencies:    None
 * Processor:       PIC24, dsPIC, PIC32
 * Compiler:        Microchip C30 and C32
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (c) 2011 Microchip Technology Inc.  All rights 
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and 
 * distribute: 
 * (i)  the Software when embedded on a Microchip microcontroller or 
 *      digital signal controller product ("Device") which is 
 *      integrated into Licensee�s product; or
 * (ii) ONLY the Software driver source files ENC28J60.c and 
 *      ENC28J60.h ported to a non-Microchip device used in 
 *      conjunction with a Microchip ethernet controller for the 
 *      sole purpose of interfacing with the ethernet controller. 
 *
 * You should refer to the license agreement accompanying this 
 * Software for additional information regarding your rights and 
 * obligations.
 *
 *
 * Date	                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 10/03/06             Original, copied from Compiler.h
 * 06/25/09             dsPIC & PIC24H support 
 * 09/15/09             Added PIC24FJ256DA210 Development Board Support
 * 06/02/11             Added MPLAB X Support
 * 06/24/14							GD; 2018
 ********************************************************************/

#ifndef HARDWARE_PROFILE_H
#define HARDWARE_PROFILE_H

#include "Compiler.h"

#ifndef CFG_INCLUDE_MPLAB_X

#if defined (__PIC24F__) || defined(__dsPIC33F__) || defined(__PIC24H__) || defined (__dsPIC33E__) || defined (__PIC24E__) 

		/*********************************************************************
	     * Hardware Configuration for 
	     * Explorer 16
    	 * Graphics PicTail v3
	     * Display TFT-G240320LTSW-118W-E
	     ********************************************************************/

		#include "Configs/system_config_8PMP_WQVGAv1.h"		
		
#endif

#else
/*********************************************************************
 * Hardware Configuration for
 * Explorer 16 with a PIM
 * GFX version 3 
 * 8 or 16-bit PMP
 * QVGA or WQVGA
 ********************************************************************/
#if defined(CFG_INCLUDE_GFXv3_EX16_8PMP_QVGAv1)
    #include "Configs/HWP_GFXv3_EX16_8PMP_QVGAv1.h"
#elif defined(CFG_INCLUDE_GFXv3_EX16_16PMP_QVGAv1)
    #include "Configs/HWP_GFXv3_EX16_16PMP_QVGAv1.h"
#elif defined(CFG_INCLUDE_GFXv3_EX16_8PMP_WQVGAv1)
    #include "Configs/system_config_8PMP_WQVGAv1.h"
#elif defined(CFG_INCLUDE_GFXv3_EX16_16PMP_WQVGAv1)
    #include "Configs/system_config_16PMP_WQVGAv1.h"
/*********************************************************************
 * Hardware Configuration for
 * GFX version 3 with a Starter Kit 
 * 8 or 16-bit PMP
 * QVGA or WQVGA
 ********************************************************************/
#elif defined(CFG_INCLUDE_GFXv3_PIC_SK_8PMP_QVGAv1)
    #include "Configs/HWP_GFXv3_PIC_SK_8PMP_QVGAv1.h"
#elif defined(CFG_INCLUDE_GFXv3_PIC_SK_16PMP_QVGAv1)
    #include "Configs/HWP_GFXv3_PIC_SK_16PMP_QVGAv1.h"
#elif defined(CFG_INCLUDE_GFXv3_PIC_SK_8PMP_WQVGAv1)
    #include "Configs/HWP_GFXv3_PIC_SK_8PMP_WQVGAv1.h"
#elif defined(CFG_INCLUDE_GFXv3_PIC_SK_16PMP_WQVGAv1)
    #include "Configs/HWP_GFXv3_PIC_SK_16PMP_WQVGAv1.h"
/*********************************************************************
 * Hardware Configuration for
 * Multimedia Expasion Board 
 * 8 or 16-bit PMP
 ********************************************************************/
#elif defined(CFG_INCLUDE_MEB_PIC32_GP_SK_8PMP)
    #include "Configs/HWP_MEB_PIC32_GP_SK_8PMP.h"
#elif defined(CFG_INCLUDE_MEB_PIC32_GP_SK_16PMP)
    #include "Configs/HWP_MEB_PIC32_GP_SK_16PMP.h"
#elif defined(CFG_INCLUDE_MEB_PIC32_USB_SK_8PMP)
    #include "Configs/HWP_MEB_PIC32_USB_SK_8PMP.h"
#elif defined(CFG_INCLUDE_MEB_PIC32_USB_SK_16PMP)
    #include "Configs/HWP_MEB_PIC32_USB_SK_16PMP.h"
#elif defined(CFG_INCLUDE_MEB_PIC32_ETH_SK_8PMP)
    #include "Configs/HWP_MEB_PIC32_ETH_SK_8PMP.h"
#elif defined(CFG_INCLUDE_MEB_PIC32_ETH_SK_16PMP)
    #include "Configs/HWP_MEB_PIC32_ETH_SK_16PMP.h"
#else
#error "Please make sure that you have a CFG_INCLUDE_xxxx as a compile preprocessor define in the MPLAB X configuration."
#endif
#endif


#ifdef USA_ETHERNET
 /*********************************************************************
 *
 *	Hardware specific definitions for:
 *    - Explorer 16
 *    - PIC24F, PIC24H, dsPIC33F
 *    - Ethernet PICtail Plus (ENC28J60)
 *
 *********************************************************************
 * FileName:        HardwareProfile.h
 * Dependencies:    Compiler.h
 * Processor:       PIC24F, PIC24H, dsPIC30F, dsPIC33F
 * Compiler:        Microchip C30 v3.24 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2010 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * Author               Date		Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder		09/16/2010	Regenerated for specific boards
 * 			2012-2014	 KTronic board(s)
 ********************************************************************/

// Define a macro describing this hardware set up (used in other files)
//#define EXPLORER_16

// Set configuration fuses (but only in MainDemo.c where THIS_IS_STACK_APPLICATION is defined)
#if defined(THIS_IS_STACK_APPLICATION)
//	_CONFIG3(WPDIS_WPDIS & WPCFG_WPCFGDIS & WPEND_WPSTARTMEM & WPFP_WPFP0 /* WPFP_WPFP3 */)		// 
//	_CONFIG2(FNOSC_FRCPLL /*& POSCMOD_XT */ & IESO_OFF  & OSCIOFNC_OFF & FCKSM_CSECMD & POSCMOD_NONE & IOL1WAY_ON)		// internal OSC with 4x PLL, write RP only once
//		_CONFIG1(JTAGEN_OFF & FWDTEN_OFF)		// JTAG off, watchdog timer off
//	_CONFIG1( JTAGEN_OFF & GCP_OFF & GWRP_OFF & COE_OFF & ICS_PGx1 & FWDTEN_OFF /*& FWPSA_PR32 */ & WDTPS_PS512 /*& BORV_27*/)
//spostato per consentire bootloader...
#endif


// Clock frequency values
// Create a PIC dependant macro for the maximum supported internal clock
#if defined(__PIC24F__) || defined(__PIC24FK__)
9	#define MAXIMUM_PIC_FREQ		(32000000ull)
#elif	defined(__PIC24E__)		// 
9	#define MAXIMUM_PIC_FREQ		(140000000ull)
#else	// dsPIC33F, PIC24H
9	#define MAXIMUM_PIC_FREQ		(80000000ull)
#endif



// Hardware I/O pin mappings
//v. hardwareprofile.h per terminale


// ENC28J60 I/O pins
//ethernet...
//	#define ENC_CS_TRIS			(TRISFbits.TRISF12)	// Comment this line out if you are using the ENC424J600/624J600, MRF24WB0M, or other network controller.
//	#define ENC_CS_IO			(LATFbits.LATF12)
	// SPI SCK, SDI, SDO pins are automatically controlled by the 
	// PIC24/dsPIC SPI module 
//	#define ENC_SPI_IF			(IFS2bits.SPI2IF)
//	#define ENC_SSPBUF			(SPI2BUF)
//	#define ENC_SPISTAT			(SPI2STAT)
//	#define ENC_SPISTATbits		(SPI2STATbits)
//	#define ENC_SPICON1			(SPI2CON1)
//	#define ENC_SPICON1bits		(SPI2CON1bits)
//	#define ENC_SPICON2			(SPI2CON2)


//// ENC624J600 Interface Configuration
//// Comment out ENC100_INTERFACE_MODE if you don't have an ENC624J600 or 
//// ENC424J600.  Otherwise, choose the correct setting for the interface you 
//// are using.  Legal values are:
////  - Commented out: No ENC424J600/624J600 present or used.  All other 
////                   ENC100_* macros are ignored.
////	- 0: SPI mode using CS, SCK, SI, and SO pins
////  - 1: 8-bit demultiplexed PSP Mode 1 with RD and WR pins
////  - 2: *8-bit demultiplexed PSP Mode 2 with R/Wbar and EN pins
////  - 3: *16-bit demultiplexed PSP Mode 3 with RD, WRL, and WRH pins
////  - 4: *16-bit demultiplexed PSP Mode 4 with R/Wbar, B0SEL, and B1SEL pins
////  - 5: 8-bit multiplexed PSP Mode 5 with RD and WR pins
////  - 6: *8-bit multiplexed PSP Mode 6 with R/Wbar and EN pins
////  - 9: 16-bit multiplexed PSP Mode 9 with AL, RD, WRL, and WRH pins
////  - 10: *16-bit multiplexed PSP Mode 10 with AL, R/Wbar, B0SEL, and B1SEL 
////        pins
//// *IMPORTANT NOTE: DO NOT USE PSP MODE 2, 4, 6, OR 10 ON EXPLORER 16! 
//// Attempting to do so will cause bus contention with the LCD module which 
//// shares the PMP.  Also, PSP Mode 3 is risky on the Explorer 16 since it 
//// can randomly cause bus contention with the 25LC256 EEPROM.
#define ENC100_INTERFACE_MODE			0
//
//// If using a parallel interface, direct RAM addressing can be used (if all 
//// addresses wires are connected), or a reduced number of pins can be used 
//// for indirect addressing.  If using an SPI interface or PSP Mode 9 or 10 
//// (multiplexed 16-bit modes), which require all address lines to always be 
//// connected, then this option is ignored. Comment out or uncomment this 
//// macro to match your hardware connections.
//#define ENC100_PSP_USE_INDIRECT_RAM_ADDRESSING
//
//// ENC424J600/624J600 parallel indirect address remapping macro function.
//// This section translates SFR and RAM addresses presented to the 
//// ReadMemory() and WriteMemory() APIs in ENCX24J600.c to the actual 
//// addresses that must be presented on the parallel interface.  This macro 
//// must be modified to match your hardware if you are using an indirect PSP 
//// addressing mode (ENC100_PSP_USE_INDIRECT_RAM_ADDRESSING is defined) and 
//// have some of your address lines tied off to Vdd.  If you are using the 
//// SPI interface, then this section can be ignored or deleted.
//#if (ENC100_INTERFACE_MODE == 1) || (ENC100_INTERFACE_MODE == 2) || (ENC100_INTERFACE_MODE == 5) || (ENC100_INTERFACE_MODE == 6) // 8-bit PSP
//	// Several TCP/IP evaluation boards use the PMA8/U2TX pin for UART communication.
//	// This function will remap the A8 address signal used in PMP modes 1, 2, 6 and 6
//	// to PMPCS1 (PMA14), which will allow continued UART2 functionality on these boards.
//	#define ENC100_TRANSLATE_TO_PIN_ADDR(a)		((((a)&0x0100)<<6) | ((a)&0x00FF))
//#elif (ENC100_INTERFACE_MODE == 3) || (ENC100_INTERFACE_MODE == 4) // 16-bit PSP
//	#define ENC100_TRANSLATE_TO_PIN_ADDR(a)		(a)
//#endif
//
//// Auto-crossover pins on Fast 100Mbps Ethernet PICtail/PICtail Plus.  If 
//// your circuit doesn't have such a feature, delete these two defines.
//#if defined(__dsPIC33E__)|| defined (__PIC24E__)
//	#define ENC100_MDIX_TRIS				(TRISAbits.TRISA9)
//	#define ENC100_MDIX_IO					(LATAbits.LATA9)
//#else
//	#define ENC100_MDIX_TRIS				(TRISBbits.TRISB3)
//	#define ENC100_MDIX_IO					(LATBbits.LATB3)
//#endif
//
//// ENC624J600 I/O control and status pins
//// If a pin is not required for your selected ENC100_INTERFACE_MODE 
//// interface selection (ex: WRH/B1SEL for PSP modes 1, 2, 5, and 6), then 
//// you can ignore, delete, or put anything for the pin definition.  Also, 
//// the INT and POR pins are entirely optional.  If not connected, comment 
//// them out.
//#if defined(__dsPIC33FJ256GP710__) || defined(__PIC24HJ256GP610__)
//	#define ENC100_INT_TRIS				(TRISAbits.TRISA13)		// INT signal is optional and currently unused in the Microchip TCP/IP Stack.  Leave this pin disconnected and comment out this pin definition if you don't want it.
//	#define ENC100_INT_IO				(PORTAbits.RA13)
//#else
//	#define ENC100_INT_TRIS				(TRISBbits.TRISB12)		// INT signal is optional and currently unused in the Microchip TCP/IP Stack.  Leave this pin disconnected and comment out this pin definition if you don't want it.
//	#define ENC100_INT_IO				(PORTBbits.RB12)
//#endif
//#if (ENC100_INTERFACE_MODE >= 1)	// Parallel mode
//	// PSP control signal pinout
//	#define ENC100_CS_TRIS					(TRISAbits.TRISA5)	// CS is optional in PSP mode.  If you are not sharing the parallel bus with another device, tie CS to Vdd and comment out this pin definition.
//	#define ENC100_CS_IO					(LATAbits.LATA5)
//	#define ENC100_POR_TRIS					(TRISCbits.TRISC1)	// POR signal is optional.  If your application doesn't have a power disconnect feature, comment out this pin definition.
//	#define ENC100_POR_IO					(LATCbits.LATC1)
//	#define ENC100_SO_WR_B0SEL_EN_TRIS		(TRISDbits.TRISD4)
//	#define ENC100_SO_WR_B0SEL_EN_IO		(LATDbits.LATD4)
//	#define ENC100_SI_RD_RW_TRIS			(TRISDbits.TRISD5)
//	#define ENC100_SI_RD_RW_IO				(LATDbits.LATD5)
//	#define ENC100_SCK_AL_TRIS				(TRISBbits.TRISB15)
//	#define ENC100_SCK_AL_IO				(LATBbits.LATB15)
//#else
//	// SPI pinout
//	#if defined(__PIC24FJ256GA110__) || defined(__dsPIC33E__)|| defined (__PIC24E__)	// Some processors must use SPI2 slot on Explorer 16.  If you don't have one of the specified processors but want to use SPI2 for some reason, you can use these definitions.
//		#define ENC100_CS_TRIS					(TRISFbits.TRISF12)	// CS is mandatory when using the SPI interface
//		#define ENC100_CS_IO					(LATFbits.LATF12)
//		#define ENC100_POR_TRIS					(TRISFbits.TRISF13)	// POR signal is optional.  If your application doesn't have a power disconnect feature, comment out this pin definition.
//		#define ENC100_POR_IO					(LATFbits.LATF13)
//	#else	// All other PIC24s, dsPICs, and PIC32s use SPI1 slot (top most closest to LCD)
		#define ENC100_CS_TRIS					(TRISFbits.TRISF12)	// CS is mandatory when using the SPI interface
		#define ENC100_CS_IO					(LATFbits.LATF12)
//		#define ENC100_POR_TRIS					(TRISDbits.TRISD15)	// POR signal is optional.  If your application doesn't have a power disconnect feature, comment out this pin definition.
//		#define ENC100_POR_IO					(LATDbits.LATD15)
//	#endif
//#endif
//
//// ENC624J600 Bit Bang PSP I/O macros and pin configuration for address and 
//// data.  If using the SPI interface (ENC100_INTERFACE_MODE is 0) then this 
//// section is not used and can be ignored or deleted.  If using the PIC PMP
//// hardware module (if present), then ENC100_BIT_BANG_PMP must be commented 
//// out and the remaining definitions will be ignored/can be deleted.  
//// Otherwise, if you are using a parallel interface mode, but do not have a 
//// PMP (or want to interface using different pins), define 
//// ENC100_BIT_BANG_PMP and properly configure the applicable macros.
////#define ENC100_BIT_BANG_PMP
//#if defined(ENC100_BIT_BANG_PMP)
//	#if ENC100_INTERFACE_MODE == 1 || ENC100_INTERFACE_MODE == 2	// Dumultiplexed 8-bit address/data modes
//		// SPI2 CANNOT BE ENABLED WHEN ACCESSING THE ENC624J600 FOR THESE TWO MODES AS THE PINS OVERLAP WITH ADDRESS LINES.
//		#if defined(ENC100_PSP_USE_INDIRECT_RAM_ADDRESSING)	// Only ENC624J600 address pins A0-A8 connected (A9-A14 tied to Vdd)
//			#if defined(__PIC24FJ256GB210__)
//				#define ENC100_INIT_PSP_BIT_BANG()	do{ANSA &= 0xF9E7; ANSB &= 0x3FFF; ANSG &= 0xFCFF;} while(0)		// RE0-RE7, RF12, RD11, RD4, RD5 (AD0-AD7, A5, A8, WR, RD) pins are already digital only pins.
//			#else
//				#define ENC100_INIT_PSP_BIT_BANG()	do{((volatile unsigned char*)&AD1PCFGH)[1] = 0xFF; ((volatile unsigned char*)&AD1PCFGL)[1] |= 0xC0;}while(0)	// Disable AN24-AN31 and AN14-AN15 analog inputs on RE0-RE7 and RB14-RB15 pins (ENCX24J600 AD0-AD7, A1-A0)
//			#endif
//			#define ENC100_SET_ADDR_TRIS_OUT()	do{TRISA &= 0xF9E7; TRISB &= 0x3FFF; TRISFbits.TRISF12 = 0; TRISGbits.TRISG9 = 0; TRISDbits.TRISD11 = 0;}while(0)
//			#define ENC100_SET_ADDR_IO(a)		do{unsigned short _SetMacro = (a); LATBbits.LATB15 = 0; LATBbits.LATB14 = 0; LATGbits.LATG9 = 0; LATA &= 0xF9E7; LATFbits.LATF12 = 0; LATDbits.LATD11 = 0; if(_SetMacro & 0x0001) LATBbits.LATB15 = 1; if(_SetMacro & 0x0002) LATBbits.LATB14 = 1; if(_SetMacro & 0x0004) LATGbits.LATG9 = 1; if(_SetMacro & 0x0008) LATAbits.LATA4 = 1; if(_SetMacro & 0x0010) LATAbits.LATA3 = 1; if(_SetMacro & 0x0020) LATFbits.LATF12 = 1; if(_SetMacro & 0x0040) LATAbits.LATA10 = 1; if(_SetMacro & 0x0080) LATAbits.LATA9 = 1; if(_SetMacro & 0x4000) LATDbits.LATD11 = 1;}while(0)
//			#define ENC100_SET_AD_TRIS_IN()		(((volatile unsigned char*)&TRISE)[0] = 0xFF)
//			#define ENC100_SET_AD_TRIS_OUT()	(((volatile unsigned char*)&TRISE)[0] = 0x00)
//			#define ENC100_GET_AD_IO()			(((volatile unsigned char*)&PORTE)[0])
//			#define ENC100_SET_AD_IO(data)		(((volatile unsigned char*)&LATE)[0] = (unsigned char)(data))
//		#else 	// All ENC624J600 address pins A0-A14 connected
//			#if defined(__PIC24FJ256GB210__)
//				#define ENC100_INIT_PSP_BIT_BANG()	do{ANSA &= 0xF9E7; ANSB &= 0x03FF; ANSG &= 0xFCFF;} while(0)		// RE0-RE7, RF12, RD11, RD4, RD5 (AD0-AD7, A5, A14, WR, RD) pins are already digital only pins.
//			#else
//				#define ENC100_INIT_PSP_BIT_BANG()	do{((volatile unsigned char*)&AD1PCFGH)[1] = 0xFF; ((volatile unsigned char*)&AD1PCFGL)[1] |= 0xFC;}while(0)	// Disable AN24-AN31 and AN10-AN15 analog inputs on RE0-RE7 and RB10-RB15 pins (ENCX24J600 AD0-AD7, A1-A0, A13-A10)
//			#endif
//			#define ENC100_SET_ADDR_TRIS_OUT()	do{TRISA &= 0xF9E7; TRISB &= 0x03FF; TRISF &= 0xEFCF; TRISGbits.TRISG9 = 0; TRISDbits.TRISD11 = 0;}while(0)
//			#define ENC100_SET_ADDR_IO(a)		do{unsigned short _SetMacro = (a); LATA &= 0xF9E7; LATB &= 0x03FF; LATF &= 0xEFCF; LATGbits.LATG9 = 0; LATDbits.LATD11 = 0; if(_SetMacro & 0x0001) LATBbits.LATB15 = 1; if(_SetMacro & 0x0002) LATBbits.LATB14 = 1; if(_SetMacro & 0x0004) LATGbits.LATG9 = 1; if(_SetMacro & 0x0008) LATAbits.LATA4 = 1; if(_SetMacro & 0x0010) LATAbits.LATA3 = 1; if(_SetMacro & 0x0020) LATFbits.LATF12 = 1; if(_SetMacro & 0x0040) LATAbits.LATA10 = 1; if(_SetMacro & 0x0080) LATAbits.LATA9 = 1; if(_SetMacro & 0x0100) LATFbits.LATF5 = 1; if(_SetMacro & 0x0200) LATFbits.LATF4 = 1; if(_SetMacro & 0x0400) LATBbits.LATB13 = 1; if(_SetMacro & 0x0800) LATBbits.LATB12 = 1; if(_SetMacro & 0x1000) LATBbits.LATB11 = 1; if(_SetMacro & 0x2000) LATBbits.LATB10 = 1; if(_SetMacro & 0x4000) LATDbits.LATD11 = 1;}while(0)
//			#define ENC100_SET_AD_TRIS_IN()		(((volatile unsigned char*)&TRISE)[0] = 0xFF)
//			#define ENC100_SET_AD_TRIS_OUT()	(((volatile unsigned char*)&TRISE)[0] = 0x00)
//			#define ENC100_GET_AD_IO()			(((volatile unsigned char*)&PORTE)[0])
//			#define ENC100_SET_AD_IO(data)		(((volatile unsigned char*)&LATE)[0] = (unsigned char)(data))
//		#endif
//	#elif ENC100_INTERFACE_MODE == 3 || ENC100_INTERFACE_MODE == 4	// Dumultiplexed 16-bit address/data modes
//		#if defined(ENC100_PSP_USE_INDIRECT_RAM_ADDRESSING)	// Only ENC624J600 address pins A0-A7 connected (A8-A13 tied to Vdd)
//			#if defined(__PIC24FJ256GB210__)
//				#define ENC100_INIT_PSP_BIT_BANG()	do{ANSA &= 0x79E7; ANSB &= 0x3FFF; ANSD &= 0xCF0F; ANSG &= 0xFCFC;}while(0)
//			#else
//				#define ENC100_INIT_PSP_BIT_BANG()	do{AD1PCFGH = 0xFFFF; AD1PCFGL = 0xFFFF; AD2PCFGL = 0xFFFF;}while(0)
//			#endif
//			#define ENC100_SET_ADDR_TRIS_OUT()	do{TRISA &= 0xF9E7; TRISBbits.TRISB15 = 0; TRISBbits.TRISB14 = 0; TRISFbits.TRISF12 = 0; TRISGbits.TRISG9 = 0;}while(0)
//			#define ENC100_SET_ADDR_IO(a)		do{unsigned short _wSetMacro = (a); LATA &= 0xF9E7; LATBbits.LATB15 = 0; LATBbits.LATB14 = 0; LATFbits.LATF12 = 0; LATGbits.LATG9 = 0; if(_wSetMacro & 0x0001) LATBbits.LATB15 = 1; if(_wSetMacro & 0x0002) LATBbits.LATB14 = 1; if(_wSetMacro & 0x0004) LATGbits.LATG9 = 1; if(_wSetMacro & 0x0008) LATAbits.LATA4 = 1; if(_wSetMacro & 0x0010) LATAbits.LATA3 = 1; if(_wSetMacro & 0x0020) LATFbits.LATF12 = 1; if(_wSetMacro & 0x0040) LATAbits.LATA10 = 1; if(_wSetMacro & 0x0080) LATAbits.LATA9 = 1;}while(0)
//			#define ENC100_WRH_B1SEL_TRIS		ENC100_SO_WR_B0SEL_EN_TRIS
//			#define ENC100_WRH_B1SEL_IO			ENC100_SO_WR_B0SEL_EN_IO
//			#define ENC100_SET_AD_TRIS_IN()		do{((volatile unsigned char*)&TRISE)[0] = 0xFF; TRISAbits.TRISA15 = 1; TRISCbits.TRISC13 = 1; TRISD |= 0x30C0; TRISGbits.TRISG0 = 1; TRISGbits.TRISG1 = 1;}while(0)
//			#define ENC100_SET_AD_TRIS_OUT()	do{((volatile unsigned char*)&TRISE)[0] = 0x00; TRISAbits.TRISA15 = 0; TRISCbits.TRISC13 = 0; TRISD &= 0xCF3F; TRISGbits.TRISG0 = 0; TRISGbits.TRISG1 = 0;}while(0)
//			#define ENC100_GET_AD_IOH()			(PORTGbits.RG0 | (PORTGbits.RG1<<1) | (PORTCbits.RC13<<2) | (PORTAbits.RA15<<3) | (PORTDbits.RD12<<4) | (PORTDbits.RD13<<5) | (PORTDbits.RD6<<6) | (PORTDbits.RD7<<7))
//			#define ENC100_GET_AD_IOL()			(((volatile unsigned char*)&PORTE)[0])
//			#define ENC100_SET_AD_IO(data)		do{unsigned short _wSetMacro = (data); ((volatile unsigned char*)&LATE)[0] = ((unsigned char*)&_wSetMacro)[0]; LATG &= 0xFFFC; LATCbits.LATC13 = 0; LATAbits.LATA15 = 0; LATD &= 0xCF3F; if(_wSetMacro & 0x0100) LATGbits.LATG0 = 1; if(_wSetMacro & 0x0200) LATGbits.LATG1 = 1; if(_wSetMacro & 0x0400) LATCbits.LATC13 = 1; if(_wSetMacro & 0x0800) LATAbits.LATA15 = 1; if(_wSetMacro & 0x1000) LATDbits.LATD12 = 1; if(_wSetMacro & 0x2000) LATDbits.LATD13 = 1; if(_wSetMacro & 0x4000) LATDbits.LATD6 = 1; if(_wSetMacro & 0x8000) LATDbits.LATD7 = 1;}while(0)
//			#define ENC100_SET_AD_IOL(data)		(((volatile unsigned char*)&LATE)[0] = (unsigned char)(data))
//		#else 	// All ENC624J600 address pins A0-A13 connected
//			#if defined(__PIC24FJ256GB210__)
//				#define ENC100_INIT_PSP_BIT_BANG()	do{ANSA &= 0x79E7; ANSB &= 0x03FF; ANSD &= 0xCF0F; ANSG &= 0xFCFC;}while(0)
//			#else
//				#define ENC100_INIT_PSP_BIT_BANG()	do{AD1PCFGH = 0xFFFF; AD1PCFGL = 0xFFFF; AD2PCFGL = 0xFFFF;}while(0)
//			#endif
//			#define ENC100_SET_ADDR_TRIS_OUT()	do{TRISA &= 0xF9E7; TRISB &= 0x03FF; TRISF &= 0xEFCF; TRISGbits.TRISG9 = 0;}while(0)
//			#define ENC100_SET_ADDR_IO(a)		do{unsigned short _wSetMacro = (a); LATA &= 0xF9E7; LATB &= 0x03FF; LATF &= 0xEFCF; LATGbits.LATG9 = 0; if(_wSetMacro & 0x0001) LATBbits.LATB15 = 1; if(_wSetMacro & 0x0002) LATBbits.LATB14 = 1; if(_wSetMacro & 0x0004) LATGbits.LATG9 = 1; if(_wSetMacro & 0x0008) LATAbits.LATA4 = 1; if(_wSetMacro & 0x0010) LATAbits.LATA3 = 1; if(_wSetMacro & 0x0020) LATFbits.LATF12 = 1; if(_wSetMacro & 0x0040) LATAbits.LATA10 = 1; if(_wSetMacro & 0x0080) LATAbits.LATA9 = 1; if(_wSetMacro & 0x0100) LATFbits.LATF5 = 1; if(_wSetMacro & 0x0200) LATFbits.LATF4 = 1; if(_wSetMacro & 0x0400) LATBbits.LATB13 = 1; if(_wSetMacro & 0x0800) LATBbits.LATB12 = 1; if(_wSetMacro & 0x1000) LATBbits.LATB11 = 1; if(_wSetMacro & 0x2000) LATBbits.LATB10 = 1;}while(0)
//			#define ENC100_WRH_B1SEL_TRIS		ENC100_SO_WR_B0SEL_EN_TRIS
//			#define ENC100_WRH_B1SEL_IO			ENC100_SO_WR_B0SEL_EN_IO
//			#define ENC100_SET_AD_TRIS_IN()		do{((volatile unsigned char*)&TRISE)[0] = 0xFF; TRISAbits.TRISA15 = 1; TRISCbits.TRISC13 = 1; TRISD |= 0x30C0; TRISGbits.TRISG0 = 1; TRISGbits.TRISG1 = 1;}while(0)
//			#define ENC100_SET_AD_TRIS_OUT()	do{((volatile unsigned char*)&TRISE)[0] = 0x00; TRISAbits.TRISA15 = 0; TRISCbits.TRISC13 = 0; TRISD &= 0xCF3F; TRISGbits.TRISG0 = 0; TRISGbits.TRISG1 = 0;}while(0)
//			#define ENC100_GET_AD_IOH()			(PORTGbits.RG0 | (PORTGbits.RG1<<1) | (PORTCbits.RC13<<2) | (PORTAbits.RA15<<3) | (PORTDbits.RD12<<4) | (PORTDbits.RD13<<5) | (PORTDbits.RD6<<6) | (PORTDbits.RD7<<7))
//			#define ENC100_GET_AD_IOL()			(((volatile unsigned char*)&PORTE)[0])
//			#define ENC100_SET_AD_IO(data)		do{unsigned short _wSetMacro = (data); ((volatile unsigned char*)&LATE)[0] = ((unsigned char*)&_wSetMacro)[0]; LATG &= 0xFFFC; LATCbits.LATC13 = 0; LATAbits.LATA15 = 0; LATD &= 0xCF3F; if(_wSetMacro & 0x0100) LATGbits.LATG0 = 1; if(_wSetMacro & 0x0200) LATGbits.LATG1 = 1; if(_wSetMacro & 0x0400) LATCbits.LATC13 = 1; if(_wSetMacro & 0x0800) LATAbits.LATA15 = 1; if(_wSetMacro & 0x1000) LATDbits.LATD12 = 1; if(_wSetMacro & 0x2000) LATDbits.LATD13 = 1; if(_wSetMacro & 0x4000) LATDbits.LATD6 = 1; if(_wSetMacro & 0x8000) LATDbits.LATD7 = 1;}while(0)
//			#define ENC100_SET_AD_IOL(data)		(((volatile unsigned char*)&LATE)[0] = (unsigned char)(data))
//		#endif
//	#elif ENC100_INTERFACE_MODE == 5 || ENC100_INTERFACE_MODE == 6	// Mutliplexed 8-bit address/data modes
//		#if defined(ENC100_PSP_USE_INDIRECT_RAM_ADDRESSING)	// Only ENCX24J600 address pins AD0-AD8 connected (AD9-AD14 tied to Vdd)
//			#if defined(__PIC24FJ256GB210__)
//				#define ENC100_INIT_PSP_BIT_BANG()	do{ANSB &= 0x7FFF; ANSG &= 0xFEFF;} while(0)		// RE0-RE7, RD11, RD4, RD5 (AD0-AD7, AD8, WR, RD) pins are already digital only pins.  RB15, RG8 (AL, CS) needs to be made digital only.
//			#else
//				#define ENC100_INIT_PSP_BIT_BANG()	do{((volatile unsigned char*)&AD1PCFGH)[1] = 0xFF;}while(0)	// Disable AN24-AN31 analog inputs on RE0-RE7 pins (ENCX24J600 AD0-AD7)
//			#endif
//			#define ENC100_SET_AD_TRIS_IN()		do{((volatile unsigned char*)&TRISE)[0] = 0xFF;}while(0)
//			#define ENC100_SET_AD_TRIS_OUT()	do{((volatile unsigned char*)&TRISE)[0] = 0x00; TRISDbits.TRISD11 = 0;}while(0)
//			#define ENC100_GET_AD_IO()			(((volatile unsigned char*)&PORTE)[0])
//			#define ENC100_SET_AD_IO(data)		do{unsigned short _wSetMacro = (data); ((volatile unsigned char*)&LATE)[0] = (unsigned char)_wSetMacro; LATDbits.LATD11 = 0; if(_wSetMacro & 0x4000) LATDbits.LATD11 = 1;}while(0)
//			#define ENC100_SET_AD_IOL(data)		(((volatile unsigned char*)&LATE)[0] = (unsigned char)(data))
//		#else 	// All ENCX24J600 address pins AD0-AD14 connected
//			// This pinout is bad for doing 8-bit bit-bang operations with all address lines.  The Fast 100Mbps Ethernet PICtail Plus hardware is wired for PMP hardware support, which requires this pinout.  However, if you are designing a custom board, you can simplify these read/write operations dramatically if you wire things more logically by putting all 15 I/O pins, in order, on PORTB or PORTD.  Such a change would enhance performance.
//			// UART2 CANNOT BE USED OR ENABLED FOR THESE TWO MODES AS THE PINS OVERLAP WITH ADDRESS LINES.
//			#if defined(__PIC24FJ256GB210__)
//				#define ENC100_INIT_PSP_BIT_BANG()	do{ANSB &= 0x43FF; ANSG &= 0xFEFF;} while(0) // Set pins as digital I/Os (not analog).  RD11, RD5, RD4, RE0-RE7, RF4, RF5 are all digital-only pins and therefore no writes to ANSD, ANSE, or ANSF are needed.
//			#else
//				#define ENC100_INIT_PSP_BIT_BANG()	do{AD1PCFGL |= 0x3C00; ((volatile unsigned char*)&AD1PCFGH)[1] = 0xFF;}while(0)	// Disable AN10-AN13 and AN24-AN31 analog inputs on RB10-RB13 and RE0-RE7 pins (ENCX24J600 AD13-AD10 and AD0-AD7)
//			#endif
//			#define ENC100_SET_AD_TRIS_IN()		do{((volatile unsigned char*)&TRISE)[0] = 0xFF;}while(0)
//			#define ENC100_SET_AD_TRIS_OUT()	do{((volatile unsigned char*)&TRISE)[0] = 0x00; TRISFbits.TRISF5 = 0; TRISFbits.TRISF4 = 0; TRISB &= 0x43FF; TRISDbits.TRISD11 = 0;}while(0)
//			#define ENC100_GET_AD_IO()			(((volatile unsigned char*)&PORTE)[0])
//			#define ENC100_SET_AD_IO(data)		do{unsigned short _wSetMacro = (data); ((volatile unsigned char*)&LATE)[0] = (unsigned char)_wSetMacro; LATFbits.LATF5 = 0; LATFbits.LATF4 = 0; LATB &= 0x43FF; LATDbits.LATD11 = 0; if(_wSetMacro & 0x0100) LATFbits.LATF5 = 1; if(_wSetMacro & 0x0200) LATFbits.LATF4 = 1; if(_wSetMacro & 0x0400) LATBbits.LATB13 = 1; if(_wSetMacro & 0x0800) LATBbits.LATB12 = 1; if(_wSetMacro & 0x1000) LATBbits.LATB11 = 1;  if(_wSetMacro & 0x2000) LATBbits.LATB10 = 1; if(_wSetMacro & 0x4000) LATDbits.LATD11 = 1;}while(0)
//			#define ENC100_SET_AD_IOL(data)		(((volatile unsigned char*)&LATE)[0] = (unsigned char)(data))
//		#endif
//	#elif ENC100_INTERFACE_MODE == 9 || ENC100_INTERFACE_MODE == 10	// Mutliplexed 16-bit address/data modes
//		// All ENC624J600 adddress/data pins AD0-AD15 connected (required for 16-bit data, so there is no differentiation for indirect versus direct addressing mode)
//		// This pinout is awful for doing 16-bit bit-bang operations.  The Fast 100Mbps Ethernet PICtail Plus hardware is wired for PMP hardware support, which requires this pinout.  However, if you are designing a custom board, you can simplify these read/write operations dramatically if you wire things more logically by putting all 16 I/O pins, in order, on PORTB or PORTD.  Such a change would enhance performance.
//		#if defined(__PIC24FJ256GB210__)
//			#define ENC100_INIT_PSP_BIT_BANG()	do{ANSBbits.ANSB15 = 0; ANSCbits.ANSC13 = 0; ANSD &= 0xCF0F; ANSGbits.ANSG8 = 0;}while(0)	// Set pins as digital I/Os (not analog).  RA15 and RE0-RE7 are all digital-only pins and therefore no writes to ANSA or ANSE are needed.
//		#else
//			#define ENC100_INIT_PSP_BIT_BANG()	do{((volatile unsigned char*)&AD1PCFGH)[1] = 0xFF;}while(0)	// Disable AN24-AN31 analog inputs on RE0-RE7 pins (ENCX24J600 AD0-AD7)
//		#endif
//		#define ENC100_WRH_B1SEL_TRIS		ENC100_SO_WR_B0SEL_EN_TRIS
//		#define ENC100_WRH_B1SEL_IO			ENC100_SO_WR_B0SEL_EN_IO
//		#define ENC100_SET_AD_TRIS_IN()		do{((volatile unsigned char*)&TRISE)[0] = 0xFF; TRISAbits.TRISA15 = 1; TRISCbits.TRISC13 = 1; TRISD |= 0x30C0; TRISGbits.TRISG0 = 1; TRISGbits.TRISG1 = 1;}while(0)
//		#define ENC100_SET_AD_TRIS_OUT()	do{((volatile unsigned char*)&TRISE)[0] = 0x00; TRISAbits.TRISA15 = 0; TRISCbits.TRISC13 = 0; TRISD &= 0xCF3F; TRISGbits.TRISG0 = 0; TRISGbits.TRISG1 = 0;}while(0)
//		#define ENC100_GET_AD_IOH()			(PORTGbits.RG0 | (PORTGbits.RG1<<1) | (PORTCbits.RC13<<2) | (PORTAbits.RA15<<3) | (PORTDbits.RD12<<4) | (PORTDbits.RD13<<5) | (PORTDbits.RD6<<6) | (PORTDbits.RD7<<7))
//		#define ENC100_GET_AD_IOL()			(((volatile unsigned char*)&PORTE)[0])
//		#define ENC100_SET_AD_IO(data)		do{unsigned short _wSetMacro = (data); ((volatile unsigned char*)&LATE)[0] = ((unsigned char*)&_wSetMacro)[0]; LATG &= 0xFFFC; LATCbits.LATC13 = 0; LATAbits.LATA15 = 0; LATD &= 0xCF3F; if(_wSetMacro & 0x0100) LATGbits.LATG0 = 1; if(_wSetMacro & 0x0200) LATGbits.LATG1 = 1; if(_wSetMacro & 0x0400) LATCbits.LATC13 = 1; if(_wSetMacro & 0x0800) LATAbits.LATA15 = 1; if(_wSetMacro & 0x1000) LATDbits.LATD12 = 1; if(_wSetMacro & 0x2000) LATDbits.LATD13 = 1; if(_wSetMacro & 0x4000) LATDbits.LATD6 = 1; if(_wSetMacro & 0x8000) LATDbits.LATD7 = 1;}while(0)
//		#define ENC100_SET_AD_IOL(data)		(((volatile unsigned char*)&LATE)[0] = (unsigned char)(data))
//	#endif
//#endif
//
//// ENC624J600 SPI SFR register selection (controls which SPI peripheral to 
//// use on PICs with multiple SPI peripherals).  If a parallel interface is 
//// used (ENC100_INTERFACE_MODE is >= 1), then the SPI is not used and this 
//// section can be ignored or deleted.
//#if defined(__PIC24FJ256GA110__)	// The PIC24FJ256GA110 must use SPI2 slot on Explorer 16.  If you don't have a PIC24FJ256GA110 but want to use SPI2 for some reason, you can use these definitions.
//	#define ENC100_ISR_ENABLE		(IEC3bits.INT4IE)
//	#define ENC100_ISR_FLAG			(IFS3bits.INT4IF)
//	#define ENC100_ISR_POLARITY		(INTCON2bits.INT4EP)
//	#define ENC100_ISR_PRIORITY		(IPC13bits.INT4IP)
//	#define ENC100_SPI_ENABLE		(ENC100_SPISTATbits.SPIEN)
//	#define ENC100_SPI_IF			(IFS1bits.SPI2IF)
//	#define ENC100_SSPBUF			(SPI2BUF)
//	#define ENC100_SPISTAT			(SPI2STAT)
//	#define ENC100_SPISTATbits		(SPI2STATbits)
//	#define ENC100_SPICON1			(SPI2CON1)
//	#define ENC100_SPICON1bits		(SPI2CON1bits)
//	#define ENC100_SPICON2			(SPI2CON2)
//#elif defined (__dsPIC33E__) || defined (__PIC24E__)	
//	#define ENC100_ISR_ENABLE		(IEC1bits.INT2IE)
//	#define ENC100_ISR_FLAG			(IFS1bits.INT2IF)
//	#define ENC100_ISR_POLARITY		(INTCON2bits.INT2EP)
//	#define ENC100_ISR_PRIORITY		(IPC7bits.INT2IP)
//	#define ENC100_SPI_ENABLE		(ENC100_SPISTATbits.SPIEN)
//	#define ENC100_SPI_IF			(IFS0bits.SPI2IF)
//	#define ENC100_SSPBUF			(SPI2BUF)
//	#define ENC100_SPISTAT			(SPI2STAT)
//	#define ENC100_SPISTATbits		(SPI2STATbits)
//	#define ENC100_SPICON1			(SPI2CON1)
//	#define ENC100_SPICON1bits		(SPI2CON1bits)
//	#define ENC100_SPICON2			(SPI2CON2)
//#else	// All other PIC24s and dsPICs use SPI1 slot (top most closest to LCD)
	#define ENC100_ISR_ENABLE		(IEC1bits.INT2IE)
	#define ENC100_ISR_FLAG			(IFS1bits.INT2IF)
	#define ENC100_ISR_POLARITY		(INTCON2bits.INT2EP)
	#define ENC100_ISR_PRIORITY		(IPC7bits.INT2IP)
	#define ENC100_SPI_ENABLE		(ENC100_SPISTATbits.SPIEN)
	#define ENC100_SPI_IF			(IFS0bits.SPI1IF)
	#define ENC100_SSPBUF			(SPI1BUF)
	#define ENC100_SPISTAT			(SPI1STAT)
	#define ENC100_SPISTATbits		(SPI1STATbits)
	#define ENC100_SPICON1			(SPI1CON1)
	#define ENC100_SPICON1bits		(SPI1CON1bits)
	#define ENC100_SPICON2			(SPI1CON2)
//#endif




////----------------------------
//// MRF24WB0M WiFi I/O pins
////----------------------------
//// If you have a MRF24WB0M WiFi PICtail, you must uncomment one of 
//// these two lines to use it.  SPI1 is the top-most slot in the Explorer 16 
//// (closer to the LCD and prototyping area) while SPI2 corresponds to 
//// insertion of the PICtail into the middle of the side edge connector slot.
//#define MRF24WB0M_IN_SPI1
////#define MRF24WB0M_IN_SPI2
//
//// PIC24FJ256GA110 PIM on Explorer 16 must use SPI2, not SPI1
//#if defined(MRF24WB0M_IN_SPI1) && defined(__PIC24FJ256GA110__)
//	#undef MRF24WB0M_IN_SPI1
//	#define MRF24WB0M_IN_SPI2
//#endif
//
//// dsPIC33E/PIC24E PIM on Explorer 16 must use SPI2, not SPI1
//#if defined(MRF24WB0M_IN_SPI1) && (defined(__dsPIC33E__) || defined(__PIC24E__))
//	#undef MRF24WB0M_IN_SPI1
//	#define MRF24WB0M_IN_SPI2
//#endif
//
//#if defined(MRF24WB0M_IN_SPI1) && !defined(__32MX460F512L__) && !defined(__32MX795F512L__) && !defined(__PIC24FJ256GA110__)
//	// MRF24WB0M in SPI1 slot
//	#define WF_CS_TRIS			(TRISBbits.TRISB2)
//	#define WF_CS_IO			(LATBbits.LATB2)
//	#define WF_SDI_TRIS			(TRISFbits.TRISF7)
//	#define WF_SCK_TRIS			(TRISFbits.TRISF6)
//	#define WF_SDO_TRIS			(TRISFbits.TRISF8)
//	#define WF_RESET_TRIS		(TRISFbits.TRISF0)
//	#define WF_RESET_IO			(LATFbits.LATF0)
//	#if defined(__dsPIC33FJ256GP710__) || defined(__PIC24HJ256GP610__) || defined (__dsPIC33E__)||defined(__PIC24E__)
//		#define WF_INT_TRIS	    (TRISAbits.TRISA12)
//		#define WF_INT_IO		(PORTAbits.RA12)
//	#else
//		#define WF_INT_TRIS	    (TRISEbits.TRISE8)  // INT1
//		#define WF_INT_IO		(PORTEbits.RE8)
//	#endif
//	#define WF_HIBERNATE_TRIS	(TRISFbits.TRISF1)
//	#define	WF_HIBERNATE_IO		(PORTFbits.RF1)
//	#define WF_INT_EDGE			(INTCON2bits.INT1EP)
//	#define WF_INT_IE			(IEC1bits.INT1IE)
//	#define WF_INT_IF			(IFS1bits.INT1IF)
//
//	#define WF_SSPBUF			(SPI1BUF)
//	#define WF_SPISTAT			(SPI1STAT)
//	#define WF_SPISTATbits		(SPI1STATbits)
//	#define WF_SPICON1			(SPI1CON1)
//	#define WF_SPICON1bits		(SPI1CON1bits)
//	#define WF_SPICON2			(SPI1CON2)
//	#define WF_SPI_IE			(IEC0bits.SPI1IE)
////	#define WF_SPI_IP			(IPC2bits.SPI1IP)
//	#define WF_SPI_IF			(IFS0bits.SPI1IF)
//
//#elif defined(MRF24WB0M_IN_SPI2) && !defined(__32MX460F512L__) && !defined(__32MX795F512L__)
//	// MRF24WB0M in SPI2 slot
//	#define WF_CS_TRIS			(TRISGbits.TRISG9)
//	#define WF_CS_IO			(LATGbits.LATG9)
//	#define WF_SDI_TRIS			(TRISGbits.TRISG7)
//	#define WF_SCK_TRIS			(TRISGbits.TRISG6)
//	#define WF_SDO_TRIS			(TRISGbits.TRISG8)
//	#define WF_RESET_TRIS		(TRISGbits.TRISG0)
//	#define WF_RESET_IO			(LATGbits.LATG0)
//	#if defined(__PIC24FJ256GB110__) || defined(__PIC24FJ256GB210__)
//		#define WF_INT_TRIS			(TRISCbits.TRISC3)	// INT3
//		#define WF_INT_IO			(PORTCbits.RC3)
//	#else
//		#define WF_INT_TRIS			(TRISAbits.TRISA14)	// INT3
//		#define WF_INT_IO			(PORTAbits.RA14)
//	#endif
//	#define WF_HIBERNATE_TRIS		(TRISGbits.TRISG1)
//	#define	WF_HIBERNATE_IO			(PORTGbits.RG1)
//	#define WF_INT_EDGE			(INTCON2bits.INT3EP)
//	#define WF_INT_IE			(IEC3bits.INT3IE)
//	#define WF_INT_IF			(IFS3bits.INT3IF)
//
//	#define WF_SSPBUF			(SPI2BUF)
//	#define WF_SPISTAT			(SPI2STAT)
//	#define WF_SPISTATbits		(SPI2STATbits)
//	#define WF_SPICON1			(SPI2CON1)
//	#define WF_SPICON1bits		(SPI2CON1bits)
//	#define WF_SPICON2			(SPI2CON2)
//	#define WF_SPI_IE			(IEC2bits.SPI2IE)
////	#define WF_SPI_IP			(IPC8bits.SPI2IP)
//	#define WF_SPI_IF			(IFS2bits.SPI2IF)
//
//#endif


// Select which UART the STACK_USE_UART and STACK_USE_UART2TCP_BRIDGE 
// options will use.  You can change these to U1BRG, U1MODE, etc. if you 
// want to use the UART1 module instead of UART2.
#define UBRG				U3BRG
#define UMODE				U3MODE
#define USTA				U3STA
#define BusyUART()			BusyUART3()
#define CloseUART()			CloseUART3()
#define ConfigIntUART(a)	ConfigIntUART3(a)
#define DataRdyUART()		DataRdyUART3()
#define OpenUART(a,b,c)		OpenUART3(a,b,c)
#define ReadUART()			ReadUART3()
#define WriteUART(a)		WriteUART3(a)
#define getsUART(a,b,c)		getsUART3(a,b,c)
#define putsUART(a)			putsUART3((unsigned int*)a)
#define getcUART()			getcUART3()
#define putcUART(a)			do{while(BusyUART3()); WriteUART3(a); while(BusyUART3()); }while(0)
#define putrsUART(a)		putsUART3(a)




#endif

// These directly influence timed events using the Tick module.  They also are used for UART and SPI baud rate generation.
//gi� di l� GetSystemClock()		(MAXIMUM_PIC_FREQ)			// Hz
// #define GetInstructionClock()	(GetSystemClock()/2)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Might need changing if using Doze modes.
//gi� di l� #define GetPeripheralClock()	(GetSystemClock()/2)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Divisor may be different if using a PIC32 since it's configurable.
#define FCY (GetSystemClock()/2)		// per LibPic30.h e delay


// Hardware I/O pin mappings




#define BUTTON0_TRIS		(TRISDbits.TRISD0)	// sw1 KTronic, shared con led...
#define	BUTTON0_IO			(PORTDbits.RD0)
#define BUTTON1_TRIS		(TRISCbits.TRISC13)	// sw3
#define	BUTTON1_IO			(PORTCbits.RC13)
#define BUTTON2_TRIS		(TRISCbits.TRISC14)	// sw4
#define	BUTTON2_IO			(PORTCbits.RC14)

#define	TASTO1			(PORTGbits.RG6)		// i 4 tasti diretti Agie
#define	TASTO2			(PORTCbits.RC4)
#define	TASTO3			(PORTCbits.RC3)
#define	TASTO4			(PORTCbits.RC2)


#define LED0_TRIS			(TRISDbits.TRISD0)	// KTronic
#define LED0_IO				(LATDbits.LATD0)
#define LED1_TRIS			(TRISCbits.TRISC13)	// 
#define LED1_IO				(LATCbits.LATC13)
#define LED2_TRIS			(TRISCbits.TRISC14)	// 
#define LED2_IO				(LATCbits.LATC14)

/*FABRIZIO X RTC*/
#define kLED0_TRIS			(TRISAbits.TRISA10)	// 
#define kLED0_IO			  (LATAbits.LATA10)
#define kLED1_TRIS			(TRISAbits.TRISA10)	// (TRISAbits.TRISA2)	// 
#define kLED1_IO			  (LATAbits.LATA10)//(LATAbits.LATA2)
#define kLED2_TRIS		     (TRISAbits.TRISA4)// (TRISAbits.TRISA3)	// 
#define kLED2_IO			   (LATAbits.LATA4)//(LATAbits.LATA3)
#define kLED3_TRIS			 (TRISAbits.TRISA4)	// 
#define kLED3_IO			   (LATAbits.LATA4)
#define kLED4_TRIS			 (TRISAbits.TRISA5)	// 
#define kLED4_IO			   (LATAbits.LATA5)
#define kLED5_TRIS			 (TRISDbits.TRISD8)	// 
#define kLED5_IO			   (LATDbits.LATD8)
#define kLED6_TRIS			(TRISDbits.TRISD11)	// 
#define kLED6_IO				(LATDbits.LATD11)
#define kLED7_TRIS			(TRISDbits.TRISD9)	// 
#define kLED7_IO				(LATDbits.LATD9)
/*FABRIZIO X RTC*/

#define	m_MicroSacchetto		(PORTAbits.RA7)
#define	m_MicroFiltro			(PORTAbits.RA6)

#define	m_MotoreVibro1		(LATCbits.LATC3)		// saranno pwm...
#define	m_MotoreVibro2		(LATCbits.LATC4)
#define	m_IoniNegativi		(LATAbits.LATA)			// futuro



// qua su KTronic abbiamo una I2C
    #define m_I2CClkBit           LATAbits.LATA14
    #define m_I2CDataBit          LATAbits.LATA15
    #define m_I2CDataBitI         PORTAbits.RA15
    #define m_I2CClkBitI          PORTAbits.RA14
    #define I2CDataTris      TRISAbits.TRISA15
    #define I2CClkTris       TRISAbits.TRISA14
		#define SPI_232_I 			PORTAbits.RA15
		#define SPI_232_IO 			LATAbits.LATA15
		#define SPI_232_I_TRIS 			TRISAbits.TRISA15
		#define SPI_232_O 			LATAbits.LATA14
		#define SPI_232_OI 			PORTAbits.RA14
		#define SPI_232_O_TRIS 			TRISAbits.TRISA14



//SDcard
        // Description: SD-SPI Chip Select Output bit
        #define SD_CS				LATDbits.LATD14
        // Description: SD-SPI Chip Select TRIS bit
        #define SD_CS_TRIS          TRISDbits.TRISD14
        
        // Description: SD-SPI Card Detect Input bit
        #define SD_CD               PORTFbits.RF100
        // Description: SD-SPI Card Detect TRIS bit
        #define SD_CD_TRIS          TRISFbits.TRISF100
        #define MEDIA_SOFT_DETECT 1

        // Description: SD-SPI Write Protect Check Input bit
        #define SD_WE               PORTFbits.RF155
        // Description: SD-SPI Write Protect Check TRIS bit
        #define SD_WE_TRIS          TRISFbits.TRISF155

        // Registers for the SPI module you want to use

        // Description: The main SPI control register
        #define SPICON1             SPI1CON1
        // Description: The SPI status register
        #define SPISTAT             SPI1STAT
        // Description: The SPI Buffer
        #define SPIBUF              SPI1BUF
        // Description: The receive buffer full bit in the SPI status register
        #define SPISTAT_RBF         SPI1STATbits.SPIRBF
        // Description: The bitwise define for the SPI control register (i.e. _____bits)
        #define SPICON1bits         SPI1CON1bits
        // Description: The bitwise define for the SPI status register (i.e. _____bits)
        #define SPISTATbits         SPI1STATbits
        // Description: The enable bit for the SPI module
        #define SPIENABLE           SPISTATbits.SPIEN

        // Tris pins for SCK/SDI/SDO lines

        // Description: The TRIS bit for the SCK pin
        #define SPICLOCK            TRISDbits.TRISD15
        // Description: The TRIS bit for the SDI pin
        #define SPIIN               TRISBbits.TRISB15
        // Description: The TRIS bit for the SDO pin
        #define SPIOUT              TRISBbits.TRISB14


        #define SPI2_CS							LATDbits.LATD14
        #define SPI2CON1             SPI2CON1
        #define SPI2STAT             SPI2STAT
        #define SPI2BUF              SPI2BUF
        #define SPI2ENABLE           SPI2STATbits.SPIEN
        #define SPI2CON1bits         SPI2CON1bits
        #define SPI2STATbits         SPI2STATbits
        #define SPI2STAT_RBF         SPI2STATbits.SPIRBF
        #define SPI2CLOCK            TRISDbits.TRISD15
        #define SPI2IN               TRISBbits.TRISB15
        #define SPI2OUT              TRISBbits.TRISB14

        // Will generate an error if the clock speed is too low to interface to the card
        #if (GetSystemClock() < 100000ul)
            #error Clock speed must exceed 100 kHz
        #endif    



//#define USB_USE_MSD 1
//#define USE_USB_INTERFACE               // USB host MSD library, inutile avendo creato un FS a s�

#endif // #ifndef HARDWARE_PROFILE_H
