#ifndef _MAINDEMO_H
#define _MAINDEMO_H
    
    
#include "GenericTypeDefs.h"
#include "graphics/Graphics.h"
#include "FSDefs.h"
#include "FSIO.h"
#include "TimeDelay.h"

#endif
#include <uart.h>
#include <timer.h>
#include <libpic30.h>

#include "RTC_i2c_MCP79400/i2c.h"
#include "RTC_i2c_MCP79400/i2c_rtcc.h"

#define VERNUMH     0
#define VERNUML     4
//FABRIZIO
//#define PR1TICK20MILLI 54597
#define PR1TICK20MILLI 5460 //20 milli
#define EEPROM_CHUNK (sizeof(I2CBuffer))
//#define DEBUG_USB 1
//#define DEBUG_MODE 1


/************************************************************
 * Enumeration Structure
 ************************************************************/
typedef enum {
  DEMO_FILE_TYPE_JPEG,
  DEMO_FILE_TYPE_RGB,
  DEMO_FILE_TYPE_OTHER
} DEMO_FILE_TYPE;


   #define DEFAULT_YEARS               0x0018
   #define DEFAULT_MONTH_DAY           0x0922
   #define DEFAULT_WEEKDAY_HOURS       0x0521
   #define DEFAULT_MINUTES_SECONDS     0x3900

   #define INDEX_HOURS                 2
   #define INDEX_MINUTES               1
   #define INDEX_SECONDS               0
   #define INDEX_YEAR                  2
   #define INDEX_MONTH                 1
   #define INDEX_DAY                   0

/*FABRIZIO*/
   #define DIM_RECORD_LOG 200
   #define DIM_INTESTAZIONE_LOG 684
   

struct COMM_STATUS {
// errori/flag in CommStatus Seriale, (LSB prima - right-aligned)
	unsigned int FRAME_2SEND:1;         // c'e' da mandare un frame
	unsigned int WAIT_4ACK  :1;
	unsigned int FRAME_REC  :1;         // c'e' un frame ricevuto
	unsigned int COMM_OVRERR:1;					// overrun seriale 
	unsigned int COMM_OVL   :1;         // errore di overflow buffer in ricezione 
	unsigned int COMM_TOUT  :1;         // errore di timeout in ricezione frame
	unsigned int COMM_FRERR :1;         // errore di framing (START/STOP) in ricezione 
	unsigned int COMM_PERR  :1;         // errore di parita' in ricezione  (0x80)
	unsigned int BUSY  :1;							// occupato
	unsigned int LINE_BUSY  :1;         // linea intasata, impossibile inviare
	unsigned int COMM_CRCERR  :1;         // errore di checksum in ricezione 
	};

    
/*FABRIZIO*/
typedef struct{
    char *text; 
    char backimg[20];
    char show1[64];
    char action[64];
    //char button[64];
    int x;
    int y;
    char align[10];
    int ixbutton[10];
    char *btn[10];
} Menu; 

//Corrispondenza con LoadMenu char *chiavi[10]={"text","backimg","show1","action","a_s","x","y","align","timeout"};
typedef enum{
  text=0,
  backimg=1,
  show1=2,
  action=3,
  a_s=4,   
  x=5,
  y=6,
  align=7,
  timeout=8  
}comandi;


extern unsigned char val;
extern unsigned char old_val ;
extern  _cursore;
extern  _cursorePrec;
extern __minimo,__massimo;
extern Menu mnu;
extern struct CONFIG_PARMS configParms;       
extern const char *MENU_INI;

/*FINE FABRIZIO*/    
    
#define CR        0xd
#define LF        0xa
#define POSTVIBRO 50

extern volatile BYTE second_10,second_30;
extern volatile WORD tick10;
extern unsigned char flag_nok_sd;
extern unsigned char flag_ok_press;


/************************************************************
 * Function Prototypes
 ************************************************************/
void reset_azione();
int showAParm(BYTE ,char *);
int handleBIOS(BYTE);
int showBIOS(void);
int handleTastiera(void);

int FSReadLine(FSFILE *,char *);
int EEFSReadLine(FSFILE *,char *);
int getParmN(const char *,const char *,int *);
int getParmS(const char *,const char *,char *);
int FSwriteParmN(FSFILE *,const char *,int );
int FSwriteParmS(FSFILE *,const char *,const char *);
int EEFSwriteParmN(FSFILE *,const char *,int );
int EEFSwriteParmS(FSFILE *,const char *,const char *);
/* FABRIZIO */
BYTE GetProfileInt(const char *file,const char *gruppo,const char *chiave,int *value);
BYTE GetProfileString(const char *file,const char *gruppo,const char *chiave,char *value);
BYTE loadMenu(const char *file,const char *chiave);
BYTE cercaRigaLingua(int riga);
/* FINE FABRIZIO*/
BYTE WriteProfileInt(const char *gruppo,const char *chiave,int value);
BYTE WriteProfileString(const char *gruppo,const char *chiave,char *value);
BYTE cercaGruppo(FSFILE *,const char *);
signed char stricmp(const char *s,const char *d);
signed char strnicmp(const char *s,const char *d,int n);
void exportSettingsSD(void);
int exportSettingsUSB(void);
int importSettingsUSB(void);
DWORD copyFileUSBToSD(const char *,const char *);

void scanKBD(void);
BYTE checkKey(BYTE);
void LCDPutChar(BYTE);
void LCDPutString(const char *);
void LCDPutStringXY(BYTE, BYTE, const char *);
void LCDPutStringCenter(BYTE,const char *);
void LCDCls(void);
void LCDHome(void);
void LCDXY(BYTE,BYTE);
void LCDXYGraph(WORD ,WORD );
void LCDscrollY(void);
void drawCursor(BYTE);
void clearChar(void);
WORD myOutChar(XCHAR);
WORD subOutChar(XCHAR);
WORD subOutCharTransparent(XCHAR);
WORD OutCharRenderRAM(XCHAR, BYTE *,BYTE);
void OutCharCursor(BYTE);
void restoreScreen();




int saveVariables(void);
int loadVariables(void);
void aggiornaZonaTasti(BYTE,const char *);
BOOL bisestile(int anno);
void PutBmp8BPPExt(SHORT left, SHORT top, void* bitmap, BYTE stretch);
void PutBmp8BPPExtFast(SHORT left, SHORT top, void* bitmap, BYTE stretch);
void PutImageRLE8BPPExt(SHORT left, SHORT top, void* bitmap, BYTE stretch);
#define PutBmp8BPPExtFlip(x,y,c,d) PutBmp8BPPExt(x,GetMaxY()-y,c,d)
BYTE *PutJPEGFile(SHORT left, SHORT top, const char *input_file, 
									BITMAPINFOHEADER *bmp, BYTE *theBytes,
									int colorBits,int ditherMode,BOOL reverseV);
BYTE *BuildJPEG(void* bitmap, BYTE stretch, BOOL reverseV, int quality);

void resetSettings(void);
void saveSettings(void);
void loadSettings(void);


#define MAX_TASTI_CONTEMPORANEI 1

#define PWM_FREQ 2000			// occhio a 70000000

#define RESERVED_EEPROM 256			// per struct config; segue il resto...


//480x272 su wqvga
// v. maindemo.h, le risoluzioni testo
#define GetRealMaxX() ( GetMaxX()+1)
#define GetRealMaxY() ( GetMaxY()+1)
#define GetRealOrgX() ( 0 )
#define GetRealOrgY() ( 0 )
//#define getCharSizeX() (configTerminale.screenSize ? (GetRealMaxX()/24) : (GetRealMaxX()/32))
//#define getCharSizeY() (configTerminale.screenSize ? (GetRealMaxY()/8) : (GetRealMaxY()/16) )
#define getCharSizeX() (12)
#define getCharSizeY() (16)
//#define getSizeX() (40)
#define getSizeX() (DISP_HOR_RESOLUTION/getCharSizeX())
#define getSizeY() (34)

extern const BYTE font16[2*16*224],font32[2*32*224];


// il Timer0 conta ogni 62.5nSec*prescaler=64... (@32MHz CPUCLK => 16MHz) su PIC24; fanno 4uS
//#define TMR2BASE (8343-11)		//   30Hz per timer (per autorepeat max=30...)
#define TMR2BASE ((FCY/1917)-11)		//   30Hz per timer (per autorepeat max=30...)


    // PIC24 RTCC Structure
typedef union {
  struct {
    unsigned char   mday;       // BCD codification for day of the month, 01-31
    unsigned char   mon;        // BCD codification for month, 01-12
    unsigned char   year;       // BCD codification for years, 00-99
    unsigned char   reserved;   // reserved for future use. should be 0
  	};                              // field access	
  unsigned char       b[4];       // byte access
  unsigned short      w[2];       // 16 bits access
  unsigned long       l;          // 32 bits access
	} PIC24_RTCC_DATE;

// PIC24 RTCC Structure
typedef union {
  struct {
    unsigned char   sec;        // BCD codification for seconds, 00-59
    unsigned char   min;        // BCD codification for minutes, 00-59
    unsigned char   hour;       // BCD codification for hours, 00-24
    unsigned char   weekday;    // BCD codification for day of the week, 00-06
  	};                              // field access
  unsigned char       b[4];       // byte access
  unsigned short      w[2];       // 16 bits access
  unsigned long       l;          // 32 bits access
	} PIC24_RTCC_TIME;


/**FABRIZIO*/    
void loadImpostazioni(void);
void initSetting(void);
void portaFiltro(int tipo);
void manuale();
void caffe_1();
void caffe_2();


DWORD   PIC24RTCCGetTime( void );
DWORD   PIC24RTCCGetDate( void );
void    PIC24RTCCSetTime( WORD weekDay_hours, WORD minutes_seconds );
void    PIC24RTCCSetDate( WORD xx_year, WORD month_day );
void    UnlockRTCC( void );


BYTE to_bcd(BYTE );
BYTE from_bcd(BYTE );

void bumpClock(void);
BYTE GetKBchar(BYTE );
BYTE GetKBcharNext(void);

extern PIC24_RTCC_DATE currentDate;
extern PIC24_RTCC_TIME currentTime;

struct CONFIG_PARMS {
	//BYTE signature;		// 0x43 TOLTA con file system!
	BYTE tVibro1[2];			// tempo pre, tempo post
	BYTE tVibro2[2];
	WORD tMacinatura[6];			// 2x3
	WORD vMacinatura;
	BYTE mVibro1[6];		// 1 per ogni funzione per ogni vibro (2x3)
	BYTE mVibro2[6];		// 
	BYTE vVibro[2];			// 
	BYTE needPortafiltro,needSacchetto;
	BYTE mBilancia;
	BYTE test;
	BYTE repeat;			//0..36
	BYTE keyclick;
	BYTE viewingAngle;
	BYTE backLight,contrasto;
	BYTE setupEnabled,splash;
    BYTE lingua_default;        // AGGIUNTA FABRIZIO
#ifdef USA_ETHERNET
#ifdef STACK_USE_ICMP_SERVER
	BYTE EnablePing;
#endif
#ifdef STACK_USE_TELNET_SERVER
	BYTE EnableTelnet;
#endif
#ifdef STACK_USE_HTTP2_SERVER
	BYTE EnableWWW;
#endif
#if defined(STACK_USE_UART2TCP_BRIDGE) || defined(STACK_USE_UART2TCP_MIRROR)
	BYTE EnableUartBridge;
#endif
#endif
	};	




#define SCHERMATA_SPLASH 0
#define SCHERMATA_PRINCIPALE 1
//#define PRIMA_SCHERMATA_VISUALIZZAZIONE (SCHERMATA_PRINCIPALE)		// qua sono la stessa... non c'� il pre-menu
//#define ULTIMA_SCHERMATA_VISUALIZZAZIONE (PRIMA_SCHERMATA_PROGRAMMAZIONE-1)
#define PRIMA_SCHERMATA_PROGRAMMAZIONE 0
#define ULTIMA_SCHERMATA_PROGRAMMAZIONE (PRIMA_SCHERMATA_PROGRAMMAZIONE+32)		// 

enum MENU {
	MENU_IDLE=0,
	MENU_MAIN,
	MENU_VISUALIZZAZIONE,
	MENU_MANUTENZIONE_MENU,
	MENU_MANUTENZIONE,
	MENU_PROGRAMMAZIONE_MENU,
	MENU_PROGRAMMAZIONE
	};

struct LCD_ITEM {
	BYTE x,y;
	BYTE size;
	WORD valMin,valMax;
	WORD *var;
	BYTE varSize;
	char *name;
	char *unita;
	};
struct LCD_ITEM_GROUP {
	struct LCD_ITEM item[2];
	};

extern const struct LCD_ITEM lcdItems[ULTIMA_SCHERMATA_PROGRAMMAZIONE-PRIMA_SCHERMATA_PROGRAMMAZIONE];

extern WORD fontColor,bkColor,defaultFontColor,defaultBackColor;

enum OPERATION_STATE {
	STATE_IDLE=0,
	STATE_STARTING,
	STATE_VIBROPRE,
//	STATE_VIBRO1_MACINATURA,
	STATE_MACINATURA,
//	STATE_VIBRO2_MACINATURA,
	STATE_VIBROPOST,
	STATE_FINITO,
	STATE_OPERATION_TAZZINA=0,
	STATE_OPERATION_SACCHETTO=16,
	STATE_SETUP=32
	};

enum KEYBOARD_LAYOUT {
	KB_UNUSED=0,
	KB_DUMMY=0,

	KB_OK=1,			// row0:col0
	KB_MENU_ESC=2,
	KB_TAZZAPICCOLA=3,
	KB_TARA=5,

	KB_MENO=6,		// row0:col1
	KB_UP=7,
	KB_TAZZAMEDIA=8,
	KB_RPM=9,
	KB_SACCHETTOGRANDE=10,

	KB_PIU=11,		// row0:col2
	KB_DOWN=12,
	KB_TAZZAGRANDE=13,
	KB_TEMPERATURA=14,
	KB_SACCHETTOMEDIO=15,
		
								// row0:col3
	KB_BILANCIA=18,
	KB_MANUALE=19,
	KB_SACCHETTOPICCOLO=20
	
								// row0:col4

	};

/*FABRIZIO*/
extern RTC_CLOCK_CALENDAR rtc_time;
extern RTC_CLOCK_CALENDAR rtc_time_to_set;
