#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Torchio2_bentwood.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Torchio2_bentwood.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=boot_hal_pic24.c boot_load_hex.c boot_media_usb_msd.c TimeDelay.c interrupt.c driver_MV/interrupt_manager.c driver_MV/pin_manager.c driver_MV/uart1.c driver_MV/clock.c driver_MV/tmr1.c driver_MV/tmr2.c SSD1963.c gfxepmp.c Primitive.c EEFSIO.c FSIO.c SD-SPI.c USBFSIO.c swi2c.c usb_config.c ../Mcc18/Usb2x/usb_host.c ../Mcc18/Usb2x/usb_host_msd.c ../Mcc18/Usb2x/usb_host_msd_scsi.c InternalResource1.c RTC_i2c_MCP79400/i2c_func.c RTC_i2c_MCP79400/i2c_rtcc_func.c Communication_Keyboards.c loads.c main.c LCD.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/boot_hal_pic24.o ${OBJECTDIR}/boot_load_hex.o ${OBJECTDIR}/boot_media_usb_msd.o ${OBJECTDIR}/TimeDelay.o ${OBJECTDIR}/interrupt.o ${OBJECTDIR}/driver_MV/interrupt_manager.o ${OBJECTDIR}/driver_MV/pin_manager.o ${OBJECTDIR}/driver_MV/uart1.o ${OBJECTDIR}/driver_MV/clock.o ${OBJECTDIR}/driver_MV/tmr1.o ${OBJECTDIR}/driver_MV/tmr2.o ${OBJECTDIR}/SSD1963.o ${OBJECTDIR}/gfxepmp.o ${OBJECTDIR}/Primitive.o ${OBJECTDIR}/EEFSIO.o ${OBJECTDIR}/FSIO.o ${OBJECTDIR}/SD-SPI.o ${OBJECTDIR}/USBFSIO.o ${OBJECTDIR}/swi2c.o ${OBJECTDIR}/usb_config.o ${OBJECTDIR}/_ext/1010306976/usb_host.o ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o ${OBJECTDIR}/InternalResource1.o ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o ${OBJECTDIR}/Communication_Keyboards.o ${OBJECTDIR}/loads.o ${OBJECTDIR}/main.o ${OBJECTDIR}/LCD.o
POSSIBLE_DEPFILES=${OBJECTDIR}/boot_hal_pic24.o.d ${OBJECTDIR}/boot_load_hex.o.d ${OBJECTDIR}/boot_media_usb_msd.o.d ${OBJECTDIR}/TimeDelay.o.d ${OBJECTDIR}/interrupt.o.d ${OBJECTDIR}/driver_MV/interrupt_manager.o.d ${OBJECTDIR}/driver_MV/pin_manager.o.d ${OBJECTDIR}/driver_MV/uart1.o.d ${OBJECTDIR}/driver_MV/clock.o.d ${OBJECTDIR}/driver_MV/tmr1.o.d ${OBJECTDIR}/driver_MV/tmr2.o.d ${OBJECTDIR}/SSD1963.o.d ${OBJECTDIR}/gfxepmp.o.d ${OBJECTDIR}/Primitive.o.d ${OBJECTDIR}/EEFSIO.o.d ${OBJECTDIR}/FSIO.o.d ${OBJECTDIR}/SD-SPI.o.d ${OBJECTDIR}/USBFSIO.o.d ${OBJECTDIR}/swi2c.o.d ${OBJECTDIR}/usb_config.o.d ${OBJECTDIR}/_ext/1010306976/usb_host.o.d ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.d ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.d ${OBJECTDIR}/InternalResource1.o.d ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.d ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.d ${OBJECTDIR}/Communication_Keyboards.o.d ${OBJECTDIR}/loads.o.d ${OBJECTDIR}/main.o.d ${OBJECTDIR}/LCD.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/boot_hal_pic24.o ${OBJECTDIR}/boot_load_hex.o ${OBJECTDIR}/boot_media_usb_msd.o ${OBJECTDIR}/TimeDelay.o ${OBJECTDIR}/interrupt.o ${OBJECTDIR}/driver_MV/interrupt_manager.o ${OBJECTDIR}/driver_MV/pin_manager.o ${OBJECTDIR}/driver_MV/uart1.o ${OBJECTDIR}/driver_MV/clock.o ${OBJECTDIR}/driver_MV/tmr1.o ${OBJECTDIR}/driver_MV/tmr2.o ${OBJECTDIR}/SSD1963.o ${OBJECTDIR}/gfxepmp.o ${OBJECTDIR}/Primitive.o ${OBJECTDIR}/EEFSIO.o ${OBJECTDIR}/FSIO.o ${OBJECTDIR}/SD-SPI.o ${OBJECTDIR}/USBFSIO.o ${OBJECTDIR}/swi2c.o ${OBJECTDIR}/usb_config.o ${OBJECTDIR}/_ext/1010306976/usb_host.o ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o ${OBJECTDIR}/InternalResource1.o ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o ${OBJECTDIR}/Communication_Keyboards.o ${OBJECTDIR}/loads.o ${OBJECTDIR}/main.o ${OBJECTDIR}/LCD.o

# Source Files
SOURCEFILES=boot_hal_pic24.c boot_load_hex.c boot_media_usb_msd.c TimeDelay.c interrupt.c driver_MV/interrupt_manager.c driver_MV/pin_manager.c driver_MV/uart1.c driver_MV/clock.c driver_MV/tmr1.c driver_MV/tmr2.c SSD1963.c gfxepmp.c Primitive.c EEFSIO.c FSIO.c SD-SPI.c USBFSIO.c swi2c.c usb_config.c ../Mcc18/Usb2x/usb_host.c ../Mcc18/Usb2x/usb_host_msd.c ../Mcc18/Usb2x/usb_host_msd_scsi.c InternalResource1.c RTC_i2c_MCP79400/i2c_func.c RTC_i2c_MCP79400/i2c_rtcc_func.c Communication_Keyboards.c loads.c main.c LCD.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/Torchio2_bentwood.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24EP512GU810
MP_LINKER_FILE_OPTION=,-Tp24EP512GU810.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/boot_hal_pic24.o: boot_hal_pic24.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/boot_hal_pic24.o.d 
	@${RM} ${OBJECTDIR}/boot_hal_pic24.o.ok ${OBJECTDIR}/boot_hal_pic24.o.err 
	@${RM} ${OBJECTDIR}/boot_hal_pic24.o 
	@${FIXDEPS} "${OBJECTDIR}/boot_hal_pic24.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/boot_hal_pic24.o.d" -o ${OBJECTDIR}/boot_hal_pic24.o boot_hal_pic24.c    
	
${OBJECTDIR}/boot_load_hex.o: boot_load_hex.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/boot_load_hex.o.d 
	@${RM} ${OBJECTDIR}/boot_load_hex.o.ok ${OBJECTDIR}/boot_load_hex.o.err 
	@${RM} ${OBJECTDIR}/boot_load_hex.o 
	@${FIXDEPS} "${OBJECTDIR}/boot_load_hex.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/boot_load_hex.o.d" -o ${OBJECTDIR}/boot_load_hex.o boot_load_hex.c    
	
${OBJECTDIR}/boot_media_usb_msd.o: boot_media_usb_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/boot_media_usb_msd.o.d 
	@${RM} ${OBJECTDIR}/boot_media_usb_msd.o.ok ${OBJECTDIR}/boot_media_usb_msd.o.err 
	@${RM} ${OBJECTDIR}/boot_media_usb_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/boot_media_usb_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/boot_media_usb_msd.o.d" -o ${OBJECTDIR}/boot_media_usb_msd.o boot_media_usb_msd.c    
	
${OBJECTDIR}/TimeDelay.o: TimeDelay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TimeDelay.o.d 
	@${RM} ${OBJECTDIR}/TimeDelay.o.ok ${OBJECTDIR}/TimeDelay.o.err 
	@${RM} ${OBJECTDIR}/TimeDelay.o 
	@${FIXDEPS} "${OBJECTDIR}/TimeDelay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/TimeDelay.o.d" -o ${OBJECTDIR}/TimeDelay.o TimeDelay.c    
	
${OBJECTDIR}/interrupt.o: interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interrupt.o.d 
	@${RM} ${OBJECTDIR}/interrupt.o.ok ${OBJECTDIR}/interrupt.o.err 
	@${RM} ${OBJECTDIR}/interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/interrupt.o.d" -o ${OBJECTDIR}/interrupt.o interrupt.c    
	
${OBJECTDIR}/driver_MV/interrupt_manager.o: driver_MV/interrupt_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/interrupt_manager.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/interrupt_manager.o.ok ${OBJECTDIR}/driver_MV/interrupt_manager.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/interrupt_manager.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/interrupt_manager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/interrupt_manager.o.d" -o ${OBJECTDIR}/driver_MV/interrupt_manager.o driver_MV/interrupt_manager.c    
	
${OBJECTDIR}/driver_MV/pin_manager.o: driver_MV/pin_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/pin_manager.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/pin_manager.o.ok ${OBJECTDIR}/driver_MV/pin_manager.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/pin_manager.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/pin_manager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/pin_manager.o.d" -o ${OBJECTDIR}/driver_MV/pin_manager.o driver_MV/pin_manager.c    
	
${OBJECTDIR}/driver_MV/uart1.o: driver_MV/uart1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/uart1.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/uart1.o.ok ${OBJECTDIR}/driver_MV/uart1.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/uart1.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/uart1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/uart1.o.d" -o ${OBJECTDIR}/driver_MV/uart1.o driver_MV/uart1.c    
	
${OBJECTDIR}/driver_MV/clock.o: driver_MV/clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/clock.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/clock.o.ok ${OBJECTDIR}/driver_MV/clock.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/clock.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/clock.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/clock.o.d" -o ${OBJECTDIR}/driver_MV/clock.o driver_MV/clock.c    
	
${OBJECTDIR}/driver_MV/tmr1.o: driver_MV/tmr1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/tmr1.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/tmr1.o.ok ${OBJECTDIR}/driver_MV/tmr1.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/tmr1.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/tmr1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/tmr1.o.d" -o ${OBJECTDIR}/driver_MV/tmr1.o driver_MV/tmr1.c    
	
${OBJECTDIR}/driver_MV/tmr2.o: driver_MV/tmr2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/tmr2.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/tmr2.o.ok ${OBJECTDIR}/driver_MV/tmr2.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/tmr2.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/tmr2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/tmr2.o.d" -o ${OBJECTDIR}/driver_MV/tmr2.o driver_MV/tmr2.c    
	
${OBJECTDIR}/SSD1963.o: SSD1963.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/SSD1963.o.d 
	@${RM} ${OBJECTDIR}/SSD1963.o.ok ${OBJECTDIR}/SSD1963.o.err 
	@${RM} ${OBJECTDIR}/SSD1963.o 
	@${FIXDEPS} "${OBJECTDIR}/SSD1963.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/SSD1963.o.d" -o ${OBJECTDIR}/SSD1963.o SSD1963.c    
	
${OBJECTDIR}/gfxepmp.o: gfxepmp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/gfxepmp.o.d 
	@${RM} ${OBJECTDIR}/gfxepmp.o.ok ${OBJECTDIR}/gfxepmp.o.err 
	@${RM} ${OBJECTDIR}/gfxepmp.o 
	@${FIXDEPS} "${OBJECTDIR}/gfxepmp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/gfxepmp.o.d" -o ${OBJECTDIR}/gfxepmp.o gfxepmp.c    
	
${OBJECTDIR}/Primitive.o: Primitive.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Primitive.o.d 
	@${RM} ${OBJECTDIR}/Primitive.o.ok ${OBJECTDIR}/Primitive.o.err 
	@${RM} ${OBJECTDIR}/Primitive.o 
	@${FIXDEPS} "${OBJECTDIR}/Primitive.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/Primitive.o.d" -o ${OBJECTDIR}/Primitive.o Primitive.c    
	
${OBJECTDIR}/EEFSIO.o: EEFSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/EEFSIO.o.d 
	@${RM} ${OBJECTDIR}/EEFSIO.o.ok ${OBJECTDIR}/EEFSIO.o.err 
	@${RM} ${OBJECTDIR}/EEFSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/EEFSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/EEFSIO.o.d" -o ${OBJECTDIR}/EEFSIO.o EEFSIO.c    
	
${OBJECTDIR}/FSIO.o: FSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/FSIO.o.d 
	@${RM} ${OBJECTDIR}/FSIO.o.ok ${OBJECTDIR}/FSIO.o.err 
	@${RM} ${OBJECTDIR}/FSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/FSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/FSIO.o.d" -o ${OBJECTDIR}/FSIO.o FSIO.c    
	
${OBJECTDIR}/SD-SPI.o: SD-SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/SD-SPI.o.d 
	@${RM} ${OBJECTDIR}/SD-SPI.o.ok ${OBJECTDIR}/SD-SPI.o.err 
	@${RM} ${OBJECTDIR}/SD-SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/SD-SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/SD-SPI.o.d" -o ${OBJECTDIR}/SD-SPI.o SD-SPI.c    
	
${OBJECTDIR}/USBFSIO.o: USBFSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/USBFSIO.o.d 
	@${RM} ${OBJECTDIR}/USBFSIO.o.ok ${OBJECTDIR}/USBFSIO.o.err 
	@${RM} ${OBJECTDIR}/USBFSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/USBFSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/USBFSIO.o.d" -o ${OBJECTDIR}/USBFSIO.o USBFSIO.c    
	
${OBJECTDIR}/swi2c.o: swi2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/swi2c.o.d 
	@${RM} ${OBJECTDIR}/swi2c.o.ok ${OBJECTDIR}/swi2c.o.err 
	@${RM} ${OBJECTDIR}/swi2c.o 
	@${FIXDEPS} "${OBJECTDIR}/swi2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/swi2c.o.d" -o ${OBJECTDIR}/swi2c.o swi2c.c    
	
${OBJECTDIR}/usb_config.o: usb_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/usb_config.o.d 
	@${RM} ${OBJECTDIR}/usb_config.o.ok ${OBJECTDIR}/usb_config.o.err 
	@${RM} ${OBJECTDIR}/usb_config.o 
	@${FIXDEPS} "${OBJECTDIR}/usb_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/usb_config.o.d" -o ${OBJECTDIR}/usb_config.o usb_config.c    
	
${OBJECTDIR}/_ext/1010306976/usb_host.o: ../Mcc18/Usb2x/usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1010306976" 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host.o.ok ${OBJECTDIR}/_ext/1010306976/usb_host.o.err 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1010306976/usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/_ext/1010306976/usb_host.o.d" -o ${OBJECTDIR}/_ext/1010306976/usb_host.o ../Mcc18/Usb2x/usb_host.c    
	
${OBJECTDIR}/_ext/1010306976/usb_host_msd.o: ../Mcc18/Usb2x/usb_host_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1010306976" 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.ok ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.err 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.d" -o ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o ../Mcc18/Usb2x/usb_host_msd.c    
	
${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o: ../Mcc18/Usb2x/usb_host_msd_scsi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1010306976" 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.ok ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.err 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o ../Mcc18/Usb2x/usb_host_msd_scsi.c    
	
${OBJECTDIR}/InternalResource1.o: InternalResource1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/InternalResource1.o.d 
	@${RM} ${OBJECTDIR}/InternalResource1.o.ok ${OBJECTDIR}/InternalResource1.o.err 
	@${RM} ${OBJECTDIR}/InternalResource1.o 
	@${FIXDEPS} "${OBJECTDIR}/InternalResource1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/InternalResource1.o.d" -o ${OBJECTDIR}/InternalResource1.o InternalResource1.c    
	
${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o: RTC_i2c_MCP79400/i2c_func.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/RTC_i2c_MCP79400" 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.d 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.ok ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.err 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o 
	@${FIXDEPS} "${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.d" -o ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o RTC_i2c_MCP79400/i2c_func.c    
	
${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o: RTC_i2c_MCP79400/i2c_rtcc_func.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/RTC_i2c_MCP79400" 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.d 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.ok ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.err 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o 
	@${FIXDEPS} "${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.d" -o ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o RTC_i2c_MCP79400/i2c_rtcc_func.c    
	
${OBJECTDIR}/Communication_Keyboards.o: Communication_Keyboards.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Communication_Keyboards.o.d 
	@${RM} ${OBJECTDIR}/Communication_Keyboards.o.ok ${OBJECTDIR}/Communication_Keyboards.o.err 
	@${RM} ${OBJECTDIR}/Communication_Keyboards.o 
	@${FIXDEPS} "${OBJECTDIR}/Communication_Keyboards.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/Communication_Keyboards.o.d" -o ${OBJECTDIR}/Communication_Keyboards.o Communication_Keyboards.c    
	
${OBJECTDIR}/loads.o: loads.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/loads.o.d 
	@${RM} ${OBJECTDIR}/loads.o.ok ${OBJECTDIR}/loads.o.err 
	@${RM} ${OBJECTDIR}/loads.o 
	@${FIXDEPS} "${OBJECTDIR}/loads.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/loads.o.d" -o ${OBJECTDIR}/loads.o loads.c    
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o.ok ${OBJECTDIR}/main.o.err 
	@${RM} ${OBJECTDIR}/main.o 
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/main.o.d" -o ${OBJECTDIR}/main.o main.c    
	
${OBJECTDIR}/LCD.o: LCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCD.o.d 
	@${RM} ${OBJECTDIR}/LCD.o.ok ${OBJECTDIR}/LCD.o.err 
	@${RM} ${OBJECTDIR}/LCD.o 
	@${FIXDEPS} "${OBJECTDIR}/LCD.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/LCD.o.d" -o ${OBJECTDIR}/LCD.o LCD.c    
	
else
${OBJECTDIR}/boot_hal_pic24.o: boot_hal_pic24.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/boot_hal_pic24.o.d 
	@${RM} ${OBJECTDIR}/boot_hal_pic24.o.ok ${OBJECTDIR}/boot_hal_pic24.o.err 
	@${RM} ${OBJECTDIR}/boot_hal_pic24.o 
	@${FIXDEPS} "${OBJECTDIR}/boot_hal_pic24.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/boot_hal_pic24.o.d" -o ${OBJECTDIR}/boot_hal_pic24.o boot_hal_pic24.c    
	
${OBJECTDIR}/boot_load_hex.o: boot_load_hex.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/boot_load_hex.o.d 
	@${RM} ${OBJECTDIR}/boot_load_hex.o.ok ${OBJECTDIR}/boot_load_hex.o.err 
	@${RM} ${OBJECTDIR}/boot_load_hex.o 
	@${FIXDEPS} "${OBJECTDIR}/boot_load_hex.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/boot_load_hex.o.d" -o ${OBJECTDIR}/boot_load_hex.o boot_load_hex.c    
	
${OBJECTDIR}/boot_media_usb_msd.o: boot_media_usb_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/boot_media_usb_msd.o.d 
	@${RM} ${OBJECTDIR}/boot_media_usb_msd.o.ok ${OBJECTDIR}/boot_media_usb_msd.o.err 
	@${RM} ${OBJECTDIR}/boot_media_usb_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/boot_media_usb_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/boot_media_usb_msd.o.d" -o ${OBJECTDIR}/boot_media_usb_msd.o boot_media_usb_msd.c    
	
${OBJECTDIR}/TimeDelay.o: TimeDelay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TimeDelay.o.d 
	@${RM} ${OBJECTDIR}/TimeDelay.o.ok ${OBJECTDIR}/TimeDelay.o.err 
	@${RM} ${OBJECTDIR}/TimeDelay.o 
	@${FIXDEPS} "${OBJECTDIR}/TimeDelay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/TimeDelay.o.d" -o ${OBJECTDIR}/TimeDelay.o TimeDelay.c    
	
${OBJECTDIR}/interrupt.o: interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interrupt.o.d 
	@${RM} ${OBJECTDIR}/interrupt.o.ok ${OBJECTDIR}/interrupt.o.err 
	@${RM} ${OBJECTDIR}/interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/interrupt.o.d" -o ${OBJECTDIR}/interrupt.o interrupt.c    
	
${OBJECTDIR}/driver_MV/interrupt_manager.o: driver_MV/interrupt_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/interrupt_manager.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/interrupt_manager.o.ok ${OBJECTDIR}/driver_MV/interrupt_manager.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/interrupt_manager.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/interrupt_manager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/interrupt_manager.o.d" -o ${OBJECTDIR}/driver_MV/interrupt_manager.o driver_MV/interrupt_manager.c    
	
${OBJECTDIR}/driver_MV/pin_manager.o: driver_MV/pin_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/pin_manager.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/pin_manager.o.ok ${OBJECTDIR}/driver_MV/pin_manager.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/pin_manager.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/pin_manager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/pin_manager.o.d" -o ${OBJECTDIR}/driver_MV/pin_manager.o driver_MV/pin_manager.c    
	
${OBJECTDIR}/driver_MV/uart1.o: driver_MV/uart1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/uart1.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/uart1.o.ok ${OBJECTDIR}/driver_MV/uart1.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/uart1.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/uart1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/uart1.o.d" -o ${OBJECTDIR}/driver_MV/uart1.o driver_MV/uart1.c    
	
${OBJECTDIR}/driver_MV/clock.o: driver_MV/clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/clock.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/clock.o.ok ${OBJECTDIR}/driver_MV/clock.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/clock.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/clock.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/clock.o.d" -o ${OBJECTDIR}/driver_MV/clock.o driver_MV/clock.c    
	
${OBJECTDIR}/driver_MV/tmr1.o: driver_MV/tmr1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/tmr1.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/tmr1.o.ok ${OBJECTDIR}/driver_MV/tmr1.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/tmr1.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/tmr1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/tmr1.o.d" -o ${OBJECTDIR}/driver_MV/tmr1.o driver_MV/tmr1.c    
	
${OBJECTDIR}/driver_MV/tmr2.o: driver_MV/tmr2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/driver_MV" 
	@${RM} ${OBJECTDIR}/driver_MV/tmr2.o.d 
	@${RM} ${OBJECTDIR}/driver_MV/tmr2.o.ok ${OBJECTDIR}/driver_MV/tmr2.o.err 
	@${RM} ${OBJECTDIR}/driver_MV/tmr2.o 
	@${FIXDEPS} "${OBJECTDIR}/driver_MV/tmr2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/driver_MV/tmr2.o.d" -o ${OBJECTDIR}/driver_MV/tmr2.o driver_MV/tmr2.c    
	
${OBJECTDIR}/SSD1963.o: SSD1963.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/SSD1963.o.d 
	@${RM} ${OBJECTDIR}/SSD1963.o.ok ${OBJECTDIR}/SSD1963.o.err 
	@${RM} ${OBJECTDIR}/SSD1963.o 
	@${FIXDEPS} "${OBJECTDIR}/SSD1963.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/SSD1963.o.d" -o ${OBJECTDIR}/SSD1963.o SSD1963.c    
	
${OBJECTDIR}/gfxepmp.o: gfxepmp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/gfxepmp.o.d 
	@${RM} ${OBJECTDIR}/gfxepmp.o.ok ${OBJECTDIR}/gfxepmp.o.err 
	@${RM} ${OBJECTDIR}/gfxepmp.o 
	@${FIXDEPS} "${OBJECTDIR}/gfxepmp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/gfxepmp.o.d" -o ${OBJECTDIR}/gfxepmp.o gfxepmp.c    
	
${OBJECTDIR}/Primitive.o: Primitive.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Primitive.o.d 
	@${RM} ${OBJECTDIR}/Primitive.o.ok ${OBJECTDIR}/Primitive.o.err 
	@${RM} ${OBJECTDIR}/Primitive.o 
	@${FIXDEPS} "${OBJECTDIR}/Primitive.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/Primitive.o.d" -o ${OBJECTDIR}/Primitive.o Primitive.c    
	
${OBJECTDIR}/EEFSIO.o: EEFSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/EEFSIO.o.d 
	@${RM} ${OBJECTDIR}/EEFSIO.o.ok ${OBJECTDIR}/EEFSIO.o.err 
	@${RM} ${OBJECTDIR}/EEFSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/EEFSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/EEFSIO.o.d" -o ${OBJECTDIR}/EEFSIO.o EEFSIO.c    
	
${OBJECTDIR}/FSIO.o: FSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/FSIO.o.d 
	@${RM} ${OBJECTDIR}/FSIO.o.ok ${OBJECTDIR}/FSIO.o.err 
	@${RM} ${OBJECTDIR}/FSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/FSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/FSIO.o.d" -o ${OBJECTDIR}/FSIO.o FSIO.c    
	
${OBJECTDIR}/SD-SPI.o: SD-SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/SD-SPI.o.d 
	@${RM} ${OBJECTDIR}/SD-SPI.o.ok ${OBJECTDIR}/SD-SPI.o.err 
	@${RM} ${OBJECTDIR}/SD-SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/SD-SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/SD-SPI.o.d" -o ${OBJECTDIR}/SD-SPI.o SD-SPI.c    
	
${OBJECTDIR}/USBFSIO.o: USBFSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/USBFSIO.o.d 
	@${RM} ${OBJECTDIR}/USBFSIO.o.ok ${OBJECTDIR}/USBFSIO.o.err 
	@${RM} ${OBJECTDIR}/USBFSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/USBFSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/USBFSIO.o.d" -o ${OBJECTDIR}/USBFSIO.o USBFSIO.c    
	
${OBJECTDIR}/swi2c.o: swi2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/swi2c.o.d 
	@${RM} ${OBJECTDIR}/swi2c.o.ok ${OBJECTDIR}/swi2c.o.err 
	@${RM} ${OBJECTDIR}/swi2c.o 
	@${FIXDEPS} "${OBJECTDIR}/swi2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/swi2c.o.d" -o ${OBJECTDIR}/swi2c.o swi2c.c    
	
${OBJECTDIR}/usb_config.o: usb_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/usb_config.o.d 
	@${RM} ${OBJECTDIR}/usb_config.o.ok ${OBJECTDIR}/usb_config.o.err 
	@${RM} ${OBJECTDIR}/usb_config.o 
	@${FIXDEPS} "${OBJECTDIR}/usb_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/usb_config.o.d" -o ${OBJECTDIR}/usb_config.o usb_config.c    
	
${OBJECTDIR}/_ext/1010306976/usb_host.o: ../Mcc18/Usb2x/usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1010306976" 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host.o.ok ${OBJECTDIR}/_ext/1010306976/usb_host.o.err 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1010306976/usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/_ext/1010306976/usb_host.o.d" -o ${OBJECTDIR}/_ext/1010306976/usb_host.o ../Mcc18/Usb2x/usb_host.c    
	
${OBJECTDIR}/_ext/1010306976/usb_host_msd.o: ../Mcc18/Usb2x/usb_host_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1010306976" 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.ok ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.err 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/_ext/1010306976/usb_host_msd.o.d" -o ${OBJECTDIR}/_ext/1010306976/usb_host_msd.o ../Mcc18/Usb2x/usb_host_msd.c    
	
${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o: ../Mcc18/Usb2x/usb_host_msd_scsi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1010306976" 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.ok ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.err 
	@${RM} ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/_ext/1010306976/usb_host_msd_scsi.o ../Mcc18/Usb2x/usb_host_msd_scsi.c    
	
${OBJECTDIR}/InternalResource1.o: InternalResource1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/InternalResource1.o.d 
	@${RM} ${OBJECTDIR}/InternalResource1.o.ok ${OBJECTDIR}/InternalResource1.o.err 
	@${RM} ${OBJECTDIR}/InternalResource1.o 
	@${FIXDEPS} "${OBJECTDIR}/InternalResource1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/InternalResource1.o.d" -o ${OBJECTDIR}/InternalResource1.o InternalResource1.c    
	
${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o: RTC_i2c_MCP79400/i2c_func.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/RTC_i2c_MCP79400" 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.d 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.ok ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.err 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o 
	@${FIXDEPS} "${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o.d" -o ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_func.o RTC_i2c_MCP79400/i2c_func.c    
	
${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o: RTC_i2c_MCP79400/i2c_rtcc_func.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/RTC_i2c_MCP79400" 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.d 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.ok ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.err 
	@${RM} ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o 
	@${FIXDEPS} "${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o.d" -o ${OBJECTDIR}/RTC_i2c_MCP79400/i2c_rtcc_func.o RTC_i2c_MCP79400/i2c_rtcc_func.c    
	
${OBJECTDIR}/Communication_Keyboards.o: Communication_Keyboards.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Communication_Keyboards.o.d 
	@${RM} ${OBJECTDIR}/Communication_Keyboards.o.ok ${OBJECTDIR}/Communication_Keyboards.o.err 
	@${RM} ${OBJECTDIR}/Communication_Keyboards.o 
	@${FIXDEPS} "${OBJECTDIR}/Communication_Keyboards.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/Communication_Keyboards.o.d" -o ${OBJECTDIR}/Communication_Keyboards.o Communication_Keyboards.c    
	
${OBJECTDIR}/loads.o: loads.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/loads.o.d 
	@${RM} ${OBJECTDIR}/loads.o.ok ${OBJECTDIR}/loads.o.err 
	@${RM} ${OBJECTDIR}/loads.o 
	@${FIXDEPS} "${OBJECTDIR}/loads.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/loads.o.d" -o ${OBJECTDIR}/loads.o loads.c    
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o.ok ${OBJECTDIR}/main.o.err 
	@${RM} ${OBJECTDIR}/main.o 
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/main.o.d" -o ${OBJECTDIR}/main.o main.c    
	
${OBJECTDIR}/LCD.o: LCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCD.o.d 
	@${RM} ${OBJECTDIR}/LCD.o.ok ${OBJECTDIR}/LCD.o.err 
	@${RM} ${OBJECTDIR}/LCD.o 
	@${FIXDEPS} "${OBJECTDIR}/LCD.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DCFG_INCLUDE_EX16_ENC28 -DUSE_WF35 -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24F/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/h" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/generic/inc" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/peripheral_30F_24H_33F" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/lib" -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/include" -I"Configs" -I"Graphics" -I"." -I"../Mcc18/Usb2x" -I"../Mcc18/h/usb2x" -mlarge-code -mlarge-data -O1 -MMD -MF "${OBJECTDIR}/LCD.o.d" -o ${OBJECTDIR}/LCD.o LCD.c    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Torchio2_bentwood.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG  -o dist/${CND_CONF}/${IMAGE_TYPE}/Torchio2_bentwood.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}         -Wl,--defsym=__MPLAB_BUILD=1,--heap=2048,--stack=4096,-L".",-Map="${DISTDIR}/Torchio2_bentwood.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1
else
dist/${CND_CONF}/${IMAGE_TYPE}/Torchio2_bentwood.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf -mcpu=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Torchio2_bentwood.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}         -Wl,--defsym=__MPLAB_BUILD=1,--heap=2048,--stack=4096,-L".",-Map="${DISTDIR}/Torchio2_bentwood.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION)
	${MP_CC_DIR}\\pic30-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/Torchio2_bentwood.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -omf=elf
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
