/**
  Communication keyboards File

  @Company
    K-TRONIC

  @File Name
    Communication_Keyboards.c

  @Summary
    This is the C file regarding the management of serial monitor

  @Description
    This C file provides APIs for the management of serial monitor.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC24EP512GU810
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


//#include "mcc_generated_files/mcc.h"
#include "loads.h"
#include "Driver_MV/pin_manager.h"
#include "Driver_MV/tmr1.h"
#include "Driver_MV/tmr2.h"
#include "MainDemo.h"
#include "LCD.h"
#include "driver_MV/uart1.h"
#include "swi2c.h"
#include "Communication_Keyboards.h"

extern flagManuale;
extern unsigned short refresh_keyboard_read; 
extern UART_OBJECT uart1_obj ;
unsigned char Comm_State=0;
unsigned char Flag_EnableComm=0;
unsigned char val =  'F';
unsigned char old_val = 'E';
unsigned short Vread = 0;
BYTE test;
unsigned long int i =0 ;

extern char BUTTON[10];
extern char BUTTON_OLD[10];
extern flg_setting;
extern int premuti;
void EnableComm(unsigned char status){
    
    Flag_EnableComm = status;
}

/**/
void reset_keyboard(){
    LATBbits.LATB5 =0;
    __delay_ms(10);
    LATBbits.LATB5 =1;    
}

void CommStart(void)
{
    extern BYTE capAttivo;
    extern BYTE ultimoCapAttivo;
   // int _i1;
    int c;
    //char a[3] ="00";
    //unsigned char found= 0;
    char appoggio[30];     
    
  //      val = U1RXREG;
        
       
    
		switch(Comm_State) {
			case 0: 
                val = UART1_Read();
				switch(val) 
                {
                    //printf("\nOK"); 
					case 'Y':        
                          Config_LED();
    
                        for(i=0;i < sizeof(BUTTON);i++){
                            Enable_Led((char)(i+'0'),'0');
                            Enable_Led((char)( sizeof(BUTTON)-1-i+'0'),'1');
                        }
                       
                        Comm_State = 1;
                        
                    break;
 
                    default:
                        Comm_State = 0;
                      
				} 
            break;

			case 1:
                LATAbits.LATA2 = 1;
            //    #ifndef DEBUG_FAB
            //    UART1_Enable();
            //    #endif

                  Enable_Mec_Button();
                  if(capAttivo!=ultimoCapAttivo){    
                    if(capAttivo==1){
                        Enable_CapSense();
                        Disable_ALL_Led_White();
                    } 
                    else{
                        Disable_CapSense();
                        Disable_ALL_Led_Blue();
                        Enable_ALL_Led_White();
                    }
                    ultimoCapAttivo=capAttivo;
                 }else{
                      if(capAttivo==0){
                            Disable_ALL_Led_Blue();
                            Enable_ALL_Led_White();
                      }
                 }  
                //Disable_ALL_Led_White();
               // TMR1_Stop(); //reset all timer
                Comm_State = 2;

            break;
            
            case 2: // Button Reading    
           
                for(c=0;c< sizeof(BUTTON);c++){
                       BUTTON[c]='1';  
                }  
                if (refresh_keyboard_read==1){
	              if(buff_obj.type==  'W') {
                            if(capAttivo!=ultimoCapAttivo){        	
                                if(capAttivo==1){

                                     Enable_CapSense();
                                     Disable_ALL_Led_White();
                                } 
                                else{
                                          Disable_CapSense();
                                          Disable_ALL_Led_Blue();
                                          Enable_ALL_Led_White();
                                }
                                ultimoCapAttivo=capAttivo;
                                //Delay_mS(100);
                            }

                         if(buff_obj.button & 0x1){
                            BUTTON[0]='0';
                             premuti++;
                                val='0';
                         }
                        else
                            BUTTON[0]='1';
                        
                        if(buff_obj.button & 0x2){
                             BUTTON[1]='0';
                             premuti++;
                                if(premuti==1){
                                   ultimoCapAttivo=capAttivo;
                                   capAttivo=(capAttivo+1)%2;
                                }
                                val='1';
                            }
                        else
                            BUTTON[1]='1';
                        
                        if(buff_obj.button & 0x4){
                            BUTTON[2]='0';
                            premuti++;
                            val='2';            
							//Enable_CapSense();               
                        }
                        else
                            BUTTON[2]='1';
                        
                        if(buff_obj.button & 0x8){
                            BUTTON[3]='0';
                             premuti++;
                                val='3';
                        }
                        else
                            BUTTON[3]='1';
                        
                        if(buff_obj.button & 0x10){
                            BUTTON[4]='0';
                             premuti++;
                                val='4';
                        }
                        else
                            BUTTON[4]='1';
                        
                        if(buff_obj.button & 0x20){
                            BUTTON[5]='0';
                             premuti++;
                                val='5';
                        }
                        else
                            BUTTON[5]='1';
                        
                        
                        if(buff_obj.button & 0x40){
                            BUTTON[6]='0';
                             premuti++;
                                val='6';
                        }
                        else
                            BUTTON[6]='1';
                        
                        
                        if(buff_obj.button & 0x80){
                            BUTTON[7]='0';
                             premuti++;
                                val='7'; 
                        }
                        else
                            BUTTON[7]='1';
                            
                        if(buff_obj.button & 0x100){
                            BUTTON[8]='0';
                             premuti++;
                                val='8';
                        }
                        else
                            BUTTON[8]='1';    
                         
                        if(buff_obj.button & 0x200){
                            BUTTON[9]='0';
                             premuti++;
                                val='9';
                        }
                        else
                            BUTTON[9]='1';     
                    }
                   uart1_obj.rxStatus.s.full = false;
                   buff_obj.type   = 0;
                   buff_obj.lenght = 0;
                   buff_obj.button = 0x0000;
                   buff_obj.chKsum = 0;    
                   refresh_keyboard_read=0;
                }//end_if refresh_keyboard==1      
            
            break;
            default:
                    break;
        }//end_switch  comm_state
        if(flg_setting==FALSE){                
            gestioneMenu();
            sprintf(appoggio,"val %c N %u - CS %u",val,premuti,Comm_State);    
            LCDPutStringXY(2,6,appoggio);
        } else {//setup
                loadImpostazioni(); 
               }
        
}

void Enable_Led(unsigned char index, unsigned char type){
    
    //type='0' = blue
    //type='1' = white
    
    UART1_Write('S');
    UART1_Write(index);
    UART1_Write('D');
    UART1_Write('D');
    UART1_Write('D');
    UART1_Write('D');
    UART1_Write('o');
    UART1_Write(type);
    UART1_Write('H');
}

void Disable_Led(unsigned char index, unsigned char type){
    
    //type='0' = blue
    //type='1' = white
    
    UART1_Write('S');
    UART1_Write(index);
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('o');
    UART1_Write(type);
    UART1_Write('H');    
}

void Config_LED(void){
    //white
    UART1_Write('G');
    UART1_Write('2');
    UART1_Write('1');
    UART1_Write('i');
    UART1_Write('1');
    UART1_Write('H');
    
    //Blue
    UART1_Write('G');
    UART1_Write('2');
    UART1_Write('1');
    UART1_Write('i');
    UART1_Write('0');
    UART1_Write('H');
    
}

void Enable_ALL_Led_White(void){
    UART1_Write('N');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('p');
    UART1_Write('1');
    UART1_Write('H');
}

void Enable_ALL_Led_Blue(void){
    UART1_Write('N');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('p');
    UART1_Write('0');
    UART1_Write('H');
}

void Disable_ALL_Led_White(void){
    
    UART1_Write('N');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('p');
    UART1_Write('1');
    UART1_Write('H');
    
}

void Disable_ALL_Led_Blue(void){
    
    UART1_Write('N');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('p');
    UART1_Write('0');
    UART1_Write('H');
}

void Hys_CapSense(unsigned char val){
    char hex[2];
    sprintf(hex, "%x", val);
    
    UART1_Write('W');
    UART1_Write(hex[0]);
    UART1_Write(hex[1]);
    UART1_Write('H');
}

void Disable_CapSense(void){
    UART1_Write('C');
    UART1_Write('0');
}

void Enable_CapSense(void){
    UART1_Write('C');
    UART1_Write('1');
}

void Enable_Mec_Button(void){
    UART1_Write('M');
    UART1_Write('a');
}

void Disable_Mec_Button(void){
  UART1_Write('M');
  UART1_Write('b');
}

void Enable_Only_Mec_ButtonX(char tasto){
    UART1_Write('M');
    UART1_Write(tasto);
}