/******************************************************************************
 *
 *                Microchip Memory Disk Drive File System
 *
 ******************************************************************************
 * FileName:        EEFSIO.h
 * Dependencies:    GenericTypeDefs.h
 *                  FSconfig.h
 *                  FSDefs.h
 *                  stddef.h
 * Processor:       PIC18/PIC24/dsPIC30/dsPIC33/PIC32
 * Compiler:        C18/C30/C32
 * Company:         Microchip Technology, Inc.
 * Version:         1.4.0
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the �Company�) for its PICmicro� Microcontroller is intended and
 * supplied to you, the Company�s customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *

	22.9.18: G.Dar versione per EEprom I2C

 *
*****************************************************************************/

#ifndef  EE_FS_DOT_H
#define  EE_FS_DOT_H

#include "GenericTypeDefs.h"
#include "EEFSconfig.h"
#include "FSDefs.h" 
#include "stddef.h"

    #include    "swi2c.h"


/*******************************************************************/
/*                     Structures and defines                     */
/*******************************************************************/

#ifndef  FS_DOT_H			// se usato insieme a SD...

#ifndef FALSE
    // Summary: False value
    // Description:  This macro will indicate that a condition is false.
    #define FALSE   0
#endif
#ifndef TRUE
    // Summary: True value
    // Description: This macro will indicate that a condition is true.
    #define TRUE    !FALSE  // True value
#endif




#ifndef SEEK_SET
    // Summary: Macro for the FSfseek SEEK_SET base location.
    // Description: Functions as an input for FSfseek that specifies that the position in the file will be changed 
    //              relative to the beginning of the file.
    #define SEEK_SET 0

#endif
#ifndef SEEK_CUR

    // Summary: Macro for the FSfseek SEEK_CUR base location.
    // Description: Functions as an input for FSfseek that specifies that the position in the file will be changed
    //              relative to the current location of the file
    #define SEEK_CUR 1

#endif
#ifndef SEEK_END

    // Summary: Macro for the FSfseek SEEK_END base location
    // Description: Functions as an input for FSfseek that specifies that the position in the file will be changed
    //              relative to the end of the file.  For this macro, the offset value will be subtracted from
    //              the end location of the file by default.
    #define SEEK_END 2

#endif

// Summary: Macro for the FSfopen FS_APPEND mode
// Description: If this macro is specified as the mode argument in a call of FSfopen, the file being opened will
//              be created if it doesn't exist.  If it does exist, it's file information will be loaded and the
//              current location in the file will be set to the end.  The user will then be able to write to the file.
#define FS_APPEND   "a"

// Summary: Macro for the FSfopen FS_WRITE mode
// Description: If this macro is specified as the mode argument in a call of FSfopen, the file being opened will
//              be created if it doesn't exist.  If it does exist, it will be erased and replaced by an empty file
//              of the same name.  The user will then be able to write to the file.
#define FS_WRITE    "w"

// Summary: Macro for the FSfopen FS_READ mode
// Description: If this macro is specified as the mode argument in a call of FSfopen, the file information for the 
//              specified file will be loaded.  If the file does not exist, the FSfopen function will fail.  The user 
//              will then be able to read from the file.
#define FS_READ "r"

// Summary: Macro for the FSfopen FS_APPEND+ mode
// Description: If this macro is specified as the mode argument in a call of FSfopen, the file being opened will
//              be created if it doesn't exist.  If it does exist, it's file information will be loaded and the
//              current location in the file will be set to the end.  The user will then be able to write to the file
//              or read from the file.
#define FS_APPENDPLUS   "a+"

// Summary: Macro for the FSfopen FS_WRITE+ mode
// Description: If this macro is specified as the mode argument in a call of FSfopen, the file being opened will
//              be created if it doesn't exist.  If it does exist, it will be erased and replaced by an empty file
//              of the same name.  The user will then be able to write to the file or read from the file.
#define FS_WRITEPLUS    "w+"
#define FS_READWRITE    FS_WRITEPLUS			//GD 2018


// Summary: Macro for the FSfopen FS_READ+ mode
// Description: If this macro is specified as the mode argument in a call of FSfopen, the file information for the 
//              specified file will be loaded.  If the file does not exist, the FSfopen function will fail.  The user 
//              will then be able to read from the file or write to the file.
#define FS_READPLUS     "r+"

#ifndef intmax_t
    #ifdef __PIC24F__
        // Summary: A data type indicating the maximum integer size in an architecture
        // Description: The intmax_t data type refers to the maximum-sized data type on any given architecture.  This
        //              data type can be specified as a format specifier size specification for the FSfprintf function.
        #define intmax_t long long
    #elif defined __PIC24H__
        #define intmax_t long long
    #elif defined __dsPIC30F__
        #define intmax_t long long
    #elif defined __dsPIC33F__
        #define intmax_t long long
    #endif
#endif



// Summary:  Indicates flag conditions for a file object
// Description: The FILEFLAGS structure is used to indicate conditions in a file.  It contains three flags: 'write' indicates
//              that the file was opened in a mode that allows writes, 'read' indicates that the file was opened in a mode
//              that allows reads, and 'FileWriteEOF' indicates that additional data that is written to the file will increase
//              the file size.
typedef struct {
  unsigned    write :1;           // Indicates a file was opened in a mode that allows writes
  unsigned    read :1;            // Indicates a file was opened in a mode that allows reads
  unsigned    FileWriteEOF :1;    // Indicates the current position in a file is at the end of the file
	} FILEFLAGS;



// Summary: Indicates how to search for file entries in the FILEfind function
// Description: The values in the SEARCH_TYPE enumeration are used internally by the library to indicate how the FILEfind function
//              how to perform a search.  The 'LOOK_FOR_EMPTY_ENTRY' value indicates that FILEfind should search for an empty file entry.
//              The 'LOOK_FOR_MATCHING_ENTRY' value indicates that FILEfind should search for an entry that matches the FSFILE object
//              that was passed into the FILEfind function.
typedef enum {
  LOOK_FOR_EMPTY_ENTRY = 0,
  LOOK_FOR_MATCHING_ENTRY
	} SEARCH_TYPE;



// Summary: Macro indicating the length of a 8.3 file name
// Description: The TOTAL_FILE_SIZE_8P3 macro indicates the maximum number of characters in an 8.3 file name.  This value includes
//              8 characters for the name, three for the extentsion, and 1 for the radix ('.')
#define TOTAL_FILE_SIZE_8P3             (8+3+1)
#define TOTAL_FILE_SIZE                 TOTAL_FILE_SIZE_8P3


// Summary: A mask that indicates the limit of directory entries in a sector
// Description: The MASK_MAX_FILE_ENTRY_LIMIT_BITS is used to indicate to the Cache_File_Entry function that a new sector needs to
//              be loaded.
#define MASK_MAX_FILE_ENTRY_LIMIT_BITS          0x0f

// Summary: Value used for shift operations to calculate the sector offset in a directory
// Description: The VALUE_BASED_ON_ENTRIES_PER_CLUSTER macro is used to calculate sector offsets for directories.  The position of the
//              entry is shifted by 4 bits (divided by 16, since there are 16 entries in a sector) to calculate how many sectors a
//              specified entry is offset from the beginning of the directory.
#define VALUE_BASED_ON_ENTRIES_PER_CLUSTER      4

// Summary: A value that will indicate that a dotdot directory entry points to the root.
// Description: The VALUE_DOTDOT_CLUSTER_VALUE_FOR_ROOT macro is used as an absolute address when writing information to a dotdot entry
//              in a newly created directory.  If a dotdot entry points to the root directory, it's cluster value must be set to 0,
//              regardless of the actual cluster number of the root directory.
#define VALUE_DOTDOT_CLUSTER_VALUE_FOR_ROOT     0

// Summary: MAcro indicating the length of an 8.3 file name in a directory entry
// Description: The FILE_NAME_SIZE_8P3 macro indicates the number of characters that an 8.3 file name will take up when packed in
//              a directory entry.  This value includes 8 characters for the name and 3 for the extension.  Note that the radix is not
//              stored in the directory entry.
#define FILE_NAME_SIZE_8P3           11
#define FILE_NAME_SIZE               FILE_NAME_SIZE_8P3


// Summary: Contains file information and is used to indicate which file to access.
// Description: The FSFILE structure is used to hold file information for an open file as it's being modified or accessed.  A pointer to 
//              an open file's FSFILE structure will be passeed to any library function that will modify that file.
typedef struct {
  DISK    *       dsk;            // Pointer to a DISK structure
  DWORD           cluster;        // The first cluster of the file
  DWORD           ccls;           // The current cluster of the file
  WORD            sec;            // The current sector in the current cluster of the file
  WORD            pos;            // The position in the current sector
  DWORD           seek;           // The absolute position in the file
  DWORD           size;           // The size of the file
  FILEFLAGS       flags;          // A structure containing file flags
  WORD            time;           // The file's last update time
  WORD            date;           // The file's last update date
  char            name[FILE_NAME_SIZE_8P3];       // The short name of the file
  WORD            entry;          // The position of the file's directory entry in it's directory
  WORD            chk;            // File structure checksum
  WORD            attributes;     // The file attributes
  DWORD           dirclus;        // The base cluster of the file's directory
  DWORD           dirccls;        // The current cluster of the file's directory
	} FSFILE;

/* Summary: Possible results of the FSGetDiskProperties() function.
** Description: See the FSGetDiskProperties() function for more information.
*/
typedef enum {
  FS_GET_PROPERTIES_NO_ERRORS = 0,
  FS_GET_PROPERTIES_DISK_NOT_MOUNTED,
  FS_GET_PROPERTIES_CLUSTER_FAILURE,
  FS_GET_PROPERTIES_STILL_WORKING = 0xFF
	} FS_DISK_ERRORS;


/* Summary: Contains the disk search information, intermediate values, and results
** Description: This structure is used in conjunction with the FSGetDiskProperties()
**              function.  See that function for more information about the usage.
*/
typedef struct {
  DISK *  disk;           /* pointer to the disk we are searching */
  BOOL    new_request;    /* is this a new request or a continued request */
  FS_DISK_ERRORS properties_status;  /* status of the last call of the function */

  struct {
    BYTE disk_format;           /* disk format: FAT12, FAT16, FAT32 */
    WORD sector_size;           /* sector size of the drive */
    BYTE sectors_per_cluster;   /* number of sectors per cluster */
    DWORD total_clusters;       /* the number of total clusters on the drive */
    DWORD free_clusters;        /* the number of free (unused) clusters on drive */
  	} results;                      /* the results of the current search */

  struct {
    DWORD   c;     
    DWORD   curcls;
    DWORD   EndClusterLimit;
    DWORD   ClusterFailValue;
  	} private;      /* intermediate values used to continue searches.  This
                       member should be used only by the FSGetDiskProperties()
                       function */

	} FS_DISK_PROPERTIES;

// Summary: A structure used for searching for files on a device.
// Description: The SearchRec structure is used when searching for file on a device.  It contains parameters that will be loaded with
//              file information when a file is found.  It also contains the parameters that the user searched for, allowing further
//              searches to be perfomed in the same directory for additional files that meet the specified criteria.
typedef struct {
  char            filename[FILE_NAME_SIZE_8P3 + 2];   // The name of the file that has been found
  unsigned char   attributes;                     // The attributes of the file that has been found
  unsigned long   filesize;                       // The size of the file that has been found
  unsigned long   timestamp;                      // The last modified time of the file that has been found (create time for directories)
  unsigned int    entry;                          // The directory entry of the last file found that matches the specified attributes. (Internal use only)
  char            searchname[FILE_NAME_SIZE_8P3 + 2]; // The 8.3 format name specified when the user began the search. (Internal use only)
  unsigned char   searchattr;                     // The attributes specified when the user began the search. (Internal use only)
  unsigned long   cwdclus;                        // The directory that this search was performed in. (Internal use only)
  unsigned char   initialized;                    // Check to determine if the structure was initialized by FindFirst (Internal use only)
	} SearchRec;

#endif


/***************************************************************************
* Prototypes                                                               *
***************************************************************************/

int EEFSInit(void);

FSFILE *EEFSfopen(const char *fileName, const char *mode);

#ifdef EE_ALLOW_PGMFUNCTIONS
    FSFILE *EEFSfopenpgm(const rom char * fileName, const rom char *mode);
    int EEFindFirstpgm (const rom char * fileName, unsigned int attr, SearchRec * rec);
    int EEFSchdirpgm(const rom char * path);

    #ifdef EE_ALLOW_WRITES
        int EEFSremovepgm (const rom char * fileName);
        int EEFSmkdirpgm (const rom char * path);
        int EEFSrmdirpgm (const rom char * path, unsigned char rmsubdirs);
        int EEFSrenamepgm(const rom char * fileName, FSFILE * fo);
    #endif
#endif


int EEFSfclose(FSFILE *fo);

void EEFSrewind(FSFILE *fo);
size_t EEFSfread(void *ptr, size_t size, size_t n, FSFILE *stream);
int EEFSfseek(FSFILE *stream, long offset, int whence);
long EEFSftell(FSFILE *fo);
int EEFSfeof(FSFILE * stream );

#ifdef EE_ALLOW_FORMATS
int EEFSformat(char mode, long int serialNumber, char * volumeID);
#endif


#ifdef EE_ALLOW_WRITES

int EEFSattrib(FSFILE * file, unsigned char attributes);

int EEFSrename(const char * fileName, FSFILE * fo);
int EEFSremove(const char * fileName);

size_t EEFSfwrite(const void *data_to_write, size_t size, size_t n, FSFILE *stream);
int EEFSputc(char c, FSFILE *stream);
int EEFSgetc(FSFILE *stream);

#endif

#ifdef EE_ALLOW_DIRS

int EEFSchdir(char * path);
char *EEFSgetcwd(char * path, int numbchars);

#ifdef EE_ALLOW_WRITES

int EEFSmkdir(char * path);
int EEFSrmdir(char * path, unsigned char rmsubdirs);

#endif

#endif

#ifdef USERDEFINEDCLOCK
int SetClockVars (unsigned int year, unsigned char month, unsigned char day, unsigned char hour, unsigned char minute, unsigned char second);
#endif


#ifdef EE_ALLOW_FILESEARCH
int EEFindFirst(const char * fileName, unsigned int attr, SearchRec * rec);
int EEFindNext(SearchRec * rec); 
#endif

#ifdef EE_ALLOW_FSFPRINTF
    #ifdef __18CXX
        int EEFSfprintf(FSFILE *fptr, const rom char *fmt, ...);
    #else
        int EEFSfprintf(FSFILE *fptr, const char * fmt, ...);
    #endif
#endif

int EEFSerror(void);

int EEFSCreateMBR(unsigned long firstSector, unsigned long numSectors);


#ifdef EE_ALLOW_GET_DISK_PROPERTIES
void EEFSGetDiskProperties(FS_DISK_PROPERTIES *properties);
#endif

//static BYTE EEFILEget_next_cluster(FSFILE *fo, DWORD n);

#endif
