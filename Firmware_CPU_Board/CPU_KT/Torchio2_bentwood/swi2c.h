
char DoI2CMO(BYTE);
BYTE DoI2CMI(char);
void DoI2CStart(void);
void DoI2CStop(void);
void setClockHigh(void);
#define setDataOutput()	{ SPI_232_I_TRIS=0; }				// DATA e' output
#define setDataInput() { SPI_232_I_TRIS=1; }				// DATA e' input
	


BYTE I2CRX1Byte(void);
BYTE I2CRXByte(void);
BYTE I2CTXByte(BYTE );
BYTE I2CWritePage(BYTE,BYTE);
BYTE I2CWritePage16(WORD,BYTE);
BYTE I2CWritePagePoll(void);
BYTE I2CReadRandom(BYTE);
BYTE I2CReadRandom16(WORD);
BYTE I2CRead8Seq(BYTE,BYTE);
BYTE I2CRead16Seq(WORD,BYTE);
void I2CWriteRTC(BYTE ,BYTE );
BYTE I2CReadRTC(BYTE );
void I2CSTART(void);
void I2CSTOP(void);
BYTE I2CBITIN(void);
void I2CBITOUT(BYTE);
void I2CDelay(void);
void setClockHigh(void);
#define I2CTXSlaveAddrW() I2CTXByte(0xA0)
#define I2CTXSlaveAddrR() I2CTXByte(0xA1)

extern BYTE I2CBuffer[64];		// per scrivere cose grosse in EEprom... cross-page!

#define DELAY_SPI_FAST 5
#define Delay_SPI() __delay_us(DELAY_SPI_FAST /*DelaySPIVal*/)
#define Delay_1uS() __delay_us(1)
#define Delay_uS(x) __delay_us(x)
#define Delay_mS(x) __delay_ms(x)
#define Delay_1mS() Delay_mS(1)

void MDD_EEI2C_InitIO(void);
BYTE MDD_EEI2C_ShutdownMedia(void);
BYTE MDD_EEI2C_SectorRead(DWORD, BYTE *);
BYTE MDD_EEI2C_SectorWrite(DWORD, BYTE *, BYTE);
BYTE MDD_EEI2C_WriteProtectState(void);
MEDIA_INFORMATION *MDD_EEI2C_MediaInitialize(void);
