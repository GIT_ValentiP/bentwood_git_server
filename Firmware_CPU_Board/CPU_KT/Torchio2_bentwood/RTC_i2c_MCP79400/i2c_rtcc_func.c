/**********************************************************************************
// File Name    : i2c_rtcc_func.c 
// PIC24 family, C language. 
// Dependencies : p24fj128ga010.h, i2c_rtcc.h, i2c.h
// Processor    : PIC24FJ128GA010    
// Hardware     : Explorer 16 demo board, MCP79410 I2C RTCC
// I.D.E.       : MPLAB + C30 compiler v3.30b
// Company      : Microchip Technology , Inc. 
//.................................................................................
//                   SOFTWARE  LICENSE AGREEMENT    
//.................................................................................
// "Microchip Technology Inc." ("Microchip") licenses this software to you 
// solely for use with Microchip's Serial RTCC products.  
// The software is owned by Microchip and/or its licensors, and is protected 
// under applicable copyright laws.  All rights reserved.
// SOFTWARE IS PROVIDED "AS IS."  MICROCHIP AND ITS LICENSOR EXPRESSLY 
// DISCLAIM ANY WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING 
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS 
// FOR A PARTICULAR PURPOSE,OR NON-INFRINGEMENT. IN NO EVENT SHALL MICROCHIP 
// AND ITS LICENSORS BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR 
// CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, HARM TO YOUR EQUIPMENT, 
// COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY 
// CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE 
// THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER SIMILAR COSTS."
//.......................................................................................
* Author                    Date            Comment
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Alexandru Valeanu        	11/7/2011		Initial release for MCP79410 I2C RTCC    
*
****************************************************************************************/
#include "p24ep512gu810.h"
#include "i2c_rtcc.h"
#include "i2c.h"

//********************************************************************************************
//                         I2C RTCC DRIVERS 
//********************************************************************************************
extern unsigned char rtc_err_flg  ;   // the error flag will be used in this file 
RTC_CLOCK_CALENDAR rtc_time;
RTC_CLOCK_CALENDAR rtc_time_to_set;
//............................................................................................
// The below function writes a data byte in the I2C RTCC
//............................................................................................
void i2c_rtcc_wr(unsigned char rtcc_reg, unsigned char time_var){ // writes a data byte to the I2C RTCC   
i2c_start()             ;  // start I2C communication   
i2c_wr(ADDR_RTCC_WRITE) ;  // write DEVICE ADDR for RTCC WRITES
i2c_wr(rtcc_reg)        ;  // write the register's ADDRESS
i2c_wr(time_var)        ;  // write byte variable in the register 
i2c_stop()           ;  
}  // stop I2C communication
//..............................................................................................
// The below function reads a data byte from the I2C RTCC
//..............................................................................................
unsigned char i2c_rtcc_rd(unsigned char rtcc_reg){ // reads a data byte from the I2C RTCC
unsigned char rtcc_buf	;  // general data buffer for the i2c rtcc 
i2c_start()             ;  // start I2C communication
i2c_wr(ADDR_RTCC_WRITE) ;  // write DEVICE ADDR for RTCC WRITES
if(rtc_err_flg)                // if an error occured at a PICTAIL removal,
{ return rtcc_buf   ;   }  // leave fast the function 
i2c_wr(rtcc_reg)        ;  // write the register ADDRESS
if(rtc_err_flg)                // if an error occured at a PICTAIL removal, 
{ return rtcc_buf   ;   }  // leave fast the function    
i2c_restart()           ;  // RESTART for READS 
i2c_wr(ADDR_RTCC_READ)  ;  // send the DEVICE ADDRESS for RTCC READS.
if(rtc_err_flg)                // if an error occured at a PICTAIL removal,
{ return rtcc_buf   ;   }  // leave fast the function 
rtcc_buf=i2c_rd()       ;  // read register (stored in 'rtcc_buf')
i2c_nack()              ;  // NOACK from MASTER (last read byte)
i2c_stop()              ;  // stop I2C communication
return rtcc_buf       	;  // return the read byte, stored in the general rtcc buffer
}  
//................................................................................................
// The below function initializes the time/date variables, only if the oscillator is not yet running
void ini_i2c_time(void){             // initialization of time/date vars on the I2C RTCC
unsigned char day=0;   				 // local variable (stores the RTCC DAY register) 
day = i2c_rtcc_rd(ADDR_DAY);   		 // read day + OSCON bit 
if((day&OSCON)==OSCON) ;             // if oscillator = already running, do nothing. 
else{                                // if oscillator = not running, set time/date(arbitrary)
    i2c_rtcc_wr(ADDR_YEAR,0x19);         // initialize YEAR  register : (20)19           
    i2c_rtcc_wr(ADDR_MNTH,0x07);   	     // initialize MONTH register : july   
    i2c_rtcc_wr(ADDR_DATE,0x17);   	     // initialize DATE  register : date = 17  
    i2c_rtcc_wr(ADDR_HOUR,0x16);   	     // initialize HOUR  register : hour = 16 
    i2c_rtcc_wr(ADDR_MIN,0x00) ;   	     // initialize MIN   register : min  = 00  
    i2c_rtcc_wr(ADDR_SEC,START_32KHZ);   // init SEC register and start the 32khz oscillator 
}
} 

//................................................................................................
// The below function initializes the I2C RTCC
//................................................................................................
void ini_i2c_rtcc(void){             // initialization of the I2C RTCC: enables the battery circuit                                     // START bit is located in the Sec register
                                     // time/date will be set in 'ini_i2c_time()' 
unsigned char day=0; 				 // local variable (stores the RTCC DAY register)       
ini_i2c1()					      ;   // init the I2C module
day = i2c_rtcc_rd(ADDR_DAY); 		 // read day + OSCON bit   
i2c_rtcc_wr(ADDR_DAY,day|VBATEN);    // enable the battery back-up  
i2c_rtcc_wr(ADDR_CTRL,ALM_NO);  //original SQWE+ALM_NO+MFP_01H
                                      // square wave on MFP, no alarms, MFP = 1Hz(CONTROL REG)
//SI METTE SOLO LA PRIMA VOLTA CHE SI INIZIALIZZA 
//ini_i2c_time()				      ;   // init time: 1.11.2011, 00:00:00 AM  
}	

 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                    INCREMENT TIME/DATE VARIABLES
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void incr_yr(void)    {             // increments YEARS value (2 digits)
                                    // 99-->00 , yy-->y(y+1) , y9-->(y+1)0  
   if(rtc_time.year==0x99) {
       rtc_time.year = 0x00 ;   // 99-->00  
   }else if ((rtc_time.year&0x0f)==0x09){       // y9-->(y+1)0    
          rtc_time.year=(rtc_time.year+16)&0xf0  ; 
         }else { 
          rtc_time.year++       ; // yy-->y(y+1)
         }    
   
}    // display year value; turn back cursor, 2 positions
//................................................................................................
   void incr_mon(void) {             // increments MONTHS value 
       
   if(rtc_time.month>0x12) {
       rtc_time.month = 0x01  ;// just in case of an unexpected error 
   }    
    switch(rtc_time.month) {                    // LP bit was already masked at reads(display_time())
     case 0x09: {
                 rtc_time.month=0x10; // 09 --> 10  
                 break;
                }   
     case 0x12: {
                 rtc_time.month=0x01;// 12 --> 01 
                 break;
                }    
       default: {
                 rtc_time.month++   ;// in any other case, increment MONTH 
                 break;
                } 
               
    }  
      // display month value, turn back cursor 2 positions 
   }  
//................................................................................................
   void incr_dat(void) {             // increments DATE
   if(rtc_time.date>0x31) { 
       rtc_time.date = 0x01 ; // just in case of an unexpected error 
   }    
     switch(rtc_time.date) {                   // DATE has no auxiliary flags  
     case 0x31: {
                 rtc_time.date = 0x01;// overflow after 31 days 
                 break;
                }  
     case 0x09: {
                 rtc_time.date = 0x10;// 09 --> 10 (decimal) 
                 break;
                }  
     case 0x19: {
                 rtc_time.date = 0x20;// 19 --> 20 (decimal) 
                 break;
                }  
     case 0x29: {
                 rtc_time.date = 0x30;// 29 --> 30 )decimal) 
                 break;
                }  
     default:   {
                 rtc_time.date++;   // if no decimal overflow, incr DATE 
                 break;
                }
     } 
     // display date value, turn back cursor 2 positions  
   } 
//................................................................................................
   void incr_hr(void){               // increments HOURS value
   if(rtc_time.hour>0x23)  { 
       rtc_time.hour = 0x00 ;    
   }   // just in case of an unexpected error   
     switch(rtc_time.hour) {                    // none auxiliary control/status bits
     case 0x23: {
                 rtc_time.hour = 0x00;
                 break;
                }   // '24h' format, overflow after '23'
     case 0x19: {
                 rtc_time.hour = 0x20;
                 break;
                }   // 19 --> 20 
     case 0x09: {
                 rtc_time.hour = 0x10;
                 break;
                }   // 09 --> 10 
     default:   {
                 rtc_time.hour++;
                 break;
                }
     } // in any other case, increment value
    // display date value 
   } 
//................................................................................................
   void incr_min(void)               // increments MINUTES ('minutes' have no aux flags)  
   { 
     if(rtc_time.mins >= 0x59)  {rtc_time.mins=0x00;}    // overflow after 59 minutes
     else if ((rtc_time.mins&0x0f)==0x09) {     // m9 --> (m+1)0 
          rtc_time.mins = (rtc_time.mins+16)&0xf0 ; 
          }  
          else { rtc_time.mins++ ; }           // mm --> m(m+1) 
      // display new MINUTES value    
   }  
//................................................................................................ 
   void incr_sec(void)               // increments SECONDS ; bit7 = ST = 1 
  {
    rtc_time.sec = rtc_time.sec & (~START_32KHZ)    ;  // clear START bit 
    if(rtc_time.sec >= 0x59)  { 
        rtc_time.sec=0x00 ; 
    }  // overflow after 59 seconds 
     else if ((rtc_time.sec&0x0f)==0x09)      // s9 --> (s+1)0 
          { 
           rtc_time.sec = (rtc_time.sec+16)&0xf0 ;
          }  
          else { 
                rtc_time.sec++ ;  // ss --> s(s+1)  
               }          
    // display new SECONDS value    
  }  
  
  //******************************************************************************************************
//                           LCD HIGH LEVEL FUNCTIONS 
//.................................................................................................                         
void sec_to_string(void)  {              // displays SECONDS
  rtc_time.sec_str[0]=(((rtc_time.sec&(~START_32KHZ))>>4)+ASCII_DIGIT) ; 
                                        // mask the START bit, extract MS digit by shifting
                                        // right 4 times,  add the ASCII digit offset
  
  rtc_time.sec_str[1]=((rtc_time.sec&0x0f)+ASCII_DIGIT); // extract LS digit, add offset  
  rtc_time.sec_str[2]='\0';
 }   
//.................................................................................................                                         
void min_to_string(void) {               // displays MINUTES
 rtc_time.mins_str[0]=((rtc_time.mins>>4)+ASCII_DIGIT)    ; // extract MS digit for minutes  
                                        // minutes have no control bits, no mask needed 
   rtc_time.mins_str[1]=((rtc_time.mins&0x0f)+ASCII_DIGIT); 
   rtc_time.mins_str[2]='\0';
} // extract LS digit for minutes                    
 //................................................................................................
void  hr_to_string(void){                // displays HOURS
  rtc_time.hour_str[0]=(((rtc_time.hour&(~HOUR_12))>>4)+ASCII_DIGIT) ; // mask the 12 hours format,
                                        //extract MS digit by shifting right 4 times 
                                        // add the ASCII digit offset
   rtc_time.hour_str[1]=((rtc_time.hour&0x0f)+ASCII_DIGIT) ; 
   rtc_time.hour_str[2]='\0';
} // extract LS digit, add offset           
 //................................................................................................
void dat_to_string(void){                // displays DATE
  rtc_time.date_str[0]=(((rtc_time.date&0x3f)>>4)+ASCII_DIGIT) ; // extract MS digit by shifting
                                        // right 4 times, add ASCII offset 
  rtc_time.date_str[1]=((rtc_time.date&0x0f)+ASCII_DIGIT); 
  rtc_time.date_str[2]='\0';
} // extract LS digit, add offset       
//.................................................................................................                                           
void mon_to_string(void){                // displays MONTH
  rtc_time.month_str[0]=(((rtc_time.month&0x1f)>>4)+ASCII_DIGIT) ; // mask leap year, extract MS digit
                                        // by shifting right 4 times 
                                        // add  the ASCII digit offset 
  rtc_time.month_str[1]=((rtc_time.month&0x0f)+ASCII_DIGIT); 
  rtc_time.month_str[2]='\0';
   
} // extract LS digit, add offset  
//.................................................................................................
void  yr_to_string(void){                // displays YEAR      
  rtc_time.year_str[0]='2';  
  
  rtc_time.year_str[1]='0';
  
  rtc_time.year_str[2]=((rtc_time.year>>4)+ASCII_DIGIT)     ; // extract MS digit by shifting right 4 times,
                                        // add ASCII offset 
  rtc_time.year_str[3]=((rtc_time.year&0x0f)+ASCII_DIGIT) ; // extract LS digit, add ASCII offset
  rtc_time.year_str[4]='\0';
 
  
} 

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                 REAL TIME CLOCK   UPDATE DATE/TIME  FUNCTION 
//................................................................................................    
void rtc_update(void)      {  // DISPLAYS ALL TIME/DATE VARIABLES : 
                                     // YEAR, MONTH, DATE, HOUR, MINUTES, SECONDS  
    rtc_time.year  = i2c_rtcc_rd(ADDR_YEAR)  ;  // read YEAR
    if(rtc_err_flg)          return   ;  // if an error occured at a PICTAIL removal,
                                     // leave fast the interrupt routine
    yr_to_string();
    
    rtc_time.month = i2c_rtcc_rd(ADDR_MNTH)  ;  // read MONTH 
    if(rtc_err_flg) return            ;  // if error, leave fast the interrupt routine 
    rtc_time.month = rtc_time.month & (~LP)             ;  // mask the leap year bit 
    mon_to_string();
    
    rtc_time.date = i2c_rtcc_rd(ADDR_DATE)  ;  // read DATE 
    if(rtc_err_flg)          return   ;  // if error, leave fast the interrupt routine
    dat_to_string();
    
    rtc_time.hour  = i2c_rtcc_rd(ADDR_HOUR)  ;  // read HOUR
    if(rtc_err_flg)          return   ;  // if error, leave fast the interrupt routine        
    hr_to_string();
    
    rtc_time.mins = i2c_rtcc_rd(ADDR_MIN)   ;  // read MIN
    if(rtc_err_flg)          return   ;  // if error, leave fast the interrupt routine 
    min_to_string();
    
    rtc_time.sec = (i2c_rtcc_rd(ADDR_SEC) & ~START_32KHZ) ;  // read SEC 
    
    
    
    if(rtc_err_flg)          return   ;  // if error, leave fast the interrupt routine  
    sec_to_string();

}  


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                 REAL TIME CLOCK   SET NEW DATA
//................................................................................................    
void rtc_set_new_data(void)      {  
// DISPLAYS ALL TIME/DATE VARIABLES : 
rtc_err_flg = 0x00                    ;   // clear error variable 
unsigned char value_BCD=0;
unsigned char value_to_convert=0;

value_to_convert= rtc_time_to_set.year;
value_BCD=(((value_to_convert/10)<<4)+(value_to_convert %10));
i2c_rtcc_wr(ADDR_YEAR,value_BCD)         ;   // update YEAR    value in RTCC
value_to_convert= rtc_time_to_set.month;
value_BCD=(((value_to_convert/10)<<4)+(value_to_convert %10));
i2c_rtcc_wr(ADDR_MNTH,value_BCD)        ;   // update MONTH   value in RTCC
value_to_convert= rtc_time_to_set.date;
value_BCD=(((value_to_convert/10)<<4)+(value_to_convert %10));                                      // LP bit is read only; you may clear it. 
i2c_rtcc_wr(ADDR_DATE,value_BCD)        ;   // update DATE    value in RTCC
value_to_convert= rtc_time_to_set.hour;
value_BCD=(((value_to_convert/10)<<4)+(value_to_convert %10));
i2c_rtcc_wr(ADDR_HOUR,value_BCD)         ;   // update HOUR    value in RTCC                
value_to_convert= rtc_time_to_set.mins;
value_BCD=(((value_to_convert/10)<<4)+(value_to_convert %10));
i2c_rtcc_wr(ADDR_MIN,value_BCD)         ;   // update MINUTES value in RTCC
value_to_convert= rtc_time_to_set.sec;
value_BCD=(((value_to_convert/10)<<4)+(value_to_convert %10));
value_BCD= START_32KHZ           ;   // restore oscillator START bit
i2c_rtcc_wr(ADDR_SEC,value_BCD)         ;   // update SECONDS value in RTCC   
//delayms(200)                      ;   // delay to avoid keyboard debounce 
}