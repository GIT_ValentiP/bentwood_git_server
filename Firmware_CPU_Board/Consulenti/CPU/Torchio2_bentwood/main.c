/******************************************************************************
 * FileName:        MainDemo.c
 * Dependencies:    see included files below
 * Processor:       PIC24F, PIC24H, dsPIC, PIC32
 * Compiler:        C30 v3.25/C32 v0.00.18
 * Company:         Microchip Technology, Inc.
 * Software License Agreement
 *
 * Copyright (c) 2011 Microchip Technology Inc.  All rights reserved.
 * Microchip licenses to you the right to use, modify, copy and distribute
 * Software only when embedded on a Microchip microcontroller or digital
 * signal controller, which is integrated into your product or third party
 * product (pursuant to the sublicense terms in the accompanying license
 * agreement).  
 *
 * You should refer to the license agreement accompanying this Software
 * for additional information regarding your rights and obligations.
 *
 *******************************************************************************/
/************************************************************
 * Includes
 ************************************************************/
#include <ctype.h>
#include <stdlib.h>
#include "MainDemo.h"

#include "Graphics/SSD1963.h"
// http://www.techtoys.com.hk/Displays/SSD1963EvalRev3B/SSD1963%20Eval%20Board%20Rev3B.htm


#include "ppsnew.h"
#include "Graphics\gfxpmp.h"
#include "sd-spi.h"
#include "swi2c.h"
#include <math.h>

#include "usb_config.h"
#include "usb.h"
#include "usb_common.h"
#include "usb_host_msd.h"
#include "usb_host_msd_scsi.h"
#include "FSIO.h"
#include "USBFSIO.h"
#include "EEFSIO.h"

#include "boot_config.h"
#include "driver_MV/uart1.h"
#include "driver_MV/clock.h"
#include "driver_MV/pin_manager.h"
#include "driver_MV/tmr1.h"
#include "driver_MV/tmr2.h"
#include "Communication_Keyboards.h"
#include "LCD.h"

/************************************************************
 * Configuration Bits
 ************************************************************/
_FGS( GWRP_OFF & GSS_OFF & GSSK_OFF ) 
_FOSCSEL(FNOSC_PRIPLL & IESO_OFF)			//FNOSC_FRCPLL 
_FOSC( POSCMD_HS & OSCIOFNC_ON & IOL1WAY_ON & FCKSM_CSDCMD )
_FWDT( WDTPOST_PS32768 & WDTPRE_PR32 & PLLKEN_ON & WINDIS_OFF & FWDTEN_OFF )
_FPOR( FPWRT_PWR8 & BOREN_ON & ALTI2C1_OFF & ALTI2C2_OFF )
_FICD( ICS_PGD3 & RSTPRI_PF & JTAGEN_OFF )
_FAS( AWRP_OFF & APL_OFF & APLK_OFF)
//_FUID0(x) simpa..

void SendLedState(char *);

char Index;  
char BUTTON[10];
char BUTTON_OLD[10] = "1111111111";
char LED_CMD[10] = "2222222222";
char Completed;
char StateRx;
char step;
char FirstPacket;
unsigned short DelayAfter;
unsigned short CycleTime;
unsigned short Cycle_1Tazza = 35;
unsigned short Cycle_2Tazza = 70;
unsigned char flag_nok_sd = 0;
unsigned char flag_ok_press = 0;

const char CopyrString[]= {'O','A',' ','(','X','X','X','X','X','X','X','X','X',' ','(','X','X','X','X',')',')',' ','-',' ','K','-','T','r','o','n','i','c',' ','-',' ','B','X','X','X','w','X','X','X',' '
	,
#ifdef USA_BOOTLOADER 
	' ','B','L',
#endif
#ifdef USA_ETHERNET
	' ','E','T','H',
#endif
	' ','v',VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0',' ',
	'1','4','/','0','3','/','1','9', 0 };


/************************************************************
 * Defines
 ************************************************************/
#define	PutBmp8BPPExt(a,b,c,d) 	PutBmp8BPPExtFast(a,b,c,d)
#define VIBRO_PWM (95)
#define WAIT_UNTIL_FINISH(x)    while(!x)
#define DEMODELAY				1000
#define KBclear() {oldkbKeys[0]=kbKeys[0]=0;}
#define TRIGK 1

#define PWM_SPEED 50
#define PWM_PERIOD    (40000)    /* equivale a 1.7KHz */

#define RTCC_INTERRUPT_REGISTER IEC3
#define RTCC_INTERRUPT_VALUE    0x2000

/************************************************************
 * Function Prototypes
 ************************************************************/
int MostraSiNo(const char *);
int showAParm(BYTE ,char *);
int handleBIOS(BYTE);
int showBIOS(void);
int handleTastiera(void);

int FSReadLine(FSFILE *,char *);
int EEFSReadLine(FSFILE *,char *);
int getParmN(const char *,const char *,int *);
int getParmS(const char *,const char *,char *);
int FSwriteParmN(FSFILE *,const char *,int );
int FSwriteParmS(FSFILE *,const char *,const char *);
int EEFSwriteParmN(FSFILE *,const char *,int );
int EEFSwriteParmS(FSFILE *,const char *,const char *);
BYTE GetProfileInt(const char *gruppo,const char *chiave,int *value);
BYTE GetProfileString(const char *gruppo,const char *chiave,char *value);
BYTE WriteProfileInt(const char *gruppo,const char *chiave,int value);
BYTE WriteProfileString(const char *gruppo,const char *chiave,char *value);
BYTE cercaGruppo(FSFILE *,const char *);
signed char stricmp(const char *s,const char *d);
signed char strnicmp(const char *s,const char *d,int n);
void exportSettingsSD(void);
int exportSettingsUSB(void);
int importSettingsUSB(void);
DWORD copyFileUSBToSD(const char *,const char *);

void scanKBD(void);
BYTE checkKey(BYTE);


/************************************************************
 * Variables
 ************************************************************/
WORD curPosX=0,curPosY=0;
BYTE LCDX=0,LCDY=0,savedX=0,savedY=0;
BYTE kbKeys[MAX_TASTI_CONTEMPORANEI],oldkbKeys[MAX_TASTI_CONTEMPORANEI];
BYTE rowState[32];		//5x5 matrice; ora 10 tasti meccanici + 12 tasti touch letti da SPI
WORD FLAGS=0;		//b0=TRIGK, b7=inCtrl, b6=inShift, b5=cursorState, b8=inSetup
BYTE /*inSetup=FALSE,*/SDcardOK=FALSE;
BYTE menuLevel=0,currSchermata=SCHERMATA_SPLASH,currSchermItem=0,enterData=0,extendedMenu=0;
BYTE deviceAttached;
struct CONFIG_PARMS configParms;

char kbTempBuffer[4];

unsigned char DC = 0; // Dario
unsigned char RX_CH = 'T'; //0x55;

const char *string_disable="DISABLE",*string_enable="ENABLE";
const char *MENU_INI="menu.ini",*SETTINGS_INI="settings.ini",*FW_UPDATE=BOOT_FILE_NAME,*MENU_UPDATE="NEWMENUS";
const BYTE *myFont=NULL;
BYTE backLight=0;

WORD textColors[16]={BLACK,BRIGHTRED,BRIGHTGREEN,BRIGHTYELLOW,BRIGHTBLUE,BRIGHTMAGENTA,BRIGHTCYAN,WHITE,DARKGRAY,RED,GREEN,YELLOW,BLUE,MAGENTA,CYAN,LIGHTGRAY};
enum COLORI_TESTO { _TESTO_NERO,_TESTO_ROSSO,_TESTO_VERDE,_TESTO_GIALLO,_TESTO_BLU,_TESTO_MAGENTA,_TESTO_CIANO,_TESTO_BIANCO,_TESTOGRIGIO,_TESTO_ROSSOSCURO,_TESTO_VERDESCURO,_TESTO_GIALLOSCURO,_TESTO_BLUSCURO,_TESTO_MAGENTASCURO,_TESTO_CIANOSCURO,_TESTO_GRIGIOSCURO};
WORD fontColor,bkColor,defaultFontColor=WHITE,defaultBackColor=BLACK;			// sotto i default vengono riassegnati
BYTE _backcolor;	

BYTE State=0;			// enum 
BYTE currMenu=0;
BYTE statoPWM[2];
WORD tempoOperazione=0;
BYTE tipoOperazione=0;


WORD myOutChar(XCHAR ch) {		// (NO non-weak) per aggiungere mirror in RAM! e per gestire font ridefiniti
	int i,x;
// con questa siamo a 1.6mSec per char circa - 13/10/14



	SetArea(GetX(),GetY(),GetX()+getCharSizeX()-1,GetY()+getCharSizeY()-1);

//fare check, e cancellare il "sotto" solo se � cambiato??
	x=getSizeX();
	i=LCDX+LCDY*x;

	if(!(FLAGS & (1 << 8))) {		// per gestire sovrapposizione setup..

		}

//  SetColor((lcdAttrib[i] & 1) ? BLACK : WHITE);

	return subOutChar(ch);

	}

WORD subOutChar(XCHAR ch) {
	int i;
  WORD  temp;
  WORD  mask, restoremask;
  SHORT xCnt, yCnt, xFontSize, yFontSize;
	WORD *theFont;


 
  restoremask = 0x8000; // 
    
	i=getSizeX();
	i=LCDX+LCDY*i;

  // Pier
//	if(myFont==font32) {
//		yFontSize=32;
//		xFontSize=16;
//		theFont=(WORD *)&font32;
//		theFont+=((ch-' ')*32);
//		}
	if(myFont==font32) {
		yFontSize=16;
		xFontSize=16;
		theFont=(WORD *)&font32;
		theFont+=((ch-' ')*32);
		}
	else {
		yFontSize=16;
		xFontSize=12;
		theFont=(WORD *)&font16;
		theFont+=((ch-' ')*16);
		}

	WriteCommand(CMD_WR_MEMSTART);
	DisplayEnable();
	DisplaySetData(); 

  for(yCnt=0; yCnt<yFontSize; yCnt++) 
  {
      mask = 0;

      for(xCnt=0; xCnt<xFontSize; xCnt++) 
      {
        if(!mask) 
        {		// 
          temp = *theFont++;
				  temp=(LOBYTE(temp) << 8) | HIBYTE(temp);
          mask = restoremask;
        }
			
// provo con PutPixel ottimizzata, spostando setarea all'inizio

// Dario
        if(temp & mask) 
			     SetColor(BLACK); //SetColor(WHITE);
			  else
			     SetColor(WHITE); //SetColor(BLACK);

				WriteColor(_color);
              
        mask >>= 1;
      }
  }

		DisplayDisable();

  // move cursor
//  _cursorX = x - (!configParms.screenSize ? 16 : 12) /*boh...*/;

  // restore color


  return 1;
	}

WORD subOutCharTransparent(XCHAR ch) {
	int i,x,y;
  WORD  temp;
  WORD  mask, restoremask;
  SHORT xCnt, yCnt, xFontSize, yFontSize;
	WORD *theFont;


 
  restoremask = 0x8000; // 
    
	i=getSizeX();
	i=LCDX+LCDY*i;

	if(myFont==font32) {
		yFontSize=32;
		xFontSize=16;
		theFont=(WORD *)&font32;
		theFont+=((ch-' ')*32);
		}
	else {
		yFontSize=16;
		xFontSize=12;
		theFont=(WORD *)&font16;
		theFont+=((ch-' ')*16);
		}

  y = GetY();
  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    mask = 0;
    x = GetX();

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
				temp=(LOBYTE(temp) << 8) | HIBYTE(temp);
        mask = restoremask;
        }
			
// provo con PutPixel ottimizzata, spostando setarea all'inizio
      if(temp & mask) 
			  PutPixel(x,y);
              
      mask >>= 1;
			x++;
      }
		y++;
    }

  return 1;
	}

WORD OutCharRenderRAM(XCHAR ch, BYTE *theFont,BYTE reverse) {
  BYTE        temp;
  BYTE        mask;
  BYTE  restoremask;
  SHORT xCnt, yCnt, xFontSize, yFontSize;
    
  restoremask = 0x80; // POI PASSARE a word ?

// STRIMINZISCO per mancanza RAM! poi rimettere a posto (anche redef char)    
		yFontSize=8 /*16*/;
		xFontSize=6 /*12*/;

  if(IsDeviceBusy())
    return (0);

	WriteCommand(CMD_WR_MEMSTART);
	DisplayEnable();
	DisplaySetData(); 

  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont;
        mask = restoremask;
        }

			if(reverse) {
// o si potrebbe plottare sia neri che bianchi, evitando di sbianchettare prima... per� poi salta il superimpose con la grafica
	      if(temp & mask) 
				  SetColor(BLACK);
				else
				  SetColor(WHITE);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
				}
			else {
	      if(temp & mask) 
				  SetColor(WHITE);
				else
				  SetColor(BLACK);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
        }
            
      mask >>= 1;
      }

//... e 2x in verticale...
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
        mask = restoremask;
        }

			if(reverse) {
// o si potrebbe plottare sia neri che bianchi, evitando di sbianchettare prima... per� poi salta il superimpose con la grafica
	      if(temp & mask) 
				  SetColor(BLACK);
				else
				  SetColor(WHITE);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
				}
			else {
	      if(temp & mask) 
				  SetColor(WHITE);
				else
				  SetColor(BLACK);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
        }
            
      mask >>= 1;
      }

    }


  // move cursor
//  _cursorX = x - (!configParms.screenSize ? 8 : 6) /*boh...*/;
				DisplayDisable();

  return 1;
	}

void OutCharCursor(BYTE ch) {
  SHORT xCnt, yCnt, x, y, xFontSize, yFontSize;

int i;

	ch+=155;


	{
  WORD temp;
  WORD mask,restoremask;
	const WORD *theFont;

//fondamentalmente copiata da outchar e outcharrenderRAM, ma duplicata per i cursori
	if(myFont==font32) {
		yFontSize=32;
		xFontSize=16;
		theFont=(WORD *)&font32;
		theFont+=((ch-' ')*32);
		}
	else {
		yFontSize=16;
		xFontSize=12;
		theFont=(WORD *)&font16;
		theFont+=((ch-' ')*16);
		}

  restoremask = 0x8000;

  y = curPosY;
  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    x = curPosX;
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
				temp=(LOBYTE(temp) << 8) | HIBYTE(temp);
        mask = restoremask;
        }
			
      if(temp & mask) {
        PutPixel(x, y);
        }
              
      mask >>= 1;
      x++;
      }
    y++;
    }
	}
	}

void LCDPutChar(BYTE ch) {

  MoveTo(curPosX,curPosY);
	if(FLAGS & (1 << 4))
		drawCursor(0);
  switch(ch) {
		case CR:
			curPosX=GetRealOrgX(); LCDX=0;
			break;
		case LF:
			curPosY+=getCharSizeY();	LCDY++;
			// questo � SEMPRE 
			if(LCDY>=getSizeY()) {
//				LCDscrollY();
				LCDXY(LCDX,getSizeY()-1);
				}
			break;
		default:
			if(ch>=32 && ch<127) {
//			  SetColor(attribGrafici.reverse ? BLACK : WHITE);
//			  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
				myOutChar(ch);
printed:
				curPosX+=getCharSizeX();
				LCDX++;
				if(LCDX>=getSizeX()) {
					curPosX=GetRealOrgX();
					LCDX=0;
					curPosY+=getCharSizeY();
					LCDY++;
					if(LCDY>=getSizeY()) {
//							LCDscrollY();
						LCDXY(LCDX,getSizeY()-1);
						}
					}		//LCDX
				else if(LCDY==getSizeY() && LCDX==1) {		
//					LCDscrollY();
					LCDXY(0,getSizeY()-1);
					myOutChar(ch);
					LCDXY(1,getSizeY()-1);
					}
				}
			else if(ch>=128) {
//			  SetColor(attribGrafici.reverse ? BLACK : LIGHTGRAY);
//			  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
				myOutChar(ch);
				goto printed;
				}

			break;
		}
	}

void LCDPutString(const char *s) {

	if(FLAGS & (1 << 4))
		drawCursor(0);
	while(*s) {
	  MoveTo(curPosX,curPosY);
	  switch(*s) {
			case CR:
				curPosX=GetRealOrgX(); LCDX=0;
				break;
			case LF:
				curPosY+=getCharSizeY();	LCDY++;
				// questo � SEMPRE 
				if(LCDY>=getSizeY()) {
//					LCDscrollY();
					LCDXY(LCDX,getSizeY()-1);
					}
				break;
			default:
				if(((BYTE)*s)>=32 && ((BYTE)*s)<127) {
//				  SetColor(attribGrafici.reverse ? BLACK : WHITE);
//				  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
			  	myOutChar(*s);
printed:
					curPosX+=getCharSizeX();
					LCDX++;
					if(LCDX>=(getSizeX())) {
						curPosX=GetRealOrgX();
						LCDX=0;
						curPosY+=getCharSizeY();
						LCDY++;
						if(LCDY>=getSizeY()) {
//								LCDscrollY();
							LCDXY(LCDX,getSizeY()-1);
							}
						}		//LCDX
					else if(LCDY==getSizeY() && LCDX==1) {		
//						LCDscrollY();
						LCDXY(0,getSizeY()-1);
						myOutChar(*s);
						LCDXY(1,getSizeY()-1);
						}
					}
				else if(((BYTE)*s)>=128) {
//				  SetColor(attribGrafici.reverse ? BLACK : LIGHTGRAY);
//				  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
					myOutChar((BYTE)*s);									// 
					goto printed;
					}

				break;
			}
		s++;
		}
	}

void LCDCls(void) {

  SetColor(BLACK);
	ClearDevice();
	LCDHome();
	}

void LCDHome(void) {

	curPosX=GetRealOrgX();
	curPosY=GetRealOrgY();
	LCDX=LCDY=0;
	}

void LCDXY(BYTE x,BYTE y) {

	if(x>=getSizeX())
		x=getSizeX()-1;
	if(y>=getSizeY())
		y=getSizeY()-1;
	curPosX=GetRealOrgX()+(x*getCharSizeX());
	curPosY=GetRealOrgY()+(y*getCharSizeY());
  MoveTo(curPosX,curPosY);
	LCDX=x; LCDY=y;
	}

void LCDXYGraph(WORD x,WORD y) {			// da usare solo per label semplice

	if(x>=GetMaxX())
		x=GetMaxX()-1;
	if(y>=GetMaxY())
		y=GetMaxY()-1;
	curPosX=GetRealOrgX()+x;
	curPosY=GetRealOrgY()+y;
  MoveTo(curPosX,curPosY);
	LCDX=x/getCharSizeX(); LCDY=y/getCharSizeY();			// tanto per rimanere allineati!
	}

void LCDPutStringXY(BYTE x, BYTE y, const char *s) {
	
	LCDXY(x,y);
	LCDPutString(s);
	}

void LCDPutStringCenter(BYTE y,const char *text) {
  SHORT  width;

//	SetFont((void *)font);
//  width = GetTextWidth(text, (void *)font);
  width = strlen(text);

  LCDPutStringXY(((getSizeX()) - width) / 2, y, text);
	}

int MostraSiNo(const char *title) {
	int n=-1;
	
//  SetColor(BLUE);
//  ClearDevice();
//  SetColor(WHITE);
	LCDCls();

	LCDPutStringCenter(2,(char *)(title ? title : "Sei sicuro?"));
	LCDPutStringXY(4,4,"si");
	LCDPutStringXY(9,4,"no");

	do {
		scanKBD(); 
		__delay_ms(100);
		ClrWdt();
		if(FLAGS & (1 << TRIGK)) {
			if(kbKeys[0]==KB_OK)
				n=1;
			else if(kbKeys[0]==KB_MENU_ESC)
				n=0;
			}
		} while(n==-1);	

	return n;
	}

void drawCursor(BYTE m) {
	BYTE n;

	if(FLAGS & (1<<6) && FLAGS & (1<<7)) {
		n=4;
		}
	else if(FLAGS & (1<<6)) {
		n=5;
		}
	else if(FLAGS & (1<<7)) {
		n=3;
		}	
	else {
		n=2;
		}	
	if(m) {
	  SetColor(WHITE);
		OutCharCursor(n);	    
		FLAGS |= (1 << 4);
		}
	else {
		int i=LCDX+LCDY*getSizeX();
		clearChar();

		FLAGS &= ~(1 << 4);
		}

	}

void clearChar(void) {

  SetColor(BLACK);
	Bar(curPosX,curPosY,curPosX+getCharSizeX(),curPosY+getCharSizeY());
	}


void resetSettings(void) {
	FSFILE *f;
	char buf[64];

	configParms.backLight=1;
	configParms.splash=0;
	configParms.test=0;
	configParms.tVibro1[0]=5;
	configParms.tVibro1[1]=5;
	configParms.tVibro2[0]=10;
	configParms.tVibro2[1]=10;

	EEFSCreateMBR(1,254);

/*{
int i;
	I2CRead16Seq(0,16);
	sprintf(buf,"%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X \r\n",
	I2CBuffer[0],I2CBuffer[1],I2CBuffer[2],I2CBuffer[3],I2CBuffer[4],I2CBuffer[5],I2CBuffer[6],I2CBuffer[7],
		I2CBuffer[8],I2CBuffer[9],I2CBuffer[10],I2CBuffer[11]);
	LCDPutString(buf);
	for(i=54; i<512; i+=12) {
		I2CRead16Seq(i,16);
		sprintf(buf,"%u:%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\r\n",i,
		I2CBuffer[0],I2CBuffer[1],I2CBuffer[2],I2CBuffer[3],I2CBuffer[4],I2CBuffer[5],I2CBuffer[6],I2CBuffer[7],
			I2CBuffer[8],I2CBuffer[9],I2CBuffer[10],I2CBuffer[11]);
		LCDPutString(buf);
	}	
}
__delay_ms(4000);*/


//__delaywdt_ms(100);
__delay_ms(100);

	EEFSformat(1,0x4448,"bwEE");

	/*	{
int i;
char buf[64];
	for(i=512; i<700; i+=12) {
		I2CRead16Seq(i,16);
		sprintf(buf,"%u:%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\r\n",i,
		I2CBuffer[0],I2CBuffer[1],I2CBuffer[2],I2CBuffer[3],I2CBuffer[4],I2CBuffer[5],I2CBuffer[6],I2CBuffer[7],
			I2CBuffer[8],I2CBuffer[9],I2CBuffer[10],I2CBuffer[11]);
		LCDPutString(buf);
	}	
}*/

	EEFSInit();			// forza il Mount di nuovo
	__delay_ms(50);

//sprintf(buf,"M,err:%u ",EEFSerror());
//	LCDPutString(buf);
/*	f=EEFSfopen("settings.ini",FS_APPEND);
sprintf(buf,"F=%u,err:%u ",f,EEFSerror());
	LCDPutString(buf);
	if(f) {
	LCDPutString("creo file");
		EEFSfwrite("dario=1\r\n",9,1,f);
		EEFSfclose(f);
		}*/
//flushData();		// sembra che serva... v. sopra: aprire il file, o scorrere DIR... la prima non va la seconda s�

//__delay_ms(1000);

	}

void saveSettings(void) {
	int i;
	FSFILE *f;

  SetColor(BRIGHTRED);
	LCDPutStringCenter(6,"SAVING DATA..");

	f=EEFSfopen(SETTINGS_INI,FS_WRITE);
char buf[64];
sprintf(buf,"F=%u,err:%u ",f,EEFSerror());
//	LCDPutString(buf);
	if(f) {
		EEFSfwrite("[opzioni]\r\n",11,1,f);
		EEFSwriteParmN(f,"backlight",configParms.backLight);
		EEFSwriteParmN(f,"splash",configParms.splash);
		EEFSwriteParmN(f,"test",configParms.test);
		EEFSfwrite("[parametri]\r\n",13,1,f);
		EEFSwriteParmN(f,"TVibro1",configParms.tVibro1[0]);
		EEFSwriteParmN(f,"TVibro2",configParms.tVibro2[0]);
		EEFSfclose(f);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	}

void loadSettings(void) {
	

	configParms.backLight=1;
	configParms.splash=0;
	configParms.test=0;

	if(!EEFSInit()) {
		resetSettings();
		saveSettings();
		}

	}

void exportSettingsSD(void) {		// copia INI da EEprom a SD
	int ch;
	FSFILE *f1,*f2;

	f1=EEFSfopen(SETTINGS_INI,FS_READ);
	if(f1) {
		f2=FSfopen(SETTINGS_INI,FS_WRITE);
		if(f2) {
			while((ch=EEFSgetc(f1)) != EOF) {
				FSputc(ch,f2);
				}	
			FSfclose(f2);
			}
		EEFSfclose(f1);
		}
	}

int exportSettingsUSB(void) {		// copia INI da EEprom a USB
	int ch;
	FSFILE *f1,*f2;

	f1=EEFSfopen(SETTINGS_INI,FS_READ);
	if(f1) {
		f2=USBFSfopen(SETTINGS_INI,FS_WRITE);
		if(f2) {
			while((ch=EEFSgetc(f1)) != EOF) {
				USBFSputc(ch,f2);
				}	
			USBFSfclose(f2);
			}
		EEFSfclose(f1);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	return f1 && f2;
	}

int importSettingsUSB(void) {		// copia INI da USB a EEprom 
	int ch;
	FSFILE *f1,*f2;

	f1=USBFSfopen(SETTINGS_INI,FS_READ);
	if(f1) {
		f2=EEFSfopen(SETTINGS_INI,FS_WRITE);
		if(f2) {
			while((ch=USBFSgetc(f1)) != EOF) {
				EEFSputc(ch,f2);
				}	
			EEFSfclose(f2);
			}
		USBFSfclose(f1);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	return f1 && f2;
	}

DWORD copyFileUSBToSD(const char *source,const char *dest) {
	int ch;
	DWORD l=0;
	FSFILE *f1,*f2;

	f1=USBFSfopen(source,FS_READ);
	if(f1) {
		f2=FSfopen(dest,FS_WRITE);
		if(f2) {
			while((ch=USBFSgetc(f1)) != EOF) {
				FSputc(ch,f2);
				l++;
				}	
			FSfclose(f2);
			}
		USBFSfclose(f1);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	return l;
	}

int FSReadLine(FSFILE *f,char *buf) {
	int i,j=0;
	char ch;

  do {
		i=FSfread(&ch,1,1,f);
		if(i>0) {
			switch(ch) {
				case 13:		//Linux??
					break;
				case 10:
					i=0;
					break;
				default:
					buf[j++]=ch;
					break;
				}
			}
		} while(i>0);
	buf[j]=0;
	return j;
	}

int getParmN(const char *buf,const char *parm,int *val) {
	char *p;

	if((p=strchr(buf,'='))) {
		*p=0;
		if(!stricmp(buf,parm)) {
			if(val)
				*val=atoi(p+1);
			*p='=';
			return 1;
			}
		else			
			*p='=';
		}
	return 0;
	}

int getParmS(const char *buf,const char *parm,char *val) {
	char *p;

	if((p=strchr(buf,'='))) {
		*p=0;
		if(!stricmp(buf,parm)) {
			if(val)
				strcpy(val,p+1);
			*p='=';
			return 1;
			}
		else			
			*p='=';
		}
	return 0;
	}

int FSwriteParmN(FSFILE *f,const char *parm,int val) {

	FSfprintf(f,"%s=%d\r\n",parm,val);
	}

int FSwriteParmS(FSFILE *f,const char *parm,const char *val) {

	FSfprintf(f,"%s=%s\r\n",parm,val);
	}

int EEFSwriteParmN(FSFILE *f,const char *parm,int val) {

	EEFSfprintf(f,"%s=%d\r\n",parm,val);
	}

int EEFSwriteParmS(FSFILE *f,const char *parm,const char *val) {

	EEFSfprintf(f,"%s=%s\r\n",parm,val);
	}

BYTE GetProfileInt(const char *gruppo,const char *chiave,int *value) {
	FSFILE *f;
	char buf[64];
	int n;

	if(f=FSfopen(MENU_INI,FS_READ)) {
		if(cercaGruppo(f,gruppo)) {
			n=strlen(chiave);
			while(FSReadLine(f,buf)>0) {
				if(!strnicmp(buf,chiave,n)) {
					if(value)
						*value=atoi(buf+n+1);
					FSfclose(f);
					return 1;
					}	
				}	
			}
		FSfclose(f);
		}	
	return 0;
	}

BYTE GetProfileString(const char *gruppo,const char *chiave,char *value) {
	FSFILE *f;
	char buf[64];
	int n;

	if(f=FSfopen(MENU_INI,FS_READ)) {
		if(cercaGruppo(f,gruppo)) {
			n=strlen(chiave);
			while(FSReadLine(f,buf)>0) {
				if(!strnicmp(buf,chiave,n)) {
					if(value)
						strcpy(value,buf+n+1);
					FSfclose(f);
					return 1;
					}	
				}	
			}
		FSfclose(f);
		}	
	return 0;
	}

BYTE WriteProfileInt(const char *gruppo,const char *chiave,int value) {
	FSFILE *f;

	if(f=FSfopen(MENU_INI,FS_READWRITE)) {
		if(cercaGruppo(f,gruppo)) {
			FSwriteParmN(f,chiave,value);
			}
		FSfclose(f);
		}	

	return 0;
	}

BYTE WriteProfileString(const char *gruppo,const char *chiave,char *value) {
	FSFILE *f;

	if(f=FSfopen(MENU_INI,FS_READWRITE)) {
		if(cercaGruppo(f,gruppo)) {
			FSwriteParmS(f,chiave,value);
			}
		FSfclose(f);
		}	

	return 0;
	}

BYTE cercaGruppo(FSFILE *f,const char *gruppo) {
	char buf[64];
	int n;

	FSfseek(f,0,SEEK_SET);
	while(FSReadLine(f,buf)>0) {
		if(buf[0]=='[') {
			n=strlen(gruppo);
			if(!strnicmp(buf+1,gruppo,n)) {
				if(buf[n+1]==']') {
					return 1;
					}	
				}	
			}
		}
	}

signed char stricmp(const char *s,const char *d) {
/*
	while(*s && *d) {
		if(tolower(*s) != tolower(*d))
			return tolower(*s) - tolower(*d);
		s++;
		d++;
		ClrWdt();
		}
	return 0;
*/

  while(tolower(*s) == tolower(*d++)) {
		if(*s++ == '\0') {
			return 0;
			}
		}

	return *s - *d;
	}

signed char strnicmp(const char *s,const char *d,int n) {

  while(tolower(*s) == tolower(*d++) && n--) {
		if(*s++ == '\0') {
			return 0;
			}
		}

	return *s - *d;
	}



/****************************************************************************
  Function:
    DWORD   PIC24RTCCGetDate( void )

  Description:
    This routine reads the date from the RTCC module.

  Precondition:
    The RTCC module has been initialized.


  Parameters:
    None

  Returns:
    DWORD in the format <xx><YY><MM><DD>

  Remarks:
    To catch roll-over, we do two reads.  If the readings match, we return
    that value.  If the two do not match, we read again until we get two
    matching values.

    For the PIC32MX, we use library routines, which return the date in the
    PIC32MX format.
  ***************************************************************************/
DWORD PIC24RTCCGetDate( void ) {
  DWORD_VAL   date1;
  DWORD_VAL   date2;

  do {
    while (RCFGCALbits.RTCSYNC);

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 1;
    date1.w[1] = RTCVAL;
    date1.w[0] = RTCVAL;

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 1;
    date2.w[1] = RTCVAL;
    date2.w[0] = RTCVAL;

    } while (date1.Val != date2.Val);

  return date1.Val;
	}

/****************************************************************************
  Function:
    DWORD   PIC24RTCCGetTime( void )

  Description:
    This routine reads the time from the RTCC module.

  Precondition:
    The RTCC module has been initialized.

  Parameters:
    None

  Returns:
    DWORD in the format <xx><HH><MM><SS>

  Remarks:
    To catch roll-over, we do two reads.  If the readings match, we return
    that value.  If the two do not match, we read again until we get two
    matching values.

    For the PIC32MX, we use library routines, which return the time in the
    PIC32MX format.
  ***************************************************************************/
DWORD PIC24RTCCGetTime( void ) {
  DWORD_VAL   time1;
  DWORD_VAL   time2;

  do {
    while (RCFGCALbits.RTCSYNC);

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 0;
    time1.w[1] = RTCVAL;
    time1.w[0] = RTCVAL;

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 0;
    time2.w[1] = RTCVAL;
    time2.w[0] = RTCVAL;

    } while (time1.Val != time2.Val);

    return time1.Val;
	}

/****************************************************************************
  Function:
    void PIC24RTCCSetDate( WORD xx_year, WORD month_day )

  Description:
    This routine sets the RTCC date to the specified value.


  Precondition:
    The RTCC module has been initialized.

  Parameters:
    WORD xx_year    - BCD year in the lower byte
    WORD month_day  - BCD month in the upper byte, BCD day in the lower byte

  Returns:
    None

  Remarks:
    For the PIC32MX, we use library routines.
  ***************************************************************************/
void PIC24RTCCSetDate( WORD xx_year, WORD month_day ) {
  
	UnlockRTCC();
  RCFGCALbits.RTCPTR0 = 1;
  RCFGCALbits.RTCPTR1 = 1;
  RTCVAL = xx_year;
  RTCVAL = month_day;
	}

/****************************************************************************
  Function:
    void PIC24RTCCSetTime( WORD weekDay_hours, WORD minutes_seconds )

  Description:
    This routine sets the RTCC time to the specified value.

  Precondition:
    The RTCC module has been initialized.

  Parameters:
    WORD weekDay_hours      - BCD weekday in the upper byte, BCD hours in the
                                lower byte
    WORD minutes_seconds    - BCD minutes in the upper byte, BCD seconds in
                                the lower byte

  Returns:
    None

  Remarks:
    For the PIC32MX, we use library routines.
  ***************************************************************************/
void PIC24RTCCSetTime( WORD weekDay_hours, WORD minutes_seconds) {

  UnlockRTCC();
  RCFGCALbits.RTCPTR0 = 1;
  RCFGCALbits.RTCPTR1 = 0;
  RTCVAL = weekDay_hours;
  RTCVAL = minutes_seconds;
	}

/****************************************************************************
  Function:
    void UnlockRTCC( void )

  Description:
    This function unlocks the RTCC so we can write a value to it.

  Precondition:
    None

  Parameters:
    None

  Return Values:
    None

  Remarks:
    For the PIC32MX, we use library routines.
  ***************************************************************************/

void UnlockRTCC( void ) {
  BOOL interruptsWereOn;

  interruptsWereOn = FALSE;
  if((RTCC_INTERRUPT_REGISTER & RTCC_INTERRUPT_VALUE) == RTCC_INTERRUPT_VALUE) {
    interruptsWereOn = TRUE;
    RTCC_INTERRUPT_REGISTER &= ~RTCC_INTERRUPT_VALUE;
    }

  // Unlock the RTCC module
  __asm__ ("mov #NVMKEY,W0");
  __asm__ ("mov #0x55,W1");
  __asm__ ("mov #0xAA,W2");
  __asm__ ("mov W1,[W0]");
  __asm__ ("nop");
  __asm__ ("mov W2,[W0]");
  __asm__ ("bset RCFGCAL,#13");
  __asm__ ("nop");
  __asm__ ("nop");

  if(interruptsWereOn) {
    RTCC_INTERRUPT_REGISTER |= RTCC_INTERRUPT_VALUE;
    }
	}


BYTE to_bcd(BYTE n) {
	
	return (n % 10) | ((n / 10) << 4);
	}

BYTE from_bcd(BYTE n) {
	
	return (n & 15) + (10*(n >> 4));
	}
// ----------------------------------------------------------------------

unsigned char WriteSPI2M(unsigned char data_out) {

#ifdef __PIC32MX__
    BYTE   clear;
    putcSPI((BYTE)data_out);
    clear = getcSPI();
    return ( 0 );                // return non-negative#
#else
    BYTE   clear;
    SPI2BUF = data_out;          // write byte to SSP1BUF register
    while(!SPI2STAT_RBF); // wait until bus cycle complete
    clear = SPI2BUF;
    return 0;                // return non-negative#
#endif
	}

BYTE ReadSPI2M(void) {

#ifdef __C32__
    putcSPI((BYTE)0xFF);
    return (BYTE)getcSPI();
#else
    SPIBUF = 0xFF;                              //Data Out - Logic ones
    while(!SPISTAT_RBF);                     //Wait until cycle complete
    return(SPIBUF);                             //Return with byte read
#endif
	}

void scanKBD(void) {
	BYTE *p,n;
	BYTE i;
	BYTE *rowStatePtr;
	BYTE kbtemp;
	BYTE kbScanY;
	BYTE kbScanCode;		// il num. progressivo del tasto scandito


//return;  //FINIRE!

	ClrWdt();
//	memcpy(oldkbKeys,kbKeys,MAX_TASTI_CONTEMPORANEI);		meglio sopra...
	memset(kbKeys,0,MAX_TASTI_CONTEMPORANEI);


  SPI2CON1 = 0x0000;              // power on state
  SPI2CON1 |= MASTER_ENABLE_ON | PRI_PRESCAL_1_1 | SEC_PRESCAL_1_1;          // select serial mode 
  SPI2CON1bits.CKP = 1;
  SPI2CON1bits.CKE = 0;
  SPI2CLOCK = 0;
  SPI2OUT = 0;                  // define SDO1 as output (master or slave)
  SPI2IN = 1;                  // define SDI1 as input (master or slave)
  SPI2ENABLE = 1;             // enable synchronous serial port


	p=(BYTE *)rowState;

	SPI2_CS=0;
	WriteSPI2M(1);
	for(i=0; i<32; i++) {
		*p++=ReadSPI2M();
		}
	SPI2_CS=1;

	kbScanCode=1;			// parto da 1!! (se no lo zero=non valido taglierebbe via ROW0:COL0)
	for(i=0; i<10; i++) {					// analizzo i tasti meccanici
		ClrWdt();

		if(rowState[i])
			checkKey(kbScanCode);

		kbScanCode++;
		}			// for(rowStatePtr...

	if(memcmp(kbKeys,oldkbKeys,MAX_TASTI_CONTEMPORANEI))
		FLAGS |= (1 << TRIGK);

	return;


	TRISBbits.TRISB4=1;
	TRISBbits.TRISB5=1;
	TRISEbits.TRISE9=1;
	TRISEbits.TRISE8=1;
	TRISAbits.TRISA0=1;

	LATBbits.LATB3=0;		// prima, riscrivo output...
	LATBbits.LATB2=0;
	LATBbits.LATB1=0;
	LATBbits.LATB0=0;
	LATGbits.LATG13=0;

	TRISBbits.TRISB3=1;
	TRISBbits.TRISB2=1;
	TRISBbits.TRISB1=1;
	TRISBbits.TRISB0=1;
	TRISGbits.TRISG13=1;

	kbScanY=0b00011110;
	kbScanCode=1;			// parto da 1!! (se no lo zero=non valido taglierebbe via ROW0:COL0)
	

// faccio la scansione, scrivendo nelle colonne e leggendo dalle righe;
//		poi salvo i valori nell'array da 8 byte rowState

	do {

		switch(kbScanY) {
			case 0b00011110:
				TRISBbits.TRISB3=0;
				break;
			case 0b00011101:
				TRISBbits.TRISB2=0;
				break;
			case 0b00011011:
				TRISBbits.TRISB1=0;
				break;
			case 0b00010111:
				TRISBbits.TRISB0=0;
				break;
			case 0b00001111:
				TRISGbits.TRISG13=0;
				break;
			}

		__delay_us(2);									// ritardino...
		ClrWdt();

		n=0;
		n |= PORTBbits.RB4;
		n |= (PORTBbits.RB5 << 1);
		n |= (PORTEbits.RE9 << 2);
		n |= (PORTEbits.RE8 << 3);
		n |= (PORTAbits.RA0 << 4);

		*p++=n;

		kbScanY <<= 1;
		kbScanY |= 1;								// entra 1
		kbScanY &= 0b00011111;

		TRISBbits.TRISB3=1;
		TRISBbits.TRISB2=1;
		TRISBbits.TRISB1=1;
		TRISBbits.TRISB0=1;
		TRISGbits.TRISG13=1;

		} while(kbScanY != 0b00011111);

// risistemiamo la situaz. uscite
	TRISBbits.TRISB3=1;
	TRISBbits.TRISB2=1;
	TRISBbits.TRISB1=1;
	TRISBbits.TRISB0=1;
	TRISGbits.TRISG13=1;




// ora vado a controllare i tasti premuti, facendo anche attenzione ai tasti ripetuti:
//		non devono essere piu' di 2 nella colonna 7 (modifier)
//		 e in generale non devono esserci tasti fantasma (quelli "a quadrato")

/*SetColor(BLACK);
LCDCls();
LCDPutString(":");*/
	for(rowStatePtr=rowState; rowStatePtr < (rowState+5); rowStatePtr++) {	// puntatore alle letture fatte prima


/*{char mybuf[8];
sprintf(mybuf,"%02X ",*rowStatePtr);
LCDPutString(mybuf);
}*/
		for(kbtemp=1; kbtemp!=0b00100000; kbtemp <<= 1 /* entra 0 */) {					// analizzo le righe...
			ClrWdt();

			if(!(kbtemp & *rowStatePtr))
				checkKey(kbScanCode);

			kbScanCode++;
			} // 5 righe

		}			// for(rowStatePtr...


/*{char mybuf[8];
int i;
LCDPutString("; ");
for(i=0; i<MAX_TASTI_CONTEMPORANEI; i++) {
sprintf(mybuf,"%u ",kbKeys[i]);
LCDPutString(mybuf);
}
}
LCDPutString("\n");*/

	if(memcmp(kbKeys,oldkbKeys,MAX_TASTI_CONTEMPORANEI))
		FLAGS |= (1 << TRIGK);

	}

BYTE checkKey(BYTE kbScanCode) {
	BYTE i;

	for(i=0; i<MAX_TASTI_CONTEMPORANEI; i++) {
		ClrWdt();
		if(!kbKeys[i]) {				//	cerco un posto vuoto x metterci lo scan code
			kbKeys[i]=kbScanCode;
			return 1;
			}
		}

	return 0;
	}

void setKBDLed(void) {
	BYTE *p,n;
	BYTE i;

	ClrWdt();

  SPI2CON1 = 0x0000;              // power on state
  SPI2CON1 |= MASTER_ENABLE_ON | PRI_PRESCAL_1_1 | SEC_PRESCAL_1_1;          // select serial mode 
  SPI2CON1bits.CKP = 1;
  SPI2CON1bits.CKE = 0;
  SPI2CLOCK = 0;
  SPI2OUT = 0;                  // define SDO1 as output (master or slave)
  SPI2IN = 1;                  // define SDI1 as input (master or slave)
  SPI2ENABLE = 1;             // enable synchronous serial port



	SPI2_CS=0;
	WriteSPI2M(1);
	for(i=0; i<32; i++) {
		WriteSPI2M(0);
		}
	SPI2_CS=1;
	}

WORD getChksum16(BYTE *p,BYTE len) {		//CRC16
	WORD c;
	int i,j;

	c=0xffff;
	while(len--) {
		c ^= *p;
		for(i=0; i<8; i++) {
			j=c & 1;
			c >>= 1;
			if(j) {
				c ^= 0xa001;
				}
			}

		p++;
		}

	return c;
	}

WORD getChksumASCII(BYTE *p,int len) {		//LRC, non usiamo qua
	WORD c;

	c=0;
	while(len--) {
		c += *p;
		p++;
		}

	return c;
	}


static FSFILE *f;
WORD ExternalMemoryCallback(GFX_EXTDATA* memory, LONG offset, WORD nCount, void* buffer) {
	WORD n=0;

//	LED2_IO = 1;
	if(memory) {
		if(!f)
			f=FSfopen((char *)memory,FS_READ);			// provo a cache-are e lasciare aperto...
		if(f) {
//			if(f->seek  != offset)		// ottimizzo NO V. SOTTO
				FSfseek(f,offset,SEEK_SET);
			n=FSfread(buffer,1,nCount,f);
			}
		}
	else {
		FSfclose(f);
		f=NULL;
		}
//	LED2_IO = 0;
	return n;
	}

WORD ExternalMemoryCallbackCont(WORD nCount, void* buffer) {

//	LED2_IO ^= 1;
	if(f) {
		return FSfread(buffer,1,nCount,f);
		}
	return 0;
	}

/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event,
                void *data, DWORD size )

  Summary:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.

  Description:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.  If the application is able to handle the event, it
    returns TRUE.  Otherwise, it returns FALSE.

  Precondition:
    None

  Parameters:
    BYTE address    - Address of device where event occurred
    USB_EVENT event - Identifies the event that occured
    void *data      - Pointer to event-specific data
    DWORD size      - Size of the event-specific data

  Return Values:
    TRUE    - The event was handled
    FALSE   - The event was not handled

  Remarks:
    The application may also implement an event handling routine if it
    requires knowledge of events.  To do so, it must implement a routine that
    matches this function signature and define the USB_HOST_APP_EVENT_HANDLER
    macro as the name of that function.
  ***************************************************************************/
BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size ) {
  
	switch(event) {
        case EVENT_VBUS_REQUEST_POWER:
            // The data pointer points to a byte that represents the amount of power
            // requested in mA, divided by two.  If the device wants too much power,
            // we reject it.
            return TRUE;

        case EVENT_VBUS_RELEASE_POWER:
            // Turn off Vbus power.
            // The PIC24F with the Explorer 16 cannot turn off Vbus through software.

            //This means that the device was removed
            deviceAttached = FALSE;
            return TRUE;
            break;

        case EVENT_HUB_ATTACH:
            return TRUE;
            break;

        case EVENT_UNSUPPORTED_DEVICE:
            return TRUE;
            break;

        case EVENT_CANNOT_ENUMERATE:
            //UART2PrintString( "\r\n***** USB Error - cannot enumerate device *****\r\n" );
            return TRUE;
            break;

        case EVENT_CLIENT_INIT_ERROR:
            //UART2PrintString( "\r\n***** USB Error - client driver initialization error *****\r\n" );
            return TRUE;
            break;

        case EVENT_OUT_OF_MEMORY:
            //UART2PrintString( "\r\n***** USB Error - out of heap memory *****\r\n" );
            return TRUE;
            break;

        case EVENT_UNSPECIFIED_ERROR:   // This should never be generated.
            //UART2PrintString( "\r\n***** USB Error - unspecified *****\r\n" );
            return TRUE;
            break;

        default:
            break;
    }

  return FALSE;
	}

const struct LCD_ITEM lcdItems[ULTIMA_SCHERMATA_PROGRAMMAZIONE-PRIMA_SCHERMATA_PROGRAMMAZIONE] = 
{
//	BYTE x,y;	BYTE size;	WORD valMin,valMax;	WORD *var;	BYTE varSize;	char *name;
//varsize=0 e val min/max 0..1 indicano ON/OFF!
	0,0,11,0,50,(WORD *)&configParms.tVibro1[0],1,"Vibro1/pre:","d.s",		// v. sopra
	0,1,11,0,50,(WORD *)&configParms.tVibro1[1],1,"Vibro1/post:","d.s",
	0,2,11,0,50,(WORD *)&configParms.tVibro2[0],1,"Vibro2/pre:","d.s",
	0,3,11,0,50,(WORD *)&configParms.tVibro2[1],1,"Vibro2/post:","d.s",
	0,4,11,0,1,(WORD *)&configParms.mVibro1[0],0, "Vibro1\r\nristretto:",NULL,
	0,5,11,0,1,(WORD *)&configParms.mVibro1[1],0, "Vibro1\r\n1 tazza:",NULL,
	0,6,11,0,1,(WORD *)&configParms.mVibro1[2],0, "Vibro1\r\n2 tazze:",NULL,
	0,7,11,0,1,(WORD *)&configParms.mVibro1[3],0, "Vibro1\r\n1/4 sacco:",NULL,
	0,8,11,0,1,(WORD *)&configParms.mVibro1[4],0, "Vibro1\r\n1/2 sacco:",NULL,
	0,9,11,0,1,(WORD *)&configParms.mVibro1[5],0, "Vibro1\r\n1 sacco:",NULL,
	0,10,11,0,1,(WORD *)&configParms.mVibro2[0],0,"Vibro2\r\nristretto:",NULL,
	0,11,11,0,1,(WORD *)&configParms.mVibro2[1],0,"Vibro2\r\n1 tazza:",NULL,
	0,12,11,0,1,(WORD *)&configParms.mVibro2[2],0,"Vibro2\r\n2 tazze:",NULL,
	0,13,11,0,1,(WORD *)&configParms.mVibro2[3],0,"Vibro2\r\n1/4 sacco:",NULL,
	0,14,11,0,1,(WORD *)&configParms.mVibro2[4],0,"Vibro2\r\n1/2 sacco:",NULL,
	0,15,11,0,1,(WORD *)&configParms.mVibro2[5],0,"Vibro2\r\n1 sacco:",NULL,
//questi passano a centesimi/sec
	0,16,14,0,30000,(WORD *)&configParms.tMacinatura[0],2,"Tempo macinaristretto:","c.s",		// 12 char per riga
	0,17,14,0,30000,(WORD *)&configParms.tMacinatura[1],2,"Tempo macina1 tazza:","c.s",
	0,18,14,0,30000,(WORD *)&configParms.tMacinatura[2],2,"Tempo macina2 tazze:","c.s",
	0,19,14,0,30000,(WORD *)&configParms.tMacinatura[3],2,"Tempo macina1/4 sacco:","c.s",
	0,20,14,0,30000,(WORD *)&configParms.tMacinatura[4],2,"Tempo macina1/2 sacco:","c.s",
	0,21,14,0,30000,(WORD *)&configParms.tMacinatura[5],2,"Tempo macina1 sacco:","c.s",

	0,22,13,300,1500,(WORD *)&configParms.vMacinatura,2,"Vel.macina:","rpm",
	0,23,7,0,100,(WORD *)&configParms.vVibro[0],1,"Vel.vibro1:","%",
	0,24,7,0,100,(WORD *)&configParms.vVibro[1],1,"Vel.vibro2:","%",
	0,25,6,0,1,(WORD *)&configParms.needPortafiltro,0,"Filtro:",NULL,	
	0,26,8,0,1,(WORD *)&configParms.needSacchetto,0,"Sacchetto:",NULL,
// inserire ioni negativi
	0,27,10,0,1,(WORD *)&configParms.test,0,"SELF TEST:",NULL,
	0,28,10,0,2,(WORD *)&configParms.backLight,1,"Backlight:",NULL,
	0,29,10,0,2,(WORD *)&configParms.backLight,1,"Contrasto:",NULL,
	0,30,14,0,1,(WORD *)&configParms.setupEnabled,0,"SETUP ENABLED:",NULL,
	0,31,14,0,1,(WORD *)&configParms.splash,0,"Splash screen:",NULL
	};
  
void myInit_PWM(void)
{
    TRISDbits.TRISD3 = 0;               /* output   */
    RPOR1bits.RP67R = 0x10;                 /* OC1 010000 RPn tied to Output Compare 1 output */  

    OC1CON1 = 0;                        /* It is a good practice to clear off the control bits initially */
    OC1CON2 = 0;
    OC1CON1bits.OCTSEL = 0x07;          /* This selects the peripheral clock as the clock input to the OC module */
    OC1R = 00000;                        /* This is just a typical number, user must calculate based on the waveform requirements and the system clock */
    OC1RS = PWM_PERIOD;                       /* Determines the Period */
    
    OC1CON2bits.SYNCSEL = 0x1F;         /* This selects the synchronization source as itself */
    OC1CON1bits.OCM = 6;                /* This selects and starts the Edge Aligned PWM mode*/

}

void mySetPwm(unsigned char perc)
{
unsigned long duty;

    duty = ((PWM_PERIOD * perc) / 100);
    OC1R = duty;                        /* This is just a typical number, user must calculate based on the waveform requirements and the system clock */

}

void mySetPwm_Pier(unsigned char perc)
{

    TRISDbits.TRISD3 = 0;               /* output   */
    RPOR1bits.RP67R = 0x10;                 /* OC1 010000 RPn tied to Output Compare 1 output */  
        
    OC1CON1 = 0;                        /* It is a good practice to clear off the control bits initially */
    OC1CON2 = 0;
    OC1CON1bits.OCTSEL = 0x07;          /* This selects the peripheral clock as the clock input to the OC module */
    OC1R = 20000;                        /* This is just a typical number, user must calculate based on the waveform requirements and the system clock */
    OC1RS = 40000;                       /* Determines the Period */
    
    OC1CON2bits.SYNCSEL = 0x1F;         /* This selects the synchronization source as itself */
    OC1CON1bits.OCM = 6;                /* This selects and starts the Edge Aligned PWM mode*/
}

void testLCD_SD_CARD(int SDcardOK){
    char buffer[64];
    char ByteRec;
    int i;
    
     // test caratteri
    myFont=&font16;
    SetFont((void *)&font16);
    SetFontOrientation(0);		//horiz
    SetColor(GREEN);
    LED0_IO ^= 1;
    
	//	LCDPutStringCenter(5,"..SELF TEST..");   //matteo2019
	if(SDcardOK) {
		FSFILE *logFile=NULL;
		 // test caratteri
		myFont=&font16;
		SetFont((void *)&font16);
		SetFontOrientation(0);		//horiz
		SetColor(GREEN);
		LED0_IO ^= 1;
    
		LCDPutStringCenter(5,"..TEST.. ");
		LCDPutStringCenter(5,SDcardOK);
 
//		SetClockVars(((WORD)(currentDate.year))+2000, currentDate.mon, currentDate.mday, currentTime.hour, currentTime.min, currentTime.sec);
//		qua usiamo RTC
        logFile = FSfopen("bw.txt",FS_APPEND);
		if(logFile) {
			sprintf((char*)&buffer[0],"%u,%u\r\n",VERNUMH,VERNUML);
			FSfwrite((const void*)&buffer[0],1,strlen(buffer),logFile);
			FSfclose(logFile);
			logFile = NULL;
		}
	}

		
	char ch;
	LCDHome();
	for(ch=' '; ch<127; ch++)
		LCDPutChar(ch);
		

	sprintf(buffer,"VIEW ANGLE SETTING=%u",configParms.viewingAngle);
   	DelayMs(1*DEMODELAY);
	LED0_IO ^= 1;
	

	LCDCls();
	scanKBD(); 
	
	SetColor(BRIGHTGREEN);

	if(SDcardOK){ 
		LCDPutStringXY(0,6,"EXT.MEM.TEST..PASSED");     
        LCDCls();
    }
	else{
		LCDCls();
		LCDPutStringXY(0,6,"EXT.MEM.TEST..FAILED");      
    }

	// Initialize USB layers
	deviceAttached = FALSE;
	//Initialize the stack
	ByteRec=USBInitialize(0);
	LCDPutStringXY(0,7,ByteRec ? "USBHOST TEST..PASSED" : "USBHOST TEST..FAILED");     
	i=EEFSInit();
//	EEFSformat(1,0x4447,"bw");

//	LCDPutStringXY(0,9,"ID=");
	strncpy(buffer,CopyrString,28);
	buffer[28]=0;
//	LCDPutStringXY(4,9,buffer);
	strncpy(buffer,CopyrString+28,28);
	buffer[28]=0;
//	LCDPutStringXY(4,10,buffer);
	strncpy(buffer,CopyrString+56,28);
	buffer[28]=0;
//	LCDPutStringXY(4,11,buffer);
	
    PutBmp8BPPExtFast(GetRealOrgX(), GetRealOrgY(), (void *) "0.bmp" , IMAGE_NORMAL);
    
   	#ifdef USA_BUZZER_PWM 		
   		OC2CON1 |= 0x0006;   // on
        __delay_ms(100);
		ClrWdt();
		OC2CON1 &= ~0x0006;   // off
		__delay_ms(100);
		ClrWdt();
		OC2CON1 |= 0x0006;   // on
		__delay_ms(100);
		ClrWdt();
		OC2CON1 &= ~0x0006;   // off
		__delay_ms(100);
		ClrWdt();
		OC2CON1 |= 0x0006;   // on
		__delay_ms(100);
		ClrWdt();
		OC2CON1 &= ~0x0006;   // off
		//	__delay_ms(100);
		ClrWdt();
	#else
		LATGbits.LATG15=1;   // on
		__delay_ms(100);
        ClrWdt();
        LATGbits.LATG15=0;   // off
		__delay_ms(100);
		ClrWdt();
		LATGbits.LATG15=1;   // on
		__delay_ms(100);
		ClrWdt();
		LATGbits.LATG15=0;   // off
		__delay_ms(100);
		ClrWdt();
		LATGbits.LATG15=1;   // on
	    __delay_ms(100);
		ClrWdt();
		LATGbits.LATG15=0;   // off
	//	__delay_ms(100);
        ClrWdt();
	#endif
	DelayMs(1*DEMODELAY);

}

//void gestioneMenu(void){
//
//	char buffer[64];
//	int i;
//	long n2=0;
//	static signed char currMenu=0,currLang=0 /* default, italiano ;) */;
//    
//    switch(currMenu) {          
//        case 0:				// caso/i particolari
//
//
//        if (BUTTON[3] == '0'){
//        // MANUALE      
//            DelayAfter = 12;        
//            currMenu = 1;              
//            PutBmp8BPPExt(GetRealOrgX(), GetRealOrgY(), (void *) "work-m.bmp" , IMAGE_NORMAL);
//            mySetPwm(VIBRO_PWM);
//            LATBbits.LATB10 = 1;
//            LED_CMD[3] = '1';
//            LED_CMD[4] = '0';
//            LED_CMD[5] = '0';
//        }
//        else if (BUTTON[4] == '0'){
//            // DUE TAZZE
//            currMenu = 2;
//            DelayAfter = 12;
//            CycleTime = Cycle_2Tazza;
//            PutBmp8BPPExt(GetRealOrgX(), GetRealOrgY(), (void *) "work-a.bmp" , IMAGE_NORMAL);
//            mySetPwm(VIBRO_PWM);
//            LATBbits.LATB10 = 1;
//            LED_CMD[3] = '0';
//            LED_CMD[4] = '1';
//            LED_CMD[5] = '0';
//        }
//        else if (BUTTON[5] == '0'){
//            // UNA TAZZA
//            currMenu = 2;
//            DelayAfter = 12;
//            CycleTime = Cycle_1Tazza;
//            PutBmp8BPPExt(GetRealOrgX(), GetRealOrgY(), (void *) "work-a.bmp" , IMAGE_NORMAL);
//            mySetPwm(VIBRO_PWM);
//            LATBbits.LATB10 = 1;
//            LED_CMD[3] = '0';
//            LED_CMD[4] = '0';
//            LED_CMD[5] = '1';              
//        } 
//        else if ((BUTTON[1] == '0') && (BUTTON_OLD[1] == '1')){
//            // Inc 2 tazze
//            Cycle_2Tazza++;               
//        }
//        else if ((BUTTON[2] == '0') && (BUTTON_OLD[2] == '1')){
//      // dec 2 tazze
//            if (Cycle_2Tazza)
//              Cycle_2Tazza--;
//            }
//        else if ((BUTTON[6] == '0') && (BUTTON_OLD[6] == '1')){
//      // dec 1 tazze
//            if (Cycle_1Tazza)
//              Cycle_1Tazza--;
//        }                             
//        else if ((BUTTON[7] == '0') && (BUTTON_OLD[7] == '1')){
//      // inc 2 tazze
//            Cycle_1Tazza++;
//        }            
//
//        BUTTON_OLD[1] = BUTTON[1]; //INc 2 Tazze 
//        BUTTON_OLD[2] = BUTTON[2]; // dec 2 tazze
//        BUTTON_OLD[6] = BUTTON[6]; // dec 1 tazza
//        BUTTON_OLD[7] = BUTTON[7]; // inc 1 tazza
//
//        break;
//        case 1:
//                if (BUTTON[3] == '1'){
//                    currMenu = 4;
//                    LATBbits.LATB10 = 0;                          
//                }
//        break;            
//        case 2:
//                if (CycleTime)              
//                    CycleTime--;
//                else{
//                    currMenu = 4;
//                    LATBbits.LATB10 = 0;                            
//                }          
//        break;
//        case 3:
//         // TODO
//            break;
//        case 4:
//                if (DelayAfter)              
//                    DelayAfter--;
//                else{
//                    currMenu = 0;
//                    PutBmp8BPPExt(GetRealOrgX(), GetRealOrgY(), (void *) "home.bmp" , IMAGE_NORMAL);
//                    SendLedState("2222222222");
//                    mySetPwm(0);              
//                    LED_CMD[3] = '2';
//                    LED_CMD[4] = '2';
//                    LED_CMD[5] = '2';                              
//                }
//        break;
//        default:	// leggo da menu.ini che fare con questo tasto in questo menu
//                    {
//                    /* char buf2[32],buf3[32];
//                                    sprintf(buffer,"menu%u_%u",currMenu,currLang);
//                    // vedere quanto � lento farlo ad ogni giro!
//                            kLED1_IO = 1;
//                                    i=0;
//                                    do {
//                                        sprintf(buf2,"show%u",i);
//                                        *buf3=0;
//                                        GetProfileString(buffer,buf2,buf3);
//                                        if(!strnicmp(buf3,"orologio",8)) {
//                                            currMenu=atoi(buf3+4);
//                                            }	
//                                        else if(!strnicmp(buf3,"contasecondi",12)) {
//                                            i=atoi(buf3+1);
//                                            }	
//                                        else if(!strnicmp(buf3,"tmacina",7)) {
//                                            }	
//                                        else if(!strnicmp(buf3,"tvibro1",7)) {
//                                            }	
//                                        }	while(!*buf3 || i<10);
//                            kLED1_IO = 0;
//                    */
//                    }
//        break;
//
//        case 9:				// caso particolare macinatura
//            switch(State & 15) {
//                case STATE_IDLE:				// idle
//                    break;
//                case STATE_STARTING:			// start
//                    LCDCls();
//                    //if() 
//    //					SetPWM(0,configParms.vVibro[0]);
//    //					SetPWM(1,configParms.vVibro[1]);
//                    if(configParms.needPortafiltro) {
//                        if(m_MicroFiltro) {
//                            LCDPutStringCenter(2,"Mettere");
//                            LCDPutStringCenter(3,"filtro");
//                            while(m_MicroFiltro) {
//                                __delay_ms(100);
//                                scanKBD();
//                                if(FLAGS & (1 << TRIGK)){
//                                    BYTE ch;
//                                    ch=GetKBchar(kbKeys[0]);
//                                    if(ch==KB_OK)
//                                        break;
//                                    if(ch==KB_MENU_ESC) {
//                                        State= (State & STATE_OPERATION_SACCHETTO) | STATE_FINITO;
//                                        break;
//                                    }
//                                }
//                                ClrWdt();
//                            }
//                            LCDCls();
//                        }
//                    }
//                    if(State & STATE_OPERATION_SACCHETTO) {
//                        if(configParms.needSacchetto) {
//                            if(m_MicroSacchetto) {
//                                LCDPutStringCenter(2,"Mettere");
//                                LCDPutStringCenter(3,"sacchetto");
//                                while(m_MicroSacchetto) {
//                                    __delay_ms(100);
//                                    scanKBD();
//                                    if(FLAGS & (1 << TRIGK)) {
//                                        BYTE ch;
//                                        ch=GetKBchar(kbKeys[0]);
//                                        if(ch==KB_OK)
//                                            break;
//                                        if(ch==KB_MENU_ESC) {
//                                            State= STATE_OPERATION_SACCHETTO | STATE_FINITO;
//                                            break;
//                                        }
//                                    }
//                                    ClrWdt();
//                                }
//                                LCDCls();
//
//                            }
//                        }
//                    }
//                    LCDPutStringCenter(0,"Preparazione");
//                    if(State & STATE_OPERATION_SACCHETTO) {
//                        if(configParms.mBilancia) {
//                            LCDPutStringCenter(1,"taratura");
//                            BilanciaTara();
//                            __delay_ms(400);
//                            ClrWdt();
//                        }
//                        LCDPutStringCenter(1,tipoOperazione==0 ? "1/4 sacco" :
//                            (tipoOperazione==1 ? "1/2 sacco" : "1 sacco"));
//                        if(!(configParms.mVibro1[tipoOperazione+3]) && !(configParms.mVibro2[tipoOperazione+3])) {		// QUA ci sarebbe ancora il problema del tempo negativo
//                            ModBusSetMotore(1);
//                            State= STATE_OPERATION_SACCHETTO | STATE_MACINATURA;
//                            tempoOperazione=configParms.tMacinatura[tipoOperazione+3]/10;		// resto in decimi!
//                        }	
//                        else {
//                            State= STATE_OPERATION_SACCHETTO | STATE_VIBROPRE;
//                            if(configParms.mVibro1[tipoOperazione+3] && configParms.tVibro1[0]>0) {
//                                SetPWM(0,configParms.vVibro[0]);
//                            }
//                            if(configParms.mVibro2[tipoOperazione+3] && configParms.tVibro1[1]>0) {
//                                    SetPWM(1,configParms.vVibro[1]);
//                            }
//                            tempoOperazione=configParms.tVibro1[0];
//                        }	
//                    }else{
//                        LCDPutStringCenter(1,tipoOperazione==0 ? "1 dose" :
//                        (tipoOperazione==1 ? "2 dosi" : "3 dosi"));
//                        if(!(configParms.mVibro1[tipoOperazione]) && !(configParms.mVibro2[tipoOperazione])) {		// QUA ci sarebbe ancora il problema del tempo negativo
//                            ModBusSetMotore(1);
//                            State= STATE_MACINATURA;
//                            tempoOperazione=configParms.tMacinatura[tipoOperazione]/10;
//                        }	
//                        else {
//                                State= STATE_VIBROPRE;
//                                if(configParms.mVibro1[tipoOperazione] && configParms.tVibro1[0]>0) {
//                                    SetPWM(0,configParms.vVibro[0]);
//                                }
//                                if(configParms.mVibro2[tipoOperazione] && configParms.tVibro1[1]>0) {
//                                    SetPWM(1,configParms.vVibro[1]);
//                                }
//                                tempoOperazione=configParms.tVibro1[0];
//                        }	
//                        }	
//                break;
//                case STATE_VIBROPRE:				// 
//                    if(State & STATE_OPERATION_SACCHETTO) {
//                        if(tempoOperazione)
//                            tempoOperazione--;
//                        else{
//                                ModBusSetMotore(1);
//                                if(configParms.mBilancia)
//                                    BilanciaLeggiPeso();			// lettura dummy per ciucca
//                                    State= STATE_OPERATION_SACCHETTO | STATE_MACINATURA;
//                                    tempoOperazione=configParms.tMacinatura[tipoOperazione+3]/10;
//                                    if(configParms.mVibro1[tipoOperazione+3]) {
//                                        SetPWM(0,configParms.vVibro[0]);
//                                    }
//                                if(configParms.mVibro2[tipoOperazione+3]) {
//                                    SetPWM(1,configParms.vVibro[1]);
//                                }
//                            }	
//                        }else {
//                            if(tempoOperazione)
//                                tempoOperazione--;
//                            else{
//                                    ModBusSetMotore(1);
//                                    State= STATE_MACINATURA;
//                                    tempoOperazione=configParms.tMacinatura[tipoOperazione]/10;
//                                    if(configParms.mVibro1[tipoOperazione]) {
//                                        SetPWM(0,configParms.vVibro[0]);
//                                    }
//                                    if(configParms.mVibro2[tipoOperazione]) {
//                                       SetPWM(1,configParms.vVibro[1]);
//                                    }
//                            }	
//                        }			
//                break;
//                case STATE_MACINATURA:				// macinatura
//                        if(State & STATE_OPERATION_SACCHETTO) {
//                            char buf[32];
//                            BYTE t1,t2,t3;
//
//                            // INCRICCAva MODBUS... 20.12.17						
//                            if(configParms.mBilancia) {
//                                static BYTE divider=0;
//                                divider++;
//                                if(!(divider & 2))
//                                    n2=BilanciaLeggiPeso();
//                                }
//
//                            t1=tempoOperazione/600;
//                            t2=(tempoOperazione/10) % 60;
//                            t3=tempoOperazione % 10;
//                            sprintf(buf,"t: %u:%02u.%u0  ",t1,t2,t3);
//                            LCDPutStringXY(1,3,buf);
//                            if(configParms.mBilancia) {
//                                sprintf(buf,"%4ld.%02u g",n2/100,abs(n2) % 100);
//                                LCDPutStringXY(0,2,"            ");
//                                LCDPutStringXY(2,2,buf);
//                            }
//                        else {
//                                t1=configParms.tMacinatura[tipoOperazione+3]/6000;
//                                t2=(configParms.tMacinatura[tipoOperazione+3]/100) % 60;
//                                t3=configParms.tMacinatura[tipoOperazione+3] % 100;
//                                sprintf(buf,"-> %u:%02u.%02u  ",t1,t2,t3);
//                                LCDPutStringXY(1,2,buf);
//                        }
//                        if(tempoOperazione) {
//                            tempoOperazione--;
//                        }
//                        else {
//                                ModBusSetMotore(0);
//                                if(!configParms.mVibro1[tipoOperazione+3] && !configParms.mVibro2[tipoOperazione+3]) {
//                                    State= STATE_OPERATION_SACCHETTO | STATE_FINITO;
//                                }
//                                else {
//                                        tempoOperazione=max(configParms.tVibro1[1],configParms.tVibro2[1]);
//                                        if(configParms.mVibro1[tipoOperazione+3])
//                                            SetPWM(0,configParms.vVibro[0]);
//                                        if(configParms.mVibro2[tipoOperazione+3])
//                                            SetPWM(1,configParms.vVibro[1]);
//                                        State= STATE_OPERATION_SACCHETTO | STATE_VIBROPOST;
//                                }	
//                        }	
//                    }
//                    else {
//                            char buf[16];
//                            BYTE t1,t2,t3;
//
//                            t1=tempoOperazione/600;
//                            t2=(tempoOperazione/10) % 60;
//                            t3=tempoOperazione % 10;
//                            sprintf(buf,"t: %u:%02u.%u0  ",t1,t2,t3);
//                            LCDPutStringXY(1,3,buf);
//                            t1=configParms.tMacinatura[tipoOperazione]/6000;
//                            t2=(configParms.tMacinatura[tipoOperazione]/100) % 60;
//                            t3=configParms.tMacinatura[tipoOperazione] % 100;
//                            sprintf(buf,"-> %u:%02u.%02u  ",t1,t2,t3);
//                            LCDPutStringXY(1,2,buf);
//                            if(tempoOperazione) {
//                                tempoOperazione--;
//                            }
//                            else{
//                                    ModBusSetMotore(0);	
//                                    if(!configParms.mVibro1[tipoOperazione] && !configParms.mVibro2[tipoOperazione]) {
//                                        State= STATE_FINITO;
//                                    }
//                                    else {
//                                            tempoOperazione=max(configParms.tVibro1[1],configParms.tVibro2[1]);
//                                            if(configParms.mVibro1[tipoOperazione])
//                                                SetPWM(0,configParms.vVibro[0]);
//                                            if(configParms.mVibro2[tipoOperazione])
//                                                SetPWM(1,configParms.vVibro[1]);
//                                            State= STATE_VIBROPOST;
//                                    }	
//                                }	
//                            }			
//                break;
//                case STATE_VIBROPOST:				// 
//                                if(State & STATE_OPERATION_SACCHETTO) {
//                                //GESTIRE 2 TEMPI!
//                                if(tempoOperazione)
//                                    tempoOperazione--;
//                                else {
//                                        ModBusSetMotore(0);		// non servirebbe...
//                                        SetPWM(0,0);
//                                        SetPWM(1,0);
//                                        State= STATE_OPERATION_SACCHETTO | STATE_FINITO;
//                                }	
//                            }else{
//                                    if(tempoOperazione)
//                                        tempoOperazione--;
//                                    else {
//                                        ModBusSetMotore(0);		// non servirebbe...
//                                        SetPWM(0,0);
//                                        SetPWM(1,0);
//                                        State= STATE_FINITO;
//                                    }	
//                                }			
//                            break;
//                case STATE_FINITO:				// finito
//                            if(State & STATE_OPERATION_SACCHETTO) {
//                                if(configParms.mBilancia) {
//                                    char buf[32];
//                                    BYTE i;
//
//                                    for(i=0; i<20; i++) {			//5sec
//                                        n2=BilanciaLeggiPeso();
//                                        sprintf(buf,"%4ld.%02u g",n2/100,abs(n2) % 100);
//                                        LCDPutStringXY(0,2,"            ");
//                                        LCDPutStringXY(2,2,buf);
//                                        __delay_ms(250);
//                                        ClrWdt();
//                                    }
//                                }
//                            }
//
//                            LCDPutStringCenter(3,"   Fatto.   ");
//                            SetPWM(0,0);
//                            SetPWM(1,0);
//                            State= STATE_IDLE;
//                            goto print_logo;
//                            break;
//                        }
//        break;
//    }
//    
//    if(FLAGS & (1 << TRIGK) /*kbKeys[0]*/) {
//        BYTE ch;
//
//    //	ModBusSetRele(1);		// debug*****
//        ch=GetKBchar(kbKeys[0]);
//        ch=0;
//
//        switch(currMenu) {
//            //	case 0:				// caso/i particolari
//            //	break;
//            default:			// leggo da menu.ini che fare con questo tasto in questo menu
//            {
//                char buf2[32],buf3[32];
//                sprintf(buffer,"menu%u_%u",currMenu,currLang);
//                GetProfileString(buffer,"backimg",buf2);
//
//                sprintf(buf2,"a_s%u",ch);
//                *buf3=0;
//                GetProfileString(buffer,buf2,buf3);
//                if(!strnicmp(buf3,"menu",4)) {
//                    currMenu=atoi(buf3+4);
//                }	
//                else if(!strnicmp(buf3,"o",1)) {
//                    i=atoi(buf3+1);
//                }	
//                else if(!strnicmp(buf3,"stop",4)) {
//                }	
//
//            }
//
//            switch(State & 15) {
//                case STATE_IDLE:				// idle
//                switch(ch) {
//                    case KB_MENU_ESC:
//                        State=STATE_SETUP;
//                        handleBIOS(255);		// ...
//                        goto print_logo;
//                    break;
//                    case KB_TAZZAPICCOLA:
//                        tipoOperazione=0;
//                        State=STATE_STARTING;
//                    break;
//                    case KB_TAZZAMEDIA:
//                        tipoOperazione=1;
//                        State=STATE_STARTING;
//                    break;
//                    case KB_TAZZAGRANDE:
//                        tipoOperazione=2;
//                        State=STATE_STARTING;
//                    break;
//                    case KB_SACCHETTOPICCOLO:
//                        tipoOperazione=0;
//                        State=STATE_STARTING | STATE_OPERATION_SACCHETTO;
//                    break;
//                    case KB_SACCHETTOMEDIO:
//                        tipoOperazione=1;
//                        State=STATE_STARTING | STATE_OPERATION_SACCHETTO;
//                    break;
//                    case KB_SACCHETTOGRANDE:
//                        tipoOperazione=2;
//                        State=STATE_STARTING | STATE_OPERATION_SACCHETTO;
//                    break;
//                    case KB_RPM:
//                        State=STATE_SETUP;
//                        handleBIOS(22);		// ... occhio VMACINATURA
//                        goto print_logo;
//                    break;
//                    case KB_MANUALE:
//                        LCDCls();
//                        tick10=0;
//                        LCDPutStringCenter(0,"Macinatura");
//                        ModBusSetMotore(1);
//                        SetPWM(0,configParms.vVibro[0]);
//                        SetPWM(1,configParms.vVibro[1]);
//                        do {
//                            char buf[8];
//                            ClrWdt();
//                            __delay_ms(100);
//                            sprintf(buf,"t: %u.%u  ",tick10/10,tick10%10);
//                            LCDPutStringXY(2,1,buf);
//                            scanKBD();
//                            ch=GetKBchar(kbKeys[0]);
//                        } while(ch==KB_MANUALE);
//                        ModBusSetMotore(0);
//                        tempoOperazione=configParms.tVibro1[1];
//                        while(tempoOperazione--) {
//                            __delay_ms(100);
//                            ClrWdt();
//                        }	
//                        SetPWM(0,0);
//                        SetPWM(1,0);
//                        LCDPutStringCenter(3,"Terminato.");
//                        goto print_logo;
//                    break;
//                    case KB_BILANCIA:
//                        if(configParms.mBilancia) {
//                            LCDCls();
//                            BilanciaLeggiPeso();			// lettura dummy per ciucca
//                            LCDPutStringCenter(1,"Peso:");
//                            do {
//                                    char buf[32];
//                                    kLED0_IO ^= 1;
//                                    n2=BilanciaLeggiPeso();
//                                    ClrWdt();
//                                    __delay_ms(300);
//                                    sprintf(buf,"%4ld.%02u g",n2/100,abs(n2) % 100);
//        //							sprintf(buf,"%8ld g",n2);
//                                    LCDPutStringXY(0,2,"            ");
//                                    LCDPutStringXY(2,2,buf);
//                                    scanKBD();
//                                    ch=GetKBchar(kbKeys[0]);
//                            } while(ch != KB_BILANCIA);		// un click entra, uno esce
//                            __delay_ms(500);			// debounce finale
//                            goto print_logo;
//                        }	
//                        else {
//                                goto non_installato;
//                        }	
//                    break;
//                    case KB_TARA:
//                        if(configParms.mBilancia) {
//                            LCDCls();
//                            LCDPutStringCenter(1,"Attendere..");
//                            kLED0_IO ^= 1;
//                            BilanciaTara();
//                            __delay_ms(1000);
//                            LCDPutStringCenter(2,".");
//                            kLED0_IO ^= 1;
//                            ClrWdt();
//                            __delay_ms(1000);
//                            LCDPutStringCenter(2,"..");
//                            kLED0_IO ^= 1;
//                            ClrWdt();
//                            __delay_ms(1000);
//                            LCDPutStringCenter(2,"...");
//                            kLED0_IO ^= 1;
//                            ClrWdt();
//                            __delay_ms(1000);
//                            LCDPutStringCenter(2,"....");
//                            kLED0_IO ^= 1;
//                            ClrWdt();
//                            n2=BilanciaLeggiPeso();
//                            goto print_logo;
//                        }
//                        else {
//                                goto non_installato;
//                        }	
//                    break;
//                    case KB_TEMPERATURA:
//                        non_installato:
//                        LCDCls();
//                        LCDPutStringCenter(1,"Non");
//                        LCDPutStringCenter(2,"installato");
//                        print_logo:
//                                __delay_ms(2000);
//                                LCDCls();
//                //				OLED_draw_bitmap(((GetMaxX()-GetRealOrgX())-112)/2, 0, 112,48,(void *)&logoKiki);		// in alto
//                                break;
//                    }
//                    break;
//
//                    case STATE_VIBROPRE:				// vibro1
//                    case STATE_MACINATURA:				// macinatura
//                    case STATE_VIBROPOST:				// vibro2
//                        switch(ch) {
//                            case KB_MENU_ESC:			// ESC interrompe operazione in corso!
//                                LCDCls();
//                                LCDPutStringCenter(2,"Interrotto!");
//                                __delay_ms(1000);
//        //								LCDCls();
//        //								OLED_draw_bitmap(((GetMaxX()-GetRealOrgX())-112)/2, 0, 112,48,(void *)&logoKiki);		// in alto
//                                ModBusSetMotore(0);
//                                State=STATE_FINITO;
//                            break;
//                        }
//                        break;
//
//                    case STATE_FINITO:				// 
//                        break;
//            }
//            break;
//        }	
//	}
//}



int main(void){
    BYTE blinkCnt=0;
	int i;
  	static signed char oldMenu=-1,currMenu=0 /* default, italiano ;) */;
      
    
    //init(); //substitute CLOCK Initialize  
    CLOCK_Initialize();
    PIN_MANAGER_Initialize();
    INTERRUPT_Initialize();
    TMR2_Initialize();
    TMR1_Initialize();

    UART1_Initialize();
    
    OUT_SNS_Pres_SetDigitalOutput();
    IN_SNS_Pres_SetDigitalInput();
    
    OUT_SNS_Pres_SetHigh();
       
	loadSettings();

	curPosX=GetRealOrgX(); curPosY=GetRealOrgY();

    InitGraph();     // Graphics
	ClrWdt();

    if(configParms.backLight) {
		DisplayBacklightOn();		//serve per attivare display!
		backLight=1;
    }

	if(MDD_MediaDetect()){//Ricerca Sd Card    
        SDcardOK=FSInit();  // File system 
        flag_nok_sd = 0;
    } else {
        SetColor(WHITE);
        LCDPutStringXY(5,5, "INSERIRE mSD-CARD");
        //while(!MDD_MediaDetect());
        LCDCls();
        flag_nok_sd = 1;
    }

    
//  Initialize the RTCC
//  Turn on the secondary oscillator
	__asm__ ("MOV #OSCCON,w1");
	__asm__ ("MOV.b #0x02, w0");
	__asm__ ("MOV #0x46, w2");
	__asm__ ("MOV #0x57, w3");
	__asm__ ("MOV.b w2, [w1]");
	__asm__ ("MOV.b w3, [w1]");
	__asm__ ("MOV.b w0, [w1]");
	
	PIC24RTCCSetDate( DEFAULT_YEARS, DEFAULT_MONTH_DAY );
	PIC24RTCCSetTime( DEFAULT_WEEKDAY_HOURS, DEFAULT_MINUTES_SECONDS );
    //	__builtin_write_OSCCONL(OSCCON | 0x0002); 		//DAL FORUM... MA CREDO CI SIA GIA'...
	RCFGCAL = 0x8000;

	//scanKBD(); 

 	SetColor(BLUE);
	ClearDevice();
    
     
//    configParms.test=1;
    configParms.splash=1;
	if(configParms.splash) {		// nostra estensione
		ClearDevice();
    	// draw border lines to show the limits of the 
        // left, right, top and bottom pixels of the screen
        // draw the topmost horizontal line
          SetColor(BRIGHTRED);
        //  SetColor(BRIGHTYELLOW); hmmm , manca il rosso...
          WAIT_UNTIL_FINISH(Line(GetRealOrgX(),GetRealOrgY(),GetRealMaxX()+GetRealOrgX(),GetRealOrgY()));
          // draw the rightmost vertical line
          SetColor(BRIGHTYELLOW);
          WAIT_UNTIL_FINISH(Line(GetRealMaxX()+GetRealOrgX()-1,GetRealOrgY(),GetRealMaxX()+GetRealOrgX()-1,GetRealMaxY()+GetRealOrgY()));
        // draw the bottom-most horizontal line
          SetColor(BRIGHTGREEN);
          WAIT_UNTIL_FINISH(Line(GetRealOrgX(),GetRealMaxY()+GetRealOrgY()-1,GetRealMaxX()+GetRealOrgX(),GetRealMaxY()+GetRealOrgY()-1));
          // draw the leftmost vertical line
          SetColor(BRIGHTBLUE);
          WAIT_UNTIL_FINISH(Line(GetRealOrgX(),GetRealOrgY(),GetRealOrgX(),GetRealMaxY()+GetRealOrgY()));

          DelayMs(DEMODELAY);
	}

    LCDCls();
    //scanKBD(); 
//
//	if(configParms.test) {	 
//        testLCD_SD_CARD(SDcardOK);
//	}

	LED0_IO ^= 1;
	LCDCls();
    SetColor(WHITE);
	LCDHome();
    LCDCls();
    
	//SearchRec srec;
    
    /*
    //	LCDPutStringXY(0,0,"DIR>");
    //	FindFirst("*.bmp",ATTR_MASK,&srec);

    //    if(configParms.test){ 
    //        // stampa a video elenco file in sd card
    //        for(i=0; i<16; i++) {
    //            LCDPutStringXY(1,i+1,srec.filename);
    //            if(FindNext(&srec))
    //                break;
    //            }
    //            DelayMs(2*DEMODELAY);
    //            LCDCls();
    //    }
    //
    //	if(!TASTO4) {// v. sopra
    ////		LCDPutStringXY(0,0,"Ripristino default!");
    //        LCDCls();
    //		for(i=0; i<8; i++) {
    //			kLED0_IO ^= 1;
    //			kLED1_IO ^= 1;
    //			__delay_ms(100);
    //			ClrWdt();
    //		}
    //		resetSettings();
    //		saveSettings();
    //	}

    */    
    //    {
    //        FSFILE *f;
    //        f=EEFSfopen(SETTINGS_INI,FS_READ);
    //        if(f) {
    //            kLED7_IO ^= 1;
    //            EEFSfclose(f);
    //        }
    //
    //        SearchRec srec;
    //        LCDPutChar('\r');
    //        LCDPutChar('\n');
    //        if(EEFindFirst("*.*",/*ATTR_NORMAL*/ ATTR_MASK,&srec)>=0)
    //            do {
    //                    ClrWdt();
    //    //				LCDPutString(srec.filename);
    //                    LCDPutChar('\r');
    //                    LCDPutChar('\n');
    //                    LCDCls();
    //                } while(!EEFindNext(&srec));
    //
    //
    //        USBTasks();
    //        if(USBHostMSDSCSIMediaDetect()) {
    //            kLED5_IO ^= 1;
    //            if(USBFSInit()) {
    //                f=USBFSfopen(SETTINGS_INI,FS_WRITE);
    //                if(f) {
    //                    kLED6_IO ^= 1;
    //                    USBFSfclose(f);
    //                }
    ////        		if(USBFindFirst("*.*",ATTR_MASK,&srec)>=0) 
    ////                    do {
    ////                        ClrWdt();
    ////                        LCDPutString(srec.filename);
    ////                        LCDPutChar('\r');
    ////                        LCDPutChar('\n');
    ////                      } while(!USBFindNext(&srec));
    //            }
    //        }   
    //        exportSettingsSD();
    //    }
    //		
    //	State=STATE_IDLE;
    //    DelayMs(2*DEMODELAY);
    //    Config_LED();
    //    Enable_ALL_Led_White();
        //	PutBmp8BPPExt(0, 0, (void *) "fondo001.bmp", IMAGE_NORMAL);

    
    if (!flag_nok_sd)PutBmp8BPPExt(GetRealOrgX(), GetRealOrgY(), (void *) "home.bmp" , IMAGE_NORMAL);
    else{LCDPutStringXY(5,5, "HOME SCREEN");}

    while(1) {    //mainloop
       
//      if (IN_SNS_Pres_GetValue()){
//          flag_ok_press = 1;
//      }else{
//          flag_ok_press = 0;
//      }  
      CommStart();
       
        //__delay_ms(100);
//      CountdownLCD(count, 30, 20);
//      //Timer1_Counter(55000);
//      //if (flag_30sec)LED5_SetHigh();
//
//      __delay_ms(20);
//        count++;
        
        //ClrWdt();
//    
//        if (FirstPacket == 1){
//            SendLedState("2222222222");     
//            FirstPacket = 2; 
//        }
//
//		if(second_30) {
//			scanKBD(); 
//		}
//		if(second_10) {
//			second_10=0;
//			blinkCnt++;
//
//			if(configParms.backLight==2) {		// gestire timed ? secondi
//				if(backLight) {
//					DisplayBacklightOff();		
//					backLight=0;
//				}
//			}
//
//			if((tick10 & 1) == 1) {			// 0.2 sec... 
//				if(oldMenu != currMenu) {
////					sprintf(buffer,"image%u.bmp",currMenu);
////					PutBmp8BPPExt(40, 0, (void *) buffer, IMAGE_NORMAL);
//					oldMenu = currMenu;
//				}
//
//                if ((blinkCnt % 2) == 0){
//                    SendLedState(LED_CMD);                       
//                }
//    
//                StateRx = 0;
//
//                gestioneMenu();
//
//			}				// 0.2 sec
//
//    	}			// second_10
//
//		if(second_30) {
//                handleTastiera();		// messo dopo per non fare uscire char se entro in bios ecc!
//                second_30=0;
//		}
	}	//main loop
    return (-1);
}

