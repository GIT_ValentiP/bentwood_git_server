/**
  PIN MANAGER Generated Driver File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the generated manager file for the PIC24 / dsPIC33 / PIC32MM MCUs device.  This manager
    configures the pins direction, initial state, analog setting.
    The peripheral pin select, PPS, configuration is also handled by this manager.

  @Description:
    This source file provides implementations for PIN MANAGER.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.95-b-SNAPSHOT
        Device            :  PIC24EP128GP206
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.36
        MPLAB 	          :  MPLAB X v5.10
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#ifndef _PIN_MANAGER_H
#define _PIN_MANAGER_H
/**
    Section: Includes
*/
//#include <xc.h>

#define PPSIn(fn,pin)    iPPSInput(IN_FN_PPS##fn,IN_PIN_PPS##pin)
#define PPSOut(fn,pin)    iPPSOutput(OUT_PIN_PPS##pin,OUT_FN_PPS##fn)

/**
    Section: Device Pin Macros
*/
/**
  @Summary
    Sets the GPIO pin, RA10, high using LATA10.

  @Description
    Sets the GPIO pin, RA10, high using LATA10.

  @Preconditions
    The RA10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA10 high (1)
    LED1_SetHigh();
    </code>

*/
#define LED1_SetHigh()          _LATA10 = 1
/**
  @Summary
    Sets the GPIO pin, RA10, low using LATA10.

  @Description
    Sets the GPIO pin, RA10, low using LATA10.

  @Preconditions
    The RA10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA10 low (0)
    LED1_SetLow();
    </code>

*/
#define LED1_SetLow()           _LATA10 = 0
/**
  @Summary
    Toggles the GPIO pin, RA10, using LATA10.

  @Description
    Toggles the GPIO pin, RA10, using LATA10.

  @Preconditions
    The RA10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA10
    LED1_Toggle();
    </code>

*/
#define LED1_Toggle()           _LATA10 ^= 1
/**
  @Summary
    Reads the value of the GPIO pin, RA10.

  @Description
    Reads the value of the GPIO pin, RA10.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA10
    postValue = LED1_GetValue();
    </code>

*/
#define LED1_GetValue()         _RA10
/**
  @Summary
    Configures the GPIO pin, RA10, as an input.

  @Description
    Configures the GPIO pin, RA10, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA10 as an input
    LED1_SetDigitalInput();
    </code>

*/
#define LED1_SetDigitalInput()  _TRISA10 = 1
/**
  @Summary
    Configures the GPIO pin, RA10, as an output.

  @Description
    Configures the GPIO pin, RA10, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA10 as an output
    LED1_SetDigitalOutput();
    </code>

*/
#define LED1_SetDigitalOutput() _TRISA10 = 0


#define LED2_SetHigh()          _LATA2 = 1
#define LED2_SetLow()           _LATA2 = 0
#define LED2_Toggle()           _LATA2 ^= 1
#define LED2_GetValue()         _RA2
#define LED2_SetDigitalInput()  _TRISA2 = 1
#define LED2_SetDigitalOutput() _TRISA2 = 0

#define LED3_SetHigh()          _LATA3 = 1
#define LED3_SetLow()           _LATA3 = 0
#define LED3_Toggle()           _LATA3 ^= 1
#define LED3_GetValue()         _RA3
#define LED3_SetDigitalInput()  _TRISA3 = 1
#define LED3_SetDigitalOutput() _TRISA3 = 0

#define LED5_SetHigh()          _LATA5 = 1
#define LED5_SetLow()           _LATA5 = 0
#define LED5_Toggle()           _LATA3 ^= 1
#define LED5_GetValue()         _RA5
#define LED5_SetDigitalInput()  _TRISA5 = 1
#define LED5_SetDigitalOutput() _TRISA5 = 0


/**
  @Summary
    Sets the GPIO pin, RA4, high using LATA4.

  @Description
    Sets the GPIO pin, RA4, high using LATA4.

  @Preconditions
    The RA4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA4 high (1)
    LED4_SetHigh();
    </code>

*/
#define LED4_SetHigh()          _LATA4 = 1
/**
  @Summary
    Sets the GPIO pin, RA4, low using LATA4.

  @Description
    Sets the GPIO pin, RA4, low using LATA4.

  @Preconditions
    The RA4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA4 low (0)
    LED4_SetLow();
    </code>

*/
#define LED4_SetLow()           _LATA4 = 0
/**
  @Summary
    Toggles the GPIO pin, RA4, using LATA4.

  @Description
    Toggles the GPIO pin, RA4, using LATA4.

  @Preconditions
    The RA4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA4
    LED4_Toggle();
    </code>

*/
#define LED4_Toggle()           _LATA4 ^= 1
/**
  @Summary
    Reads the value of the GPIO pin, RA4.

  @Description
    Reads the value of the GPIO pin, RA4.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA4
    postValue = LED4_GetValue();
    </code>

*/
#define LED4_GetValue()         _RA4
/**
  @Summary
    Configures the GPIO pin, RA4, as an input.

  @Description
    Configures the GPIO pin, RA4, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA4 as an input
    LED4_SetDigitalInput();
    </code>

*/
#define LED4_SetDigitalInput()  _TRISA4 = 1
/**
  @Summary
    Configures the GPIO pin, RA4, as an output.

  @Description
    Configures the GPIO pin, RA4, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA4 as an output
    LED4_SetDigitalOutput();
    </code>

*/
#define LED4_SetDigitalOutput() _TRISA4 = 0
/**
  @Summary
    Sets the GPIO pin, RB10, high using LATB10.

  @Description
    Sets the GPIO pin, RB10, high using LATB10.

  @Preconditions
    The RB10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB10 high (1)
    Relay_Macina_SetHigh();
    </code>

*/
#define Relay_Macina_SetHigh()          _LATB10 = 1
/**
  @Summary
    Sets the GPIO pin, RB10, low using LATB10.

  @Description
    Sets the GPIO pin, RB10, low using LATB10.

  @Preconditions
    The RB10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB10 low (0)
    Relay_Macina_SetLow();
    </code>

*/
#define Relay_Macina_SetLow()           _LATB10 = 0
/**
  @Summary
    Toggles the GPIO pin, RB10, using LATB10.

  @Description
    Toggles the GPIO pin, RB10, using LATB10.

  @Preconditions
    The RB10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB10
    Relay_Macina_Toggle();
    </code>

*/
#define Relay_Macina_Toggle()           _LATB10 ^= 1
/**
  @Summary
    Reads the value of the GPIO pin, RB10.

  @Description
    Reads the value of the GPIO pin, RB10.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB10
    postValue = Relay_Macina_GetValue();
    </code>

*/
#define Relay_Macina_GetValue()         _RB10
/**
  @Summary
    Configures the GPIO pin, RB10, as an input.

  @Description
    Configures the GPIO pin, RB10, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB10 as an input
    Relay_Macina_SetDigitalInput();
    </code>

*/
#define Relay_Macina_SetDigitalInput()  _TRISB10 = 1
/**
  @Summary
    Configures the GPIO pin, RB10, as an output.

  @Description
    Configures the GPIO pin, RB10, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB10 as an output
    Relay_Macina_SetDigitalOutput();
    </code>

*/
#define Relay_Macina_SetDigitalOutput() _TRISB10 = 0
/**
  @Summary
    Sets the GPIO pin, RB11, high using LATB11.

  @Description
    Sets the GPIO pin, RB11, high using LATB11.

  @Preconditions
    The RB11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB11 high (1)
    IN_SNS_Pres_SetHigh();
    </code>

*/
#define IN_SNS_Pres_SetHigh()          _LATB11 = 1
/**
  @Summary
    Sets the GPIO pin, RB11, low using LATB11.

  @Description
    Sets the GPIO pin, RB11, low using LATB11.

  @Preconditions
    The RB11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB11 low (0)
    IN_SNS_Pres_SetLow();
    </code>

*/
#define IN_SNS_Pres_SetLow()           _LATB11 = 0
/**
  @Summary
    Toggles the GPIO pin, RB11, using LATB11.

  @Description
    Toggles the GPIO pin, RB11, using LATB11.

  @Preconditions
    The RB11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB11
    IN_SNS_Pres_Toggle();
    </code>

*/
#define IN_SNS_Pres_Toggle()           _LATB11 ^= 1
/**
  @Summary
    Reads the value of the GPIO pin, RB11.

  @Description
    Reads the value of the GPIO pin, RB11.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB11
    postValue = IN_SNS_Pres_GetValue();
    </code>

*/
#define IN_SNS_Pres_GetValue()         _RB11
/**
  @Summary
    Configures the GPIO pin, RB11, as an input.

  @Description
    Configures the GPIO pin, RB11, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB11 as an input
    IN_SNS_Pres_SetDigitalInput();
    </code>

*/
#define IN_SNS_Pres_SetDigitalInput()  _TRISB11 = 1
/**
  @Summary
    Configures the GPIO pin, RB11, as an output.

  @Description
    Configures the GPIO pin, RB11, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB11 as an output
    IN_SNS_Pres_SetDigitalOutput();
    </code>

*/
#define IN_SNS_Pres_SetDigitalOutput() _TRISB11 = 0
/**
  @Summary
    Sets the GPIO pin, RB9, high using LATB9.

  @Description
    Sets the GPIO pin, RB9, high using LATB9.

  @Preconditions
    The RB9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB9 high (1)
    OUT_SNS_Pres_SetHigh();
    </code>

*/
#define OUT_SNS_Pres_SetHigh()          _LATB9 = 1
/**
  @Summary
    Sets the GPIO pin, RB9, low using LATB9.

  @Description
    Sets the GPIO pin, RB9, low using LATB9.

  @Preconditions
    The RB9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB9 low (0)
    OUT_SNS_Pres_SetLow();
    </code>

*/
#define OUT_SNS_Pres_SetLow()           _LATB9 = 0
/**
  @Summary
    Toggles the GPIO pin, RB9, using LATB9.

  @Description
    Toggles the GPIO pin, RB9, using LATB9.

  @Preconditions
    The RB9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB9
    OUT_SNS_Pres_Toggle();
    </code>

*/
#define OUT_SNS_Pres_Toggle()           _LATB9 ^= 1
/**
  @Summary
    Reads the value of the GPIO pin, RB9.

  @Description
    Reads the value of the GPIO pin, RB9.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB9
    postValue = OUT_SNS_Pres_GetValue();
    </code>

*/
#define OUT_SNS_Pres_GetValue()         _RB9
/**
  @Summary
    Configures the GPIO pin, RB9, as an input.

  @Description
    Configures the GPIO pin, RB9, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB9 as an input
    OUT_SNS_Pres_SetDigitalInput();
    </code>

*/
#define OUT_SNS_Pres_SetDigitalInput()  _TRISB9 = 1
/**
  @Summary
    Configures the GPIO pin, RB9, as an output.

  @Description
    Configures the GPIO pin, RB9, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB9 as an output
    OUT_SNS_Pres_SetDigitalOutput();
    </code>

*/
#define OUT_SNS_Pres_SetDigitalOutput() _TRISB9 = 0
/**
  @Summary
    Sets the GPIO pin, RC13, high using LATC13.

  @Description
    Sets the GPIO pin, RC13, high using LATC13.

  @Preconditions
    The RC13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC13 high (1)
    SW3_SetHigh();
    </code>

*/

#define SW1_SetHigh()          _LATD1 = 1
#define SW1_SetLow()           _LATD1 = 0
#define SW1_Toggle()           _LATD1 ^= 1
#define SW1_GetValue()         _RD1
#define SW1_SetDigitalInput()  _TRISD1 = 1
#define SW1_SetDigitalOutput() _TRISD1 = 0

#define SW2_SetHigh()          _LATD0 = 1
#define SW2_SetLow()           _LATD0 = 0
#define SW2_Toggle()           _LATD0 ^= 1
#define SW2_GetValue()         _RD0
#define SW2_SetDigitalInput()  _TRISD0 = 1
#define SW2_SetDigitalOutput() _TRISD0 = 0

#define SW3_SetHigh()          _LATC13 = 1
/**
  @Summary
    Sets the GPIO pin, RC13, low using LATC13.

  @Description
    Sets the GPIO pin, RC13, low using LATC13.

  @Preconditions
    The RC13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC13 low (0)
    SW3_SetLow();
    </code>

*/
#define SW3_SetLow()           _LATC13 = 0
/**
  @Summary
    Toggles the GPIO pin, RC13, using LATC13.

  @Description
    Toggles the GPIO pin, RC13, using LATC13.

  @Preconditions
    The RC13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RC13
    SW3_Toggle();
    </code>

*/
#define SW3_Toggle()           _LATC13 ^= 1
/**
  @Summary
    Reads the value of the GPIO pin, RC13.

  @Description
    Reads the value of the GPIO pin, RC13.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RC13
    postValue = SW3_GetValue();
    </code>

*/
#define SW3_GetValue()         _RC13
/**
  @Summary
    Configures the GPIO pin, RC13, as an input.

  @Description
    Configures the GPIO pin, RC13, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC13 as an input
    SW3_SetDigitalInput();
    </code>

*/
#define SW3_SetDigitalInput()  _TRISC13 = 1
/**
  @Summary
    Configures the GPIO pin, RC13, as an output.

  @Description
    Configures the GPIO pin, RC13, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC13 as an output
    SW3_SetDigitalOutput();
    </code>

*/
#define SW3_SetDigitalOutput() _TRISC13 = 0
/**
  @Summary
    Sets the GPIO pin, RD3, high using LATD3.

  @Description
    Sets the GPIO pin, RD3, high using LATD3.

  @Preconditions
    The RD3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD3 high (1)
    PWM_VIBR_SetHigh();
    </code>

*/
#define PWM_VIBR_SetHigh()          _LATD3 = 1
/**
  @Summary
    Sets the GPIO pin, RD3, low using LATD3.

  @Description
    Sets the GPIO pin, RD3, low using LATD3.

  @Preconditions
    The RD3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD3 low (0)
    PWM_VIBR_SetLow();
    </code>

*/
#define PWM_VIBR_SetLow()           _LATD3 = 0
/**
  @Summary
    Toggles the GPIO pin, RD3, using LATD3.

  @Description
    Toggles the GPIO pin, RD3, using LATD3.

  @Preconditions
    The RD3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RD3
    PWM_VIBR_Toggle();
    </code>

*/
#define PWM_VIBR_Toggle()           _LATD3 ^= 1
/**
  @Summary
    Reads the value of the GPIO pin, RD3.

  @Description
    Reads the value of the GPIO pin, RD3.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RD3
    postValue = PWM_VIBR_GetValue();
    </code>

*/
#define PWM_VIBR_GetValue()         _RD3
/**
  @Summary
    Configures the GPIO pin, RD3, as an input.

  @Description
    Configures the GPIO pin, RD3, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD3 as an input
    PWM_VIBR_SetDigitalInput();
    </code>

*/
#define PWM_VIBR_SetDigitalInput()  _TRISD3 = 1
/**
  @Summary
    Configures the GPIO pin, RD3, as an output.

  @Description
    Configures the GPIO pin, RD3, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD3 as an output
    PWM_VIBR_SetDigitalOutput();
    </code>

*/
#define PWM_VIBR_SetDigitalOutput() _TRISD3 = 0
/**
  @Summary
    Sets the GPIO pin, RD8, high using LATD8.

  @Description
    Sets the GPIO pin, RD8, high using LATD8.

  @Preconditions
    The RD8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD8 high (1)
    LED6_SetHigh();
    </code>

*/


#define SW4_SetHigh()          _LATC14 = 1
#define SW4_SetLow()           _LATC14 = 0
#define SW4_Toggle()           _LATC14 ^= 1
#define SW4_GetValue()         _RC14
#define SW4_SetDigitalInput()  _TRISC14 = 1
#define SW4_SetDigitalOutput() _TRISC14 = 0


#define LED6_SetHigh()          _LATD8 = 1
/**
  @Summary
    Sets the GPIO pin, RD8, low using LATD8.

  @Description
    Sets the GPIO pin, RD8, low using LATD8.

  @Preconditions
    The RD8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD8 low (0)
    LED6_SetLow();
    </code>

*/
#define LED6_SetLow()           _LATD8 = 0
/**
  @Summary
    Toggles the GPIO pin, RD8, using LATD8.

  @Description
    Toggles the GPIO pin, RD8, using LATD8.

  @Preconditions
    The RD8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RD8
    LED6_Toggle();
    </code>

*/
#define LED6_Toggle()           _LATD8 ^= 1
/**
  @Summary
    Reads the value of the GPIO pin, RD8.

  @Description
    Reads the value of the GPIO pin, RD8.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RD8
    postValue = LED6_GetValue();
    </code>

*/
#define LED6_GetValue()         _RD8
/**
  @Summary
    Configures the GPIO pin, RD8, as an input.

  @Description
    Configures the GPIO pin, RD8, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD8 as an input
    LED6_SetDigitalInput();
    </code>

*/
#define LED6_SetDigitalInput()  _TRISD8 = 1
/**
  @Summary
    Configures the GPIO pin, RD8, as an output.

  @Description
    Configures the GPIO pin, RD8, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD8 as an output
    LED6_SetDigitalOutput();
    </code>

*/
#define LED6_SetDigitalOutput() _TRISD8 = 0
/**
  @Summary
    Sets the GPIO pin, RF12, high using LATF12.
*/
#define LED7_SetHigh()          _LATD11 = 1
#define LED7_SetLow()           _LATD11 = 0
#define LED7_Toggle()           _LATD11 ^= 1
#define LED7_GetValue()         _RD11
#define LED7_SetDigitalInput()  _TRISD11 = 1
#define LED7_SetDigitalOutput() _TRISD11 = 0
/**
  @Preconditions
    The RF12 must be set to an output.
*/
#define LED8_SetHigh()          _LATD9 = 1
#define LED8_SetLow()           _LATD9 = 0
#define LED8_Toggle()           _LATD9 ^= 1
#define LED8_GetValue()         _RD9
#define LED8_SetDigitalInput()  _TRISD9 = 1
#define LED8_SetDigitalOutput() _TRISD9 = 0

 /**
  @Param
    None.

  @Example
    <code>
    // Set RF12 high (1)
    PWM_FAN_SetHigh();
    </code>

*/
#define PWM_FAN_SetHigh()          _LATF12 = 1
/**
  @Summary
    Sets the GPIO pin, RF12, low using LATF12.

  @Description
    Sets the GPIO pin, RF12, low using LATF12.

  @Preconditions
    The RF12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF12 low (0)
    PWM_FAN_SetLow();
    </code>

*/
#define PWM_FAN_SetLow()           _LATF12 = 0
/**
  @Summary
    Toggles the GPIO pin, RF12, using LATF12.

  @Description
    Toggles the GPIO pin, RF12, using LATF12.

  @Preconditions
    The RF12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RF12
    PWM_FAN_Toggle();
    </code>

*/
#define PWM_FAN_Toggle()           _LATF12 ^= 1
/**
  @Summary
    Reads the value of the GPIO pin, RF12.

  @Description
    Reads the value of the GPIO pin, RF12.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RF12
    postValue = PWM_FAN_GetValue();
    </code>

*/
#define PWM_FAN_GetValue()         _RF12
/**
  @Summary
    Configures the GPIO pin, RF12, as an input.

  @Description
    Configures the GPIO pin, RF12, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF12 as an input
    PWM_FAN_SetDigitalInput();
    </code>

*/
#define PWM_FAN_SetDigitalInput()  _TRISF12 = 1
/**
  @Summary
    Configures the GPIO pin, RF12, as an output.

  @Description
    Configures the GPIO pin, RF12, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF12 as an output
    PWM_FAN_SetDigitalOutput();
    </code>

*/
#define PWM_FAN_SetDigitalOutput() _TRISF12 = 0



/**
    Section: Function Prototypes
*/
/**
  @Summary
    Configures the pin settings of the PIC24EP128GP206
    The peripheral pin select, PPS, configuration is also handled by this manager.

  @Description
    This is the generated manager file for the PIC24 / dsPIC33 / PIC32MM MCUs device.  This manager
    configures the pins direction, initial state, analog setting.
    The peripheral pin select, PPS, configuration is also handled by this manager.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    void SYSTEM_Initialize(void)
    {
        // Other initializers are called from this function
        PIN_MANAGER_Initialize();
    }
    </code>

*/
void PIN_MANAGER_Initialize (void);


#endif
