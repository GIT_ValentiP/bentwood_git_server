/**
  PIN MANAGER Generated Driver File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.c

  @Summary:
    This is the generated manager file for the PIC24 / dsPIC33 / PIC32MM MCUs device.  This manager
    configures the pins direction, initial state, analog setting.
    The peripheral pin select, PPS, configuration is also handled by this manager.

  @Description:
    This source file provides implementations for PIN MANAGER.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.95-b-SNAPSHOT
        Device            :  PIC24EP128GP206
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.36
        MPLAB 	          :  MPLAB X v5.10
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/


/**
    Section: Includes
*/

#include <stdio.h>
#include "pin_manager.h"
#include <ctype.h>
#include <stdlib.h>
#include "sd-spi.h"
#include "../ppsnew.h"

/**
 Section: File specific functions
*/

/**
 Section: Driver Interface Function Definitions
*/
void PIN_MANAGER_Initialize (void)
{
    ANSELA=0x0000;
    ANSELB=0x0000;
    ANSELC=0x0000;
    ANSELD=0x0000;
    ANSELE=0x0000;
    ANSELG=0x0000;  
    
	TRISA = 0b0000000000000000;			// 
	TRISB = 0b0000000000000000;			// RB10 - RELAY MACINA 
	TRISC = 0b0000000000011100;			// 
	TRISD = 0b0000000000000000;			// 
	TRISE = 0b0000000000000000;			// 
	TRISF = 0b0000000000000000;			// 
	TRISG = 0b0100000001000000;			//  

	TRISGbits.TRISG15=0;		//buzzer
	TRISDbits.TRISD15=0;		//SPI
	TRISBbits.TRISB14=0;		//SPI
	TRISBbits.TRISB15=1;		//SPI
	TRISDbits.TRISD14=0;		//SPI SD
	TRISFbits.TRISF12=0;		//ENC
	TRISDbits.TRISD1=0;		//retro pwm
	TRISBbits.TRISB7=0;		//TX-EN  ?
	TRISBbits.TRISB8=0;		//RX-EN  ?
	LATBbits.LATB7=1;  // ?????
	LATBbits.LATB8=0;  // ?????
	LATFbits.LATF12=1;		// CS ENC disattivo
	LATDbits.LATD14=1;		// CS SD disattivo

    TRISBbits.TRISB5=0;		//reset
    LATBbits.LATB5 =1;		//reset disattivo
    
    // Configurazione UART
    LATFbits.LATF5=0;
    LATFbits.LATF4=0;
    TRISFbits.TRISF5 = 0;   //F5 as output
    TRISFbits.TRISF4 = 1;   //F4 as input
    
    //Configurazione loads
    ANSELBbits.ANSB9 = 0;      //RB9 OUTPressino set to digital
    ANSELBbits.ANSB11 = 0;      //RB11 INPressino set to digital

    TRISDbits.TRISD3 = 0; //RD3 out PWM VIbro
    TRISFbits.TRISF12 = 0; //RD3 out PWM Ventole
    TRISBbits.TRISB10 = 0; //RB10 out Relay
    TRISBbits.TRISB9 = 0;       //RB9 OUTPressino
    TRISBbits.TRISB11 = 1;      //RB11 INPressino
    
    
    PPSUnLock;   
    // Configurazione PIN SDI
    PPSIn(_SDI1, _RPI47);       
	// Outputs
    PPSOut(_SCK1, _RP79);      // 
    PPSOut(_SDO1, _RP104);      // 
    
    // Configurazione PIN UART
    RPOR9bits.RP101R = 1;       //RF5/RP101 as U1TX
    RPINR18bits.U1RXR = 100;    //RP100/RF4 as U1RX 
    RPOR1bits.RP67R = 16;           //RD3->OC1:OC1
    
	PPSLock;

    while(OSCCONbits.LOCK != 1) ClrWdt();   

}


