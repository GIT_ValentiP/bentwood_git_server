/* 
 * File:   LCD.h
 * Author: marco
 *
 * Created on 24 giugno 2019, 0.39
 */

#ifndef LCD_H
#define	LCD_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* LCD_H */

void CountdownLCD(unsigned short time, unsigned char maxtime_s, unsigned char refreshtime);
void CountIncLCD(unsigned short time, unsigned char refreshtime);