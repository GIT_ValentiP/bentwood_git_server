/**
  Loads File

  @Company
    K-TRONIC

  @File Name
    loads.c

  @Summary
    This is the C file regarding the management of serial monitor

  @Description
    This C file provides APIs for the management of serial monitor.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC24EP512GU810
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/

#include "Driver_MV/pin_manager.h"
//#include "oc1.h"
#include "Driver_MV/tmr2.h"

#include <ctype.h>
#include <stdlib.h>
#include "sd-spi.h"


unsigned char Flag_Enable_PWMVibr=0;


void EnableFan(void){
    
    PWM_FAN_SetHigh();
}

void DisableFan(void){
    
    PWM_FAN_SetLow();
}


void EnablePWMVibr(unsigned char status){
    
    Flag_Enable_PWMVibr = status;
    if(!status){
        
            /* Stop the Timer */
        TMR2_Stop();

        /*Disable the interrupt*/
        IEC0bits.T2IE = 0;
        IFS0bits.T2IF = 0;
        OC1CON1 = 0x0000;
    }
}

void PWMVibr(void){
    
    //TCKPS 1:8; T32 16 Bit; TON enabled; TSIDL disabled; TCS FOSC/2; TGATE disabled; 
    T2CON = 0x8010;
    
    if(Flag_Enable_PWMVibr){
        //bit 13 OCSIDL: Stop Output Compare x in Idle Mode Control bit
        //      1 = Output Compare x halts in CPU Idle mode
        //      0 = Output Compare x continues to operate in CPU Idle mode
        //bit 12-10 OCTSEL<2:0>: Output Compare x Clock Select bits
        //      111 = Peripheral clock (FP)
        //      110 = Reserved
        //      101 = Reserved
        //      100 = Clock source of T1CLK is the clock source of OCx (only the synchronous clock is supported)
        //      011 = Clock source of T5CLK is the clock source of OCx
        //      010 = Clock source of T4CLK is the clock source of OCx
        //      001 = Clock source of T3CLK is the clock source of OCx
        //      000 = Clock source of T2CLK is the clock source of OCx
        //bit 9 ENFLTC: Fault C Input Enable bit
        //      1 = Output Compare Fault C input (OCFC) is enabled
        //      0 = Output Compare Fault C input (OCFC) is disabled
        //bit 8 ENFLTB: Fault B Input Enable bit
        //      1 = Output Compare Fault B input (OCFB) is enabled
        //      0 = Output Compare Fault B input (OCFB) is disabled
        //bit 7 ENFLTA: Fault A Input Enable bit
        //      1 = Output Compare Fault A input (OCFA) is enabled
        //      0 = Output Compare Fault A input (OCFA) is disabled
        //bit 6 OCFLTC: PWM Fault C Condition Status bit
        //      1 = PWM Fault C condition on OCFC pin has occurred
        //      0 = No PWM Fault C condition on OCFC pin has occurred
        //bit 5 OCFLTB: PWM Fault B Condition Status bit
        //      1 = PWM Fault B condition on OCFB pin has occurred
        //      0 = No PWM Fault B condition on OCFB pin has occurred
        //bit 4 OCFLTA: PWM Fault A Condition Status bit
        //      1 = PWM Fault A condition on OCFA pin has occurred
        //      0 = No PWM Fault A condition on OCFA pin has occurred
        //bit 3 TRIGMODE: Trigger Status Mode Select bit
        //      1 = TRIGSTAT (OCxCON2<6>) is cleared when
        //      0 = TRIGSTAT is cleared only by software
        //bit 2-0 OCM<2:0>: Output Compare Mode Select bits
        //      111 = Center-Aligned PWM mode: Output set high when OCxTMR = OCxR and set low when
        //          OCxTMR = OCxRS(1)
        //      110 = Edge-Aligned PWM mode: Output set high when OCxTMR = 0 and set low when
        //          OCxTMR = OCxR(1)
        //      101 = Double Compare Continuous Pulse mode: Initialize OCx pin low, toggle OCx state continuously
        //          on alternate matches of OCxR and OCxRS
        //      100 = Double Compare Single-Shot mode: Initialize OCx pin low, toggle OCx state on matches of
        //          OCxR and OCxRS for one cycle
        //      011 = Single Compare mode: Compare events with OCxR, continuously toggle OCx pin
        //      010 = Single Compare Single-Shot mode: Initialize OCx pin high, compare event with OCxR, forces
        //          OCx pin low
        //      001 = Single Compare Single-Shot mode: Initialize OCx pin low, compare event with OCxR, forces OCx
        //          pin high
        //      000 = Output compare channel is disabled
        OC1CON1 = 0x0000; // Turn off Output Compare 1 Module
        OC1R = 0x1116; // Initialize Compare Register1 with 0x0026
        OC1RS = 0x1116; // Initialize Secondary Compare Register1 with 0x0026
        OC1CON1 = 0x0006; // Load new compare mode to OC1CON
        //    PWM Frequency = 1Khz
        //    FOSC = 8MHz /2 = 4Mhz
        //    TCY = 1/FOSC = 0,25 us
        //    PWM Period = 1/PWM Frequency = 1/1 kHz = 1ms
        //    PWM Period = (PR2 + 1) * TCY * (Timer2 Prescale Value)
        //    1ms = (PR2 + 1) * 0,25us * 1
        //    PR2 = 4000
        //PR2 = 0x0FA0; // Initialize PR2 with 0x004C
        PR2 = 0x222D;
        IPC1bits.T2IP = 1; // Setup Output Compare 1 interrupt for
        IFS0bits.T2IF = 0; // Clear Output Compare 1 interrupt flag
        IEC0bits.T2IE = 1; // Enable Output Compare 1 interrupts
        T2CONbits.TON = 1; // Start Timer2 with assumed settings  
    }
}

void Enable_RelayMacina(void){
    
    Relay_Macina_SetHigh();
    
}

void Disable_RelayMacina(void){
    
    Relay_Macina_SetLow();
    
}    
