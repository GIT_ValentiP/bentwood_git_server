/**
  Communication keyboards File

  @Company
    K-TRONIC

  @File Name
    Communication_Keyboards.c

  @Summary
    This is the C file regarding the management of serial monitor

  @Description
    This C file provides APIs for the management of serial monitor.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC24EP512GU810
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


//#include "mcc_generated_files/mcc.h"
#include "loads.h"
#include "Driver_MV/pin_manager.h"
#include "Driver_MV/tmr1.h"
#include "Driver_MV/tmr2.h"
#include "MainDemo.h"
#include "LCD.h"
#include "driver_MV/uart1.h"

 
unsigned char Comm_State=0;
unsigned char Flag_EnableComm=0;
unsigned char val =  'F';
unsigned char old_val = 'E';
unsigned short Vread = 0;
unsigned short count_down = 0;
unsigned short count_temp = 0; 
unsigned short checkMan = 0;
unsigned short start_timer_macin_manual = 0;
unsigned short ok_macin_manual = 0;
unsigned short ok_macin_auto = 0;
unsigned long int i =0 ;
unsigned short int FiltroCheck = 0;
unsigned short hys_val =  40;
//unsigned char* buffer;

void EnableComm(unsigned char status){
    
    Flag_EnableComm = status;
}

void CommStart(void)
{
    char a[3] ="00";
    unsigned char found= 0;
    //Disable_Led(val,'0');
    //LCDCls();
   
       
		switch(Comm_State) {

			case 0: 
                val = UART1_Read();
				switch(val) 
                {
                    //printf("\nOK");
					case 'Y':                                          
                        Comm_State = 1;
                        Config_LED();
                        //IEC0bits.U1RXIE = 0;
                        //UART1_Write('R');
                        //LCDPutStringXY(5,5, "OK");
                    break;
 
                    default:
                        Comm_State = 0;
				} 
            break;

			case 1:
                
                LATAbits.LATA2 = 1;
                UART1_Enable();
                Enable_Mec_Button();
                Enable_CapSense();
                TMR1_Stop(); //reset all timer

                Comm_State = 2;

            break;
            
            case 2: // Button Reading
              switch(buff_obj.type) 
              {
                case 'W': //means mec button
                    switch(buff_obj.button) 
                    {
                    case 64:
                        val = val +1;
                        Hys_CapSense(val);
                    break;

                    case 4:                                          
                        val = val -1;
                        Hys_CapSense(val);
                    break;

                    }
                break;   
              }
            break;

        }
        
}


void Enable_Led(unsigned char index, unsigned char type){
    
    //type='0' = blue
    //type='1' = white
    
    UART1_Write('S');
    UART1_Write(index);
    UART1_Write('D');
    UART1_Write('D');
    UART1_Write('D');
    UART1_Write('D');
    UART1_Write('o');
    UART1_Write(type);
    UART1_Write('H');    
}

void Disable_Led(unsigned char index, unsigned char type){
    
    //type='0' = blue
    //type='1' = white
    
    UART1_Write('S');
    UART1_Write(index);
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('o');
    UART1_Write(type);
    UART1_Write('H');    
}

void Config_LED(void){
    //white
//    UART1_Write('G');
//    UART1_Write('2');
//    UART1_Write('1');
//    UART1_Write('i');
//    UART1_Write('1');
//    UART1_Write('H');
    
    //Blue
    UART1_Write('G');
    UART1_Write('2');
    UART1_Write('1');
    UART1_Write('i');
    UART1_Write('0');
    UART1_Write('H');
    
}
void Enable_ALL_Led_White(void){
    

    UART1_Write('N');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('p');
    UART1_Write('1');
    UART1_Write('H');
}

void Disable_ALL_Led_White(void){
    
    UART1_Write('N');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('p');
    UART1_Write('1');
    UART1_Write('H');
    
}

void Disable_ALL_Led_Blue(void){
    
    UART1_Write('N');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('p');
    UART1_Write('0');
    UART1_Write('H');
}

void Hys_CapSense(unsigned char val){
    char hex[2];
    sprintf(hex, "%x", val);
    
    UART1_Write('W');
    UART1_Write(hex[0]);
    UART1_Write(hex[1]);
    UART1_Write('H');
}

void Disable_CapSense(void){
    UART1_Write('C');
    UART1_Write('0');
}

void Enable_CapSense(void){
    UART1_Write('C');
    UART1_Write('1');
}

void Enable_Mec_Button(void){
    UART1_Write('M');
    UART1_Write('a');
}

void Disable_Mec_Button(void){
    UART1_Write('M');
    UART1_Write('b');
}

void Enable_Only_Mec_Button3(void){
    UART1_Write('M');
    UART1_Write('3');
}