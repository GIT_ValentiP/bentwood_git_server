#include "maindemo.h"

	

volatile WORD tick10=0;
volatile BYTE second_10=0,second_30=0;
#ifdef USA_SW_RTC 
volatile PIC24_RTCC_DATE currentDate;
volatile PIC24_RTCC_TIME currentTime;
#else
PIC24_RTCC_DATE currentDate;
PIC24_RTCC_TIME currentTime;
#endif
volatile DWORD milliseconds;
volatile BYTE CommTimer;					// timeout in ricezione 



// ---------------------------------------------------------------------------------------
void _ISR __attribute__((__no_auto_psv__)) _AddressError(void) {
	Nop();
	Nop();
	}

void _ISR __attribute__((__no_auto_psv__)) _StackError(void) {
	Nop();
	Nop();
	}
	

//void __attribute__ (( interrupt, /*shadow*/,  no_auto_psv )) _T2Interrupt(void) {
//// dev'essere 30Hz
//	static BYTE divider1s=0;
//
////	WriteTimer2(0);	// WRITETIMER0(0) dovrebbe essere la macro!
////  TMR2=0;		non serve su PIC24...
////	kLED0_IO ^= 1; 
////	SD_CS^=1;
//
//	divider1s++;
//
//	//second_30=1;					// flag
//	if(!(divider1s & 1)) {		// 
//		second_10=1;					// flag
//		tick10++;
//		}
//	if(divider1s==30) {		// per RealTimeClock
//		if(CommTimer)CommTimer--;
//		divider1s=0;
//        second_30=1;					// flag
//        #ifdef USA_SW_RTC 
//                milliseconds+=1000;
//                bumpClock();
//        #endif
//		}
//
//#if !defined(__PIC24EP512GU810__)
//	T2_Clear_Intr_Status_Bit; 	
//#else
//	IFS0bits.T2IF = 0; 			//Clear the Timer2 interrupt status flag STRONZI
//#endif
//	}
//

void __attribute__ (( interrupt, /*shadow*/,  no_auto_psv )) _T4Interrupt(void) {
// in mSec per spegnere Buzzer
	
//	WriteTimer4(0);	// WRITETIMER0(0) dovrebbe essere la macro!
//  TMR4=0;		non serve su PIC24...
//	LED0_IO ^= 1; 


#ifdef USA_BUZZER_PWM 		// se no � on-off
	OC2CON1 &= ~0x0006;   // off
#else
	LATGbits.LATG15=0;   // off
#endif

//	IFS0bits.T3IF = 0; 			//Clear the Timer3 interrupt status flag
	DisableIntT4;
#if !defined(__PIC24EP512GU810__)
	T4_Clear_Intr_Status_Bit; 	
#else
	IFS1bits.T4IF = 0; 			//Clear the Timer4 interrupt status flag (...)
#endif
	}



void bumpClock(void) {

	while(milliseconds >= 1000) {
		milliseconds -= 1000;
		currentTime.sec++;
		if(currentTime.sec >= 60) {
			currentTime.sec=0;
			currentTime.min++;
			if(currentTime.min >= 60) {
				currentTime.min=0;
				currentTime.hour++;
				if(currentTime.hour >= 24) {
					currentTime.hour=0;
					currentDate.mday++;
					if(currentDate.mday >= 30) {		// finire...
						currentDate.mday=0;
						currentDate.mon++;
						if(currentDate.mon >= 12) {		// 
							currentDate.mon=0;
							currentDate.year++;
							}
						}
					}
				}
			} 
		} 
	}

