/**
  CTMU Management File

  @Company
    Microchip Technology Inc.

  @File Name
    CTMU.c

  @Summary
    This is the generated driver implementation file for the ADC driver using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This source file provides implementations for driver APIs for ADC.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  2.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB             :  MPLAB X 5.10
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

/**
  Section: Included Files
*/

#include <xc.h>
#include <stdlib.h>
#include <math.h>
#include "mcc_generated_files/CTMU.h"
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/Tip_And_Tricks.h"
#include "mcc_generated_files/PCA9685.h"


#define VADC_LIMIT_HIGH 71 //%
#define VADC_LIMIT_LOW 69 //%


#define MAX_DELAY 500 //effetive val max 5461

#define NUM_SAMPLE_CHECK 5
#define HYSTERISIS  16//40
#define NUM_OVER_HYSTERISIS 2//4



#define START_ADDRESS_MEM_CTMU  0x00
#define S_ADDRESS_CAPSENSE_1 0x00
#define S_ADDRESS_CAPSENSE_2 0x09
#define S_ADDRESS_CAPSENSE_3 0x12
#define S_ADDRESS_CAPSENSE_4 0x1B
#define S_ADDRESS_CAPSENSE_5 0x24
#define S_ADDRESS_CAPSENSE_6 0x2D
#define S_ADDRESS_CAPSENSE_7 0x36
#define S_ADDRESS_CAPSENSE_8 0x3F
#define S_ADDRESS_CAPSENSE_9 0x48
#define S_ADDRESS_CAPSENSE_10 0x51
#define S_ADDRESS_CAPSENSE_11 0x5A
#define S_ADDRESS_CAPSENSE_12 0x63
#define NUM_PARAM_EE 0x09

//Data MatrixCapSense 12x10:
//[Ch, Time, Current, Trim, Vread_media, numSampleCheck, hysteresis, NumberOverHysteresis, Vadc_limitH, Vadc_limitL]
//[Ch, Time, Current, Trim, Vread_media, numSampleCheck, hysteresis, NumberOverHysteresis, Vadc_limitH, Vadc_limitL]
//[Ch, Time, Current, Trim, Vread_media, numSampleCheck, hysteresis, NumberOverHysteresis, Vadc_limitH, Vadc_limitL]
// .   .       .       .        .               .           .               .        
// .   .       .       .        .               .           .               .        
// .   .       .       .        .               .           .               .        
//[Ch, Time, Current, Trim, Vread_media, numSampleCheck, hysteresis, NumberOverHysteresis, Vadc_limitH, Vadc_limitL]
unsigned short MatrixCapSense[NUM_BUTTCAP][NUM_PARAMETERS];
unsigned int cap_vadcH[NUM_BUTTCAP] = {71, 71, 71, 71, 71, 71, 71, 71, 71, 71, 71, 71};
unsigned int cap_vadcL[NUM_BUTTCAP] = {69, 69, 69, 69, 69, 69, 69, 69, 69, 69, 69, 69};

unsigned int cap_vad[NUM_BUTTCAP][MAX_BUFFER_VADC];
unsigned int cur_ix_cap=0;
unsigned int cur_jx_cap=MAX_BUFFER_VADC-1;


int cap_trimL[NUM_BUTTCAP] = {-31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, -31};
int cap_trimH[NUM_BUTTCAP] = {31, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32};
unsigned int cap_overHyst[NUM_BUTTCAP] = {2, 2, 2,2, 2, 2, 2, 2, 2, 2, 2, 2};
unsigned int cap_limite_ins[NUM_BUTTCAP] = {11, 12, 13,13, 12, 13, 9, 10, 10, 10, 9, 11};
unsigned int cap_sample[NUM_BUTTCAP] = {NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK, NUM_SAMPLE_CHECK};
// senza messa a terra 
//unsigned int cap_Hyst[NUM_BUTTCAP] = {50, 60, 65, 70, 80, 68, 80, 80, 60, 60, 60, 60};
//smontato
//unsigned int cap_Hyst[NUM_BUTTCAP] = {28, 30, 20, 20, 22, 17, 21, 30, 13, 17, 24, 24};
unsigned int cap_Hyst[NUM_BUTTCAP] = {28, 30, 20, 20, 22, 17, 21, 30, 13, 17, 24, 24};

unsigned int buttons[NUM_BUTTCAP] = {10, 8, 9, 11, 13, 0, 1, 2, 3, 5, 6, 12};
unsigned char buffer_cap_sense[6] ={0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
unsigned char* buffer;
unsigned char flag_analysis_CapSense = '0';
unsigned char flag_enable_CapSense = '0';

void CTMU_Initialize(void){ 
    /**************************************************************************/
    //Set up CTMU;
    /**************************************************************************/
    
    //CTMUCONH
    //make sure CTMU is disabled
    CTMUCONHbits.CTMUEN = 0;    //CTMU Enable bit (1 = Module is enabled;0 = Module is disabled)
    CTMUCONHbits.CTMUSIDL = 0;  //Stop in Idle Mode bit
                                //1 = Discontinue module operation when device enters Idle mode
                                //0 = Continue module operation in Idle mode
    CTMUCONHbits.TGEN = 0;      //Time Generation Enable bit
                                //1 = Enables edge delay generation
                                //0 = Disables edge delay generation
    CTMUCONHbits.EDGEN = 0;     //Edge Enable bit
                                //1 = Edges are not blocked
                                //0 = Edges are blocked
    CTMUCONHbits.EDGSEQEN = 0;  //Edge Sequence Enable bit
                                //1 = Edge 1 event must occur before Edge 2 event can occur
                                //0 = No edge sequence is needed
    CTMUCONHbits.IDISSEN = 0;   //Analog Current Source Control bit
                                //1 = Analog current source output is grounded
                                //0 = Analog current source output is not grounded
    CTMUCONHbits.CTTRIG = 0;   //CTMU Special Event Trigger Control Bit
                               //1 = CTMU Special Event Trigger is enabled
                               //0 = CTMU Special Event Trigger is disabled

    //CTMUCONL
    //CTMU continues to run when emulator is stopped,CTMU continues
    //to run in idle mode,Time Generation mode disabled, Edges are blocked
    //No edge sequence order, Analog current source not grounded, trigger
    //output disabled, Edge2 polarity = positive level, Edge2 source =
    //source 0, Edge1 polarity = positive level, Edge1 source = source 0,
    //CTMUICON - CTMU Current Control Register
    CTMUCONLbits.EDG2POL = 1;    //Edge 2 Polarity Select bit
                                 //1 = Edge 2 programmed for a positive edge response
                                 //0 = Edge 2 programmed for a negative edge response 
    CTMUCONLbits.EDG2SEL = 0;   //Edge 2 Source Select bits
                                //11 = CTED1 pin; 10 = CTED2 pin; 
                                //01 = ECCP1 Special Event Trigger; 00 = ECCP2 Special Event Trigger
    CTMUCONLbits.EDG1POL = 1;   //Edge 1 Polarity Select bit
                                //1 = Edge 1 programmed for a positive edge response
                                //0 = Edge 1 programmed for a negative edge response
    CTMUCONLbits.EDG1SEL = 0;   //Edge 1 Source Select bits
                                //11 = CTED1 pin; 10 = CTED2 pin; 
                                //01 = ECCP1 Special Event Trigger; 00 = ECCP2 Special Event Trigger
    CTMUCONLbits.EDG2STAT = 0;  //Edge 2 Status bit
                                //1 = Edge 2 event has occurred
                                //0 = Edge 2 event has not occurred
    CTMUCONLbits.EDG1STAT = 0;  //Edge 1 Status bit
                                //1 = Edge 1 event has occurred
                                //0 = Edge 1 event has not occurred
      
    //CTMUICON
    CTMUICONbits.ITRIM = 0b000000;     //Current Source Trim bits
                                //011111 = Maximum positive change from nominal current
                                //011110
                                //...
                                //000001 = Minimum positive change from nominal current
                                //000000 = Nominal current output specified by IRNG<1:0>
                                //111111 = Minimum negative change from nominal current
                                //...
                                //100010
                                //100001 = Maximum negative change from nominal current
    CTMUICONbits.IRNG = 1; //Current Source Range Select bits
                                //11 = 100 x Base current
                                //10 = 10 x Base current
                                //01 = Base current level
                                //00 = Current source disabled
    
    
    PIR3bits.CTMUIF =0; //CTMUIF: CTMU Interrupt Flag bit
                        //1 = CTMU interrupt occurred (must be cleared in software)
                        //0 = No CTMU interrupt occurred
    
}

void CTMU_Inizialize_MatrixCapSense(void){   
    int i;
    for (i=0; i<NUM_BUTTCAP;i++ ){

        MatrixCapSense[i][0]  = i+1; //channel
        MatrixCapSense[i][1]  = 0; //time
        MatrixCapSense[i][2]  = 0; //current
        MatrixCapSense[i][3]  = 0; //trim
        MatrixCapSense[i][4]  = 0; //Vadc
        MatrixCapSense[i][5]  = cap_sample[i];
        MatrixCapSense[i][6] = cap_Hyst[i];          //Hysterisis amplitude
        MatrixCapSense[i][7] = 720;// soglia adc button not pressed //cap_overHyst[i];   //number of times that Vread is over Hysterisis
        MatrixCapSense[i][8] = cap_vadcH[i];   //High limit to evaluate capsens
        MatrixCapSense[i][9] = cap_vadcL[i];   //Low limit to evaluate capsens
        MatrixCapSense[i][10] = 0;   //status pressed not pressed
    }
   //CTMU_Print();
}

void CTMU_Inizialize_Port(void){
    //set channel 0,1,2,3 PORTA as an input (CAPbutton 6-7-8-9)
    //set channel 0,1,2,3,4,5 PORTB as an input (CAPbutton 12-1-2-3-4-5)
    //set channel 0,1 PORTE as an input (CAPbutton 10-11)
    
    // TASTO_CAP_xx set as input and as analog input
   
    TASTO_CAP_01_SetDigitalInput();
    TASTO_CAP_01_SetAnalogMode();  
 
    TASTO_CAP_02_SetDigitalInput();
    TASTO_CAP_02_SetAnalogMode();
    
    TASTO_CAP_03_SetDigitalInput();
    TASTO_CAP_03_SetAnalogMode();

    TASTO_CAP_04_SetDigitalInput();
    TASTO_CAP_04_SetAnalogMode();    
    
    TASTO_CAP_05_SetDigitalInput();
    TASTO_CAP_05_SetAnalogMode();    
    
    TASTO_CAP_06_SetDigitalInput();
    TASTO_CAP_06_SetAnalogMode();
    
    TASTO_CAP_07_SetDigitalInput();
    TASTO_CAP_07_SetAnalogMode();    
    
    TASTO_CAP_08_SetDigitalInput();
    TASTO_CAP_08_SetAnalogMode();    
    
    TASTO_CAP_09_SetDigitalInput();
    TASTO_CAP_09_SetAnalogMode();

    TASTO_CAP_10_SetDigitalInput();
    TASTO_CAP_10_SetAnalogMode();

    TASTO_CAP_11_SetDigitalInput();
    TASTO_CAP_11_SetAnalogMode();

    TASTO_CAP_12_SetDigitalInput();
    TASTO_CAP_12_SetAnalogMode();
}

//Enable CTMU
void CTMU_Enabling(void) {
    CTMUCONHbits.CTMUEN = 1; // Enable the CTMU
    CTMUCONLbits.EDG1STAT = 0; // Set Edge status bits to zero
    CTMUCONLbits.EDG2STAT = 0;
}

//Get Voltage for 
unsigned short CTMU_GetVoltage(unsigned short delay){
    // Consider Paragaph 19.4.2 (Relative Charge Measurement) pag319
    unsigned short Vread;
//    unsigned char* reg;
    
    INTERRUPT_PeripheralInterruptDisable();
    PIE1bits.RC1IE = 0;
    
    CTMU_Enabling();
    CTMUCONHbits.IDISSEN = 1; //drain charge on the circuit
    
    // delay is defined in us
    //delay min = 1; delay max = 5461
    //reg[0] is MSB; reg[1] is LSB
 /* provvisoriamente per verificare che avvenga la scarica della capacit� del tasto mettiamo un delay fisso 
  */
//    TMR0_GetRegValues(delay);
//    TMR0_StartTimer();
//    INTCONbits.TMR0IF = 0;
//    while(!(INTCONbits.TMR0IF));
//    TMR0_StopTimer();

    __delay_us(125);
    CTMUCONHbits.IDISSEN = 0; //end drain of circuit
    CTMUCONLbits.EDG1STAT = 1; //Begin charging the circuit
    //using CTMU current source

    TMR0_GetRegValues(delay);
    TMR0_StartTimer();
    INTCONbits.TMR0IF = 0;
    while(!(INTCONbits.TMR0IF));
    TMR0_StopTimer();
    
    //DELAY; //wait for 125us
    CTMUCONLbits.EDG1STAT = 0; //Stop charging circuit
    PIR1bits.ADIF = 0; //make sure A/D Int not set
    ADCON0bits.GO=1; //and begin A/D conv.
    while(!PIR1bits.ADIF); //Wait for A/D convert complete
    Vread = ADRES; //Get the value from the A/D
    PIR1bits.ADIF = 0; //Clear A/D Interrupt Flag
    
    INTERRUPT_PeripheralInterruptEnable();
    PIE1bits.RC1IE = 1;
    
    return Vread;
}


//R = Ricalibra filtri Proximity  --> prevedere un errore in caso di calibrazione errata
//Calibration data = [Channel, Time, Current, Trim, Vread_media, numSampleCheck,... 
//                      hysteresis, NumberOverHysteresis ]
unsigned char CTMU_ChannelCalibs(unsigned int *buttons, unsigned char channel){
    unsigned char calib_ok =0;
    unsigned short Vread=0;
    unsigned short num_element;
    unsigned long Vsum=0;
    unsigned short i;
    unsigned short delay;
    unsigned char current;
    short int iTrim;
    int var;
    float Vread_med;
    float VadcLimH;
    float VadcLimL;
    

    VadcLimH = MatrixCapSense[channel][8] * (1023/100);
    VadcLimL = MatrixCapSense[channel][9] * (1023/100);
    //printf("\n\rVadcLimH = %5.1f\t", VadcLimH);
    //printf("\n\rVadcLimL = %5.1f\t", VadcLimL);
    
    ADC_SelectChannel(buttons[channel]);
    //DEBUG
    //printf("\n\rADCON0bits.CHS=%u\t", ADCON0bits.CHS);
    //printf("\n\rPORT-%u\t", buttons[channel]);
    
    for (current = 1; current <= 3; current++){
        CTMUICONbits.IRNG = current;

        for ( iTrim = cap_trimL[channel]; iTrim < cap_trimH[channel]; iTrim++ ){
            CTMUICONbits.ITRIM = iTrim & 0x3F;
            
            var=0;
            for (delay =MAX_DELAY/2; delay < MAX_DELAY; delay=delay+var){
                num_element = MatrixCapSense[channel][5];
                Vsum =0;
                for (i=0; i<MatrixCapSense[channel][5]; i++)
                {   
                    Vread = CTMU_GetVoltage(delay);
                    //printf("%u ",Vread);
                    if(Vread >10){
                        Vsum = Vsum + Vread;
                    }else
                    {
                        num_element--;
                    }
                }
                Vread_med = Vsum/num_element;
                //printf("Vread_med =%4.1f ",Vread_med);
                if (Vread_med < VadcLimH && Vread_med > VadcLimL)
                {
                    calib_ok = 1;
                }
                else
                {
                   calib_ok = 0; 
                }
                if (calib_ok){
                    break;
                }
                else
                {
                    if(abs(var)%2==0){
                        var=abs(var)+2;
                    }else{
                        var=(var+2)*-1;
                    }
                }
            }
            if (calib_ok)break;
        }
        if (calib_ok)break;
    }
    
    if (calib_ok)
    {
        MatrixCapSense[channel][0] = (unsigned short)(channel+1);
        MatrixCapSense[channel][1] = delay;
        MatrixCapSense[channel][2] = current;
        MatrixCapSense[channel][3] = iTrim;
        MatrixCapSense[channel][4] = Vread_med;
        MatrixCapSense[channel][7] = Vread_med;
       
        /*Riempio la prima volta il buffer */
        for(i =0;i<MAX_BUFFER_VADC;i++){
            cap_vad[channel][i]=Vread_med;
            
        }
        cap_limite_ins[channel]+=(cap_limite_ins[channel]*20)/100;
    }
    else{}
    
    //ADCON0bits.ADON = 0;
    
    return calib_ok;
 
}


//Get Calibration array data for channel = [Channel, Time, Current, Trim, Vread_media, 
//                      numSampleCheck, hysteresis, NumberOverHysteresis, SogliaHigh, SogliaLow]
unsigned short* CTMU_GetCalibsValue(unsigned char channel){
    
    unsigned short* Calibs;
    
    Calibs[0] = MatrixCapSense[0][0];    //channel
    Calibs[1] = MatrixCapSense[1][1];    //time
    Calibs[2] = MatrixCapSense[2][2];    //current
    Calibs[3] = MatrixCapSense[3][3];    //trim
    Calibs[4] = MatrixCapSense[4][4];    //Vadc
    Calibs[5] = MatrixCapSense[5][5];    // num sample
    Calibs[6] = MatrixCapSense[6][6];    //Hysterisis amplitude
    Calibs[7] = MatrixCapSense[7][7];    //number of times that Vread is over Hysterisis
    Calibs[8]= VADC_LIMIT_HIGH;
    Calibs[9]= VADC_LIMIT_LOW;  
    
    return Calibs;
    
}


//User can set the parameters for each capsense channel
//[Channel, Time, Current, Trim, - ,numSampleCheck, hysteresis, NumberOverHysteresis, SogliaHigh, SogliaLow]
void CTMU_SetCalibValue(unsigned char channel, unsigned char time, unsigned char current, unsigned char Trim, unsigned char numSampleCheck, unsigned char hysteresis, unsigned short overHys, unsigned char VadcLimitH, unsigned char VadcLimitL, unsigned char W_EE){
    
    MatrixCapSense[channel-1][0] = channel;
    MatrixCapSense[channel-1][1] = time * 2;
    MatrixCapSense[channel-1][2] = current;
    MatrixCapSense[channel-1][3] = Trim - 31;
    //MatrixCapSense[channel][4] Vread not set
    MatrixCapSense[channel-1][5] = numSampleCheck;
    MatrixCapSense[channel-1][6] = hysteresis;
    MatrixCapSense[channel-1][7] = overHys;
    MatrixCapSense[channel-1][8] = VadcLimitH;
    MatrixCapSense[channel-1][9] = VadcLimitL;
    
    if (W_EE)CTMU_WriteButtonEE(channel-1);
}

//User can set the hysteresis for each capsense channel
void CTMU_SetHysteresis(unsigned char hysteresis){
    
    unsigned char ch;
    for (ch =0; ch<NUM_BUTTCAP; ch++)
    {
        MatrixCapSense[ch][6] = hysteresis;
    }
    
    
}

//MatrixCapSense[i][4] = 0; //Vadc media (default, after calib)
//MatrixCapSense[i][6] = HYSTERISIS;          //Hysterisis amplitude
//MatrixCapSense[i][7] = NUM_OVER_HYSTERISIS;   //number of times that Vread is over Hysterisis
unsigned char CTMU_ChannelProximity(unsigned char channel, unsigned short Vread){

    unsigned char button_pressed =0;
    unsigned char i;
    unsigned short Vadc_H = MatrixCapSense[channel][7]+ MatrixCapSense[channel][6]/2;
    unsigned short Vadc_L = MatrixCapSense[channel][7]- MatrixCapSense[channel][6]/2;
    
//    for (i=0;i<MatrixCapSense[channel][7]; i++){
//        if(Vread>Vadc_H){
//            button_pressed=0;
//        }
//        else if(Vread<Vadc_L){
//            button_pressed=1;
//        }
//        else{}
//        if (!button_pressed)break;
//    }
     if(Vread>Vadc_H){
            button_pressed=0;
        }
        else if(Vread<Vadc_L){
            button_pressed=1;
        }
    return button_pressed; 
}



 
 
unsigned char CTMU_ChannelEvaluation(unsigned char channel){
    unsigned short Vread=0;
    float Vread_med=0;
    short delta_stop_th=0;
    unsigned long Vsum;
    unsigned short i;
    unsigned char ok_button_pressed =0;
    unsigned short num_element;
    
    ADC_SelectChannel(buttons[channel]);
    
    CTMUICONbits.IRNG = MatrixCapSense[channel][2]; //source current to supply at capsense channel x 
    CTMUICONbits.ITRIM = MatrixCapSense[channel][3]; //trim value 
    
    // DEBUG   
    //    printf("\n\rADCON0bits.CHS=%u\t", ADCON0bits.CHS);
    //    printf("\n\rPORT-%u\t", buttons[channel]);
    //    printf("\n\rTasto[%u]\t", channel+1);
    //    printf("delay = %u\t", MatrixCapSense[channel][1]);
    //    printf("I = %u\t", MatrixCapSense[channel][2]);
    //    printf("trim = %i\t", MatrixCapSense[channel][3]);
    
    Vsum =0;
    num_element = MatrixCapSense[channel][5]; //num of sample of capsense channel to read  
    //printf("\n\r");
    
    
  
    
    for (i=0; i<MatrixCapSense[channel][5]; i++)
    {   
        Vread = CTMU_GetVoltage(MatrixCapSense[channel][1]);
        
        if(Vread >10){
            Vsum = Vsum + Vread;
        }else
        {
            num_element--;
        }
    }
    Vread_med = Vsum/num_element;
    
    delta_stop_th=MatrixCapSense[channel][7]-Vread_med;
    if(delta_stop_th<0)
        delta_stop_th*=-1;
    
    if(delta_stop_th < cap_limite_ins[channel]){
    //cur_jx_cap[]: indice vettore media campionatura 
        cap_vad[channel][cur_jx_cap] = Vread_med;    
    }
        
    
  
    ok_button_pressed = CTMU_ChannelProximity(channel, Vread_med);
    
    
   //if (flag_analysis_CapSense == '1'){
        MatrixCapSense[channel][4] = Vread_med;
    //}    
    if (ok_button_pressed)
    {
        //printf("\n\r");
       // printf("TASTO_%u PRESSED",channel+1);
    }
    else
    {   
        //printf("TASTO_%u UNPRESSED",channel+1);   
    }

    return ok_button_pressed;
}
 



//Acquisition Voltage and Evaluate if button is pressed
//Indice   0       1       2       3    4          5            6          7
//Array [Channel, time, current, Trim, Vadc, numSampleCheck, hysteresis, overHys]
//************* TARTATURA CTMU***********
// T aa bb cc dd ee ff gg hh ii l H
// esempio : T 01 F5 02 22 0A 28 04 48 46 1 H
//
//Posizione 0 aa = channel
//Posizione 1 bb = timer (resolution 2)
//Posizione 2 cc = current
//Posizione 3 dd = Trim (offset -31)
//Posizione 4 ee = numSampleCheck
//Posizione 5 ff = hysteresis
//Posizione 6 gg = overHys
//Posizione 7 hh = VadcLimitH
//Posizione 8 ii = VadcLimitL
//Posizione 9 l = 0/1 = no memorization EEPROM /memorization EEPROM


//ACquisition Voltage for each cap sense
void CTMU_Acquisition(void){
 
    //unsigned int buttons[NUM_BUTTCAP] = {10, 8};
    unsigned char i;

    //CTMU_Inizialize_MatrixCapSense();
    for (i = 0; i < NUM_BUTTCAP; ++i)
    {
        CTMU_ChannelCalibs(buttons,i);
    }
    CTMU_WriteMatrixEE();
  
   
    EUSART1_Write('Y');
  
    
   
    //flag_enable_CapSense = 1;
//    EUSART1_Write('R');
//    EUSART1_Write('E');
//    EUSART1_Write('E');
    
}

//Print MatrixCapSense
void CTMU_Print(void){
    unsigned char i,j;
    printf  ("\n\r");
    for (i = 0; i < NUM_BUTTCAP; i++)
    {
       
        for (j = 0; j < NUM_PARAMETERS; j++)
        {
            if(j==3){
                printf  ("%i\t", MatrixCapSense[i][j]);
            }else{
                printf  ("%u\t", MatrixCapSense[i][j]);
        
            }
        }
        printf  ("\n\r");
    }
    printf  ("\n     \r");
    //printf  ("\n\r");
}



//Print MatrixCapSense
void Capsense_Print(void){
    unsigned char i,j;
    for (i = 0; i < NUM_BUTTCAP; i++){
        EUSART1_Write('\n');
        EUSART1_Write('\r');
                
        for (j = 0; j < NUM_PARAMETERS; j++){
            EUSART1_Write(MatrixCapSense[i][j]);            
        }
    }
    EUSART1_Write('\n');
    EUSART1_Write('\r');
    EUSART1_Write('\n');
    EUSART1_Write('\r');
    EUSART1_Write('1');
}

//Evaluate Vread and Button pressed for each button
void CTMU_ButtonEvaluation(void){
     
    unsigned char vect_cap[NUM_BUTTCAP] = {0,0,0,0,0,0,0,0,0,0,0,0};
    unsigned char ok_button_pressed =0;
    unsigned char i,j;
    unsigned char check =0;
    unsigned char premuto=18;   
    float avg_cap;
    
    
    
    
    cur_jx_cap = (cur_ix_cap+ MAX_BUFFER_VADC - 1) % MAX_BUFFER_VADC;
   
    
    for(i=0;i<NUM_BUTTCAP;i++)
    {
       avg_cap=0;
        //Media con buffer pieno 
        for(j =0;j<MAX_BUFFER_VADC;j++){
            avg_cap = avg_cap + cap_vad[i][j];
        }
       MatrixCapSense[i][7]=(unsigned short)(avg_cap / MAX_BUFFER_VADC);
      
              
       ok_button_pressed= CTMU_ChannelEvaluation(i);
       vect_cap[i] = ok_button_pressed;
       MatrixCapSense[i][10] = ok_button_pressed;   //status
  	   if(ok_button_pressed & !check){
           check=1;
           
           if (i==0){
               //EUSART1_Write('1');
               buffer_cap_sense[4] = buffer_cap_sense[4] | 0x01;
               PCA9685_SET_LED_BLUE_ON(9,0xff,0xff);
               premuto=9;
               
           }else if(i==1){
               //EUSART1_Write('2');
               buffer_cap_sense[4] = buffer_cap_sense[4] | 0x02;
               PCA9685_SET_LED_BLUE_ON(2,0xFF,0xFF);
               premuto=2;
           }else if(i==2){
               //EUSART1_Write('3');
               buffer_cap_sense[4] = buffer_cap_sense[4] | 0x04;
               PCA9685_SET_LED_BLUE_ON(1,0xFF,0xFF);
               premuto=1;
           }else if(i==3){
               //EUSART1_Write('4');
               buffer_cap_sense[4] = buffer_cap_sense[4] | 0x08;
               PCA9685_SET_LED_BLUE_ON(3,0xFF,0xFF);
               premuto=3;
           }else if(i==4){
               //EUSART1_Write('5');
               buffer_cap_sense[4] = buffer_cap_sense[4] | 0x10;
               PCA9685_SET_LED_BLUE_ON(3,0xFF,0xFF);
               premuto=3;
           }else if(i==5){
               //EUSART1_Write('6');
               buffer_cap_sense[4] = buffer_cap_sense[4] | 0x20;
               PCA9685_SET_LED_BLUE_ON(4,0xFF,0xFF);
               premuto=4;
           }else if(i==6){
               //EUSART1_Write('7');
               buffer_cap_sense[4] = buffer_cap_sense[4] | 0x40;
               PCA9685_SET_LED_BLUE_ON(5,0xFF,0xFF);
               premuto=5;
           }else if(i==7){
               //EUSART1_Write('8');
               buffer_cap_sense[4] = buffer_cap_sense[4] | 0x80;
               PCA9685_SET_LED_BLUE_ON(8,0xFF,0xFF);
               premuto=8;
           }else if(i==8){
               //EUSART1_Write('9');
               buffer_cap_sense[3] = buffer_cap_sense[3] | 0x01;
               PCA9685_SET_LED_BLUE_ON(6,0xFF,0xFF);
               premuto=6;
           }else if(i==9){
               //EUSART1_Write('A');
               buffer_cap_sense[3] = buffer_cap_sense[3] | 0x02;
               PCA9685_SET_LED_BLUE_ON(6,0xFF,0xFF);
               premuto=6;
           }else if(i==10){
               //EUSART1_Write('B');
               buffer_cap_sense[3] = buffer_cap_sense[3] | 0x04;
               PCA9685_SET_LED_BLUE_ON(9,0xFF,0xFF);
               premuto=9;
           }else{
               //EUSART1_Write('C');
               buffer_cap_sense[3] = buffer_cap_sense[3] | 0x08;
               PCA9685_SET_LED_BLUE_ON(9,0xFF,0xFF);
               premuto=9;
           }
       }
       else{
           

             
           if (i==0){
               buffer_cap_sense[4] = buffer_cap_sense[4] & 0xFE;
           }else if(i==1){
               buffer_cap_sense[4] = buffer_cap_sense[4] & 0xFD;
           }else if(i==2){
               buffer_cap_sense[4] = buffer_cap_sense[4] & 0xFB;
           }else if(i==3){
               buffer_cap_sense[4] = buffer_cap_sense[4] & 0xF7;
           }else if(i==4){
               buffer_cap_sense[4] = buffer_cap_sense[4] & 0xEF;
           }else if(i==5){
               buffer_cap_sense[4] = buffer_cap_sense[4] & 0xDF;
           }else if(i==6){
               buffer_cap_sense[4] = buffer_cap_sense[4] & 0xBF;
           }else if(i==7){
               buffer_cap_sense[4] = buffer_cap_sense[4] & 0x7F;
           }else if(i==8){
               buffer_cap_sense[3] = buffer_cap_sense[3] & 0xFE;
           }else if(i==9){
               buffer_cap_sense[3] = buffer_cap_sense[3] & 0xFD;
           }else if(i==10){
               buffer_cap_sense[3] = buffer_cap_sense[3] & 0xFB;
           }else{
               buffer_cap_sense[3] = buffer_cap_sense[3] & 0xF7;
           }
           
          // premuto=3;
        
           
      
         // __delay_ms(10);
          
         /* PCA9685_SET_LED_BLUE_ON(0,0x00,0x00);
           PCA9685_SET_LED_BLUE_ON(1,0x00,0x00);
           PCA9685_SET_LED_BLUE_ON(2,0x00,0x00);
           PCA9685_SET_LED_BLUE_ON(3,0x00,0x00);
           PCA9685_SET_LED_BLUE_ON(4,0x00,0x00);
           PCA9685_SET_LED_BLUE_ON(5,0x00,0x00);
           PCA9685_SET_LED_BLUE_ON(6,0x00,0x00);
           PCA9685_SET_LED_BLUE_ON(7,0x00,0x00);
           PCA9685_SET_LED_BLUE_ON(8,0x00,0x00);
           PCA9685_SET_LED_BLUE_ON(9,0x00,0x00);*/
      if(premuto!=i)
                PCA9685_SET_LED_BLUE_ON(i,0,0);
       }
      
          
    }
    
    cur_ix_cap = (cur_ix_cap + 1) % (MAX_BUFFER_VADC);
   
    if(flag_enable_CapSense== '1' & check ==1){
        buffer_cap_sense[0] = 'A';
        buffer_cap_sense[1] = 'C';
        buffer_cap_sense[2] = 0xC;
        buffer_cap_sense[5] = buffer_cap_sense[0]+buffer_cap_sense[1]+buffer_cap_sense[2]+buffer_cap_sense[4]+buffer_cap_sense[3]+0x20;
        buffer = buffer_cap_sense;
        EUSART1_BufferWrite(buffer,6);
        //PrintArray(vect_cap,NUM_BUTTCAP);
    }
    
    if (flag_analysis_CapSense == '1'){
        CTMU_Print();
        __delay_ms(300);
        printf("\033[2J");
        printf("\033[H");   
        
    }
}

//Write CTMU Matrix data in EE
void CTMU_WriteMatrixEE(void){
    unsigned short count_add = START_ADDRESS_MEM_CTMU;
    unsigned char val_mem;
    unsigned char i,j;
    for(i=0; i<NUM_BUTTCAP; i++){
        printf("\n\r");
        for (j=0; j<NUM_PARAMETERS; j++){
            if (j ==4){ //Vread no memorized
                
            }
            else{
                if (j==1){ //time
                    val_mem = (unsigned char)(MatrixCapSense[i][j]/2);
                }else if(j==3){ //Trim
                    val_mem = (unsigned char)(MatrixCapSense[i][j]+31);
                }
                else{
                    val_mem = (unsigned char)MatrixCapSense[i][j];
                }
                //printf(" %u",val_mem);
                //printf(" %u",MatrixCapSense[i][j]);
                DATAEE_WriteByte(count_add, val_mem);
                count_add++;     
            } 
        }
    }
}

//Read CTMU data in EE
void CTMU_ReadMatrixEE(void){
    unsigned short count_add = START_ADDRESS_MEM_CTMU;
    unsigned char val_mem;
    unsigned char row,col;
    
    for (row=0;row<NUM_BUTTCAP; row++){
        
        for (col=0; col<NUM_PARAMETERS; col++ ){
            
            if(col !=4){
                val_mem = DATAEE_ReadByte(count_add);

                if (col ==1){ //time
                    MatrixCapSense[row][col] = val_mem *2;
                }else if(col ==3){ //Trim
                    MatrixCapSense[row][col] = val_mem -31;
                }
                else{
                    MatrixCapSense[row][col] = val_mem;
                }
                count_add++;
            }
            else{}
        }
    }
}

//Write CTMU data for single button in EE
void CTMU_WriteButtonEE(unsigned char channel){
    
    unsigned char s_add,col;
    unsigned char val_mem;
    unsigned short i;
    
   
    if (channel ==0) s_add=S_ADDRESS_CAPSENSE_1;
    else if (channel ==1) s_add=S_ADDRESS_CAPSENSE_2;
    else if (channel ==2) s_add=S_ADDRESS_CAPSENSE_3;
    else if (channel ==3) s_add=S_ADDRESS_CAPSENSE_4;
    else if (channel ==4) s_add=S_ADDRESS_CAPSENSE_5;
    else if (channel ==5) s_add=S_ADDRESS_CAPSENSE_6;
    else if (channel ==6) s_add=S_ADDRESS_CAPSENSE_7;
    else if (channel ==7) s_add=S_ADDRESS_CAPSENSE_8;
    else if (channel ==8) s_add=S_ADDRESS_CAPSENSE_9;
    else if (channel ==9) s_add=S_ADDRESS_CAPSENSE_10;
    else if (channel ==10) s_add=S_ADDRESS_CAPSENSE_11;
    else if (channel ==11) s_add=S_ADDRESS_CAPSENSE_12;
    
    col=0;
    for (i=s_add; i < s_add + NUM_PARAM_EE;i++){
        
        if (col==1){ //time
            val_mem = (unsigned char)(MatrixCapSense[channel][col]/2);
        }else if(col==3){ //Trim
            val_mem = (unsigned char)(MatrixCapSense[channel][col]+31);
        }
        else{
            val_mem = (unsigned char)MatrixCapSense[channel][col];
        }
        DATAEE_WriteByte(i, val_mem);
        col++;
        if(col==4)col++; //skip colomn 4 (Vread)
    }
    
}


/**
 End of File
*/