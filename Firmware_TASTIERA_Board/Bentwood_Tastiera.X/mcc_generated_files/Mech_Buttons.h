/**
  Mechanical Buttons Header File

  @Company
    K-TRONIC

  @File Name
    Mech_Buttons.h

  @Summary
    This is the header file for manage the 10 Mechanical buttons

  @Description
    This header file provides APIs for manage the 10 Mechanical buttons.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/
extern unsigned char flag_enable_Mec;

void Mech_Buttons_Get_Port(void);
void Mech_Buttons_Get_Port_debug(void);