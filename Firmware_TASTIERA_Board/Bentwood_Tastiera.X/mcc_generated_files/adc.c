/**
  ADC Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    adc.c

  @Summary
    This is the generated driver implementation file for the ADC driver using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This source file provides implementations for driver APIs for ADC.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  2.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB             :  MPLAB X 5.10
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

/**
  Section: Included Files
*/

#include <xc.h>
#include "adc.h"
#include "device_config.h"


/**
  Section: ADC Module APIs
*/

void ADC_Initialize(void)
{
// set the ADC to the options selected in the User Interface
    
    //ADCON0
    // GO_nDONE stop; ADON enabled; CHS AN0; 
    ADCON0bits.CHS = 0;         //Analog Channel Select bits
                                //00000 = AN0; 00001 = AN1; 00010 = AN2; 00011 = AN3
                                //00100 = AN4; 00101 = AN5(1); 00110 = AN6(1); 00111 = AN7(1)
                                //01000 = AN8; 01001 = AN9; 01010 = AN10; 01011 = AN11; 01100 = AN12
                                //01101 = AN13; 01110 = AN14; 01111 = AN15; 10000 = AN16
                                //10001 = AN17; 10010 = AN18; 10011 = AN19; 10100 = AN20(1)
                                //10101 = AN21(1); 10110 = AN22(1); 10111 = AN23(1); 11000 = AN24(1)
                                //11001 = AN25(1); 11010 = AN26(1); 11011 = AN27(1); 11100 = Reserved
                                //11101 = CTMU; 11110 = DAC; 11111 = FVR BUF2 (1.024V/2.048V/2.096V Volt Fixed Voltage Reference)(2)
    ADCON0bits.GO_nDONE = 0;    //A/D Conversion Status bit
                                //1 = A/D conversion cycle in progress. Setting this bit starts an A/D conversion cycle.
                                //This bit is automatically cleared by hardware when the A/D conversion has completed.
                                //0 = A/D conversion completed/not in progress
    ADCON0bits.ADON = 1;        //ADC Enable bit
                                //1 = ADC is enabled
                                //0 = ADC is disabled and consumes no operating current
    
    //ADCON1
    // TRIGSEL CCP5; NVCFG external; PVCFG external; 
    ADCON1bits.TRIGSEL = 0;     //Special Trigger Select bit
                                //1 = Selects the special trigger from CTMU
                                //0 = Selects the special trigger from CCP5
    ADCON1bits.NVCFG = 0;       //Positive Voltage Reference Configuration bits
                                //00 = A/D VREF+ connected to internal signal, AVDD
                                //01 = A/D VREF+ connected to external pin, VREF+
                                //10 = A/D VREF+ connected to internal signal, FVR BUF2
                                //11 = Reserved (by default, A/D VREF+ connected to internal signal, AVDD)
    ADCON1bits.PVCFG = 0;       //Negative Voltage Reference Configuration bits
                                //00 = A/D VREF- connected to internal signal, AVSS
                                //01 = A/D VREF- connected to external pin, VREF-
                                //10 = Reserved (by default, A/D VREF- connected to internal signal, AVSS)
                                //11 = Reserved (by default, A/D VREF- connected to internal signal, AVSS)
    
    //ADCON2       
    // ADFM left; ACQT 20; ADCS FOSC/4; 
    ADCON2bits.ADFM = 1;    //A/D Conversion Result Format Select bit
                            //1 = Right justified; 0 = Left justified
    ADCON2bits.ACQT = 1;    //A/D Acquisition time select bits. Acquisition time is the duration that the A/D charge
                            //holding capacitor remains connected to A/D channel from the instant the GO/DONE bit is set until
                            //conversions begins.
                            //000 = 0(1); 001 = 2 TAD; 010 = 4 TAD; 011 = 6 TAD
                            //100 = 8 TAD; 101 = 12 TAD; 110 = 16 TAD; 111 = 20 TAD
    ADCON2bits.ADCS = 6;    //A/D Conversion Clock Select bits
                            //000 = FOSC/2; 001 = FOSC/8; 010 = FOSC/32
                            //011 = FRC(1) (clock derived from a dedicated internal oscillator = 600 kHz nominal)
                            //100 = FOSC/4; 101 = FOSC/16; 110 = FOSC/64
                            //111 = FRC(1) (clock derived from a dedicated internal oscillator = 600 kHz nominal)
    // ADRESL 0; 
    ADRESL = 0x00;
    
    // ADRESH 0; 
    ADRESH = 0x00;
    
    // Enabling ADC interrupt.
    PIE1bits.ADIE = 1;
}

void ADC_SelectChannel(adc_channel_t channel)
{
	//Note
    //PORTA-CH0 (RA0) --> AN0 (00000b) TASTO-06
    //PORTA-CH1 (RA1) --> AN1 (00001b) TASTO-07
    //PORTA-CH2 (RA2) --> AN2 (00010b) TASTO-08
    //PORTA-CH3 (RA3) --> AN3 (00011b) TASTO-09
    //PORTB-CH0 (RB0) --> AN12 (01100b) TASTO-12
    //PORTB-CH1 (RB1) --> AN10 (01010b) TASTO-01
    //PORTB-CH2 (RB2) --> AN8 (01000b) TASTO-02
    //PORTB-CH3 (RB3) --> AN9 (01001b) TASTO-03
    //PORTB-CH4 (RB4) --> AN11 (01011b) TASTO-04
    //PORTB-CH5 (RB5) --> AN13 (01101b) TASTO-05
    //PORTE-CH0 (RE0) --> AN5 (00101b) TASTO-10
    //PORTE-CH1 (RE1) --> AN6 (00110b) TASTO-11	  
    // select the A/D channel
    
    ADCON0bits.CHS = channel;
    // Turn on the ADC module
    ADCON0bits.ADON = 1;  
}

void ADC_StartConversion()
{
    // Start the conversion
    ADCON0bits.GO_nDONE = 1;
}


bool ADC_IsConversionDone()
{
    // Start the conversion
    return ((bool)(!ADCON0bits.GO_nDONE));
}

adc_result_t ADC_GetConversionResult(void)
{
    // Conversion finished, return the result
     return ((adc_result_t)((ADRESH << 8) + ADRESL));
}

adc_result_t ADC_GetConversion(adc_channel_t channel)
{
    // select the A/D channel
    ADCON0bits.CHS = channel;    

    // Turn on the ADC module
    ADCON0bits.ADON = 1;

    // Start the conversion
    ADCON0bits.GO_nDONE = 1;

    // Wait for the conversion to finish
    while (ADCON0bits.GO_nDONE)
    {
    }

    // Conversion finished, return the result
    return ((adc_result_t)((ADRESH << 8) + ADRESL));
}

void ADC_TemperatureAcquisitionDelay(void)
{
    __delay_us(200);
}

void ADC_ISR(void)
{
    // Clear the ADC interrupt flag
    PIR1bits.ADIF = 0;
}
/**
 End of File
*/