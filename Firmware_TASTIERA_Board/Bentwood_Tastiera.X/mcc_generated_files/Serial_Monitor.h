/**
  Serial Monitor Header File

  @Company
    K-TRONIC

  @File Name
    Serial_Monitor.h

  @Summary
    This is the header file regarding the management of serial monitor

  @Description
    This header file provides APIs for the management of serial monitor.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/



void Serial_Monitor(void);
void Serial_Config_PCA9685(void);
void ShowVersion(void);
void Init_Config_PCA9685_Debug(void);



