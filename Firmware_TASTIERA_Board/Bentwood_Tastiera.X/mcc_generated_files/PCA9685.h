/**
  PCA9865 Header File

  @Company
    K-TRONIC

  @File Name
    PCA9865.h

  @Summary
    This is the header file for drive the PCA9865 driver using PIC18 MCUs

  @Description
    This header file provides APIs for PCA9865 LED driver.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


/***********************
  I2C Defines & Variable
************************/

#define PCA9685_MODE1 0x00
#define PCA9685_MODE2 0x01
    
#define PCA9685_WHITE_ADDRESS 0x41
#define PCA9685_BLUE_ADDRESS 0x40

#define PCA9685_ALLCALL 0x01
#define PCA9685_SLEEP 0x10
#define PCA9685_AI 0x20
#define PCA9685_OUTDRV 0x04
#define PCA9685_PRE_SCALE 0xFE
#define PCA9685_MAX_DUTY_CICLE 4095
#define PCA9685_REGISTERS_PER_CHANNEL 4 
#define PCA9685_LED0_ON_L 0x06  



void PCA9685_SET_MODE1(unsigned char Serial_Config_Command, unsigned char PCA9685_Address);
void PCA9685_SET_MODE2(unsigned char Serial_Config_Command, unsigned char PCA9685_Address);
void PCA9685_SET_PRESCALE(unsigned char Serial_Config_Command, unsigned char PCA9685_Address);
void PCA9685_ALL_LED_WHITE_OFF(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2);
void PCA9685_ALL_LED_BLUE_OFF(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2);
void PCA9685_ALL_LED_WHITE_ON(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2);
void PCA9685_ALL_LED_BLUE_ON(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2);
void PCA9685_SET_LED_WHITE_ON(unsigned char Serial_Config_Variable_2, unsigned char Serial_Config_Variable_1, unsigned char Index_LED);
void PCA9685_SET_LED_BLUE_ON(unsigned char Serial_Config_Variable_2, unsigned char Serial_Config_Variable_1, unsigned char Index_LED);


void PCA9685_Init_Default(void);
void PCA9685_Init_LED_Default(void);
//void PCA9685_ALL_LED_BLUE_ON(unsigned char PWM);      
//void PCA9685_ALL_LED_WHITE_ON(unsigned char PWM);    
void PCA9685_Init_LED_SW1(void);
void PCA9685_Init_LED_SW2(void);



