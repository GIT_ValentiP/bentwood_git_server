/**
  Tip and Tricks Driver File

  @Company
    K-TRONIC

  @File Name
    Tip_And_Tricks.c

  @Summary
    This is the C file for generical applications

  @Description
    This C file provides APIs for generical applications.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


#include "mcc_generated_files/mcc.h"

unsigned short Led_status = 10000;
unsigned char i=0;
unsigned char flag = 0;


// Funzione per generare una conversione in binario da un ASCII HEX
// ovvero prende due caratteri e li trasforma in HEX; esempio da '2' e '1' si ottine 0x21
// vale per caratteri da 0 a F
unsigned char ASCIItoHEX(unsigned char MSB, unsigned char LSB) 
{
    if (MSB > '9') MSB += 9;            // Convert MSB value to a contiguous range (0x30..0x?F)
    if (LSB > '9') LSB += 9;            // Convert LSB value to a contiguous range (0x30..0x?F)
    return (MSB <<4) | (LSB & 0x0F);    // Make a result byte using only low nibbles of MSB and LSB
}

// Funzione per generare una conversione in binario da un ASCII HEX in RAW
// ovvero prende due caratteri e li trasforma in HEX; esempio da '2' e '1' si ottine 0x21
unsigned char HEXtoRAW(unsigned char MSB, unsigned char LSB) 
{
    MSB = MSB-48;
    MSB = LSB-48;
    return MSB*10+LSB;
}

void PrintMatrix(unsigned short** matrix, int row, int column)
{
    int i, j;

    for (i = 0; i < row; i++)
    {
        printf  ("\n\r");
        for (j = 0; j < column; j++)
        {
            printf  ("%u\t", &matrix[i][j]);
        }
    }
}

void Toggle_Led_Status(void)
{   

}

void PrintArray(unsigned char* array, unsigned char num_element)
{
    unsigned char i;
    printf("\n\r");
    for (i = 0; i < num_element; i++)
    {
        printf("%u ", array[i]);
    }
    
}







