/**
  Mechanical Buttons Driver File

  @Company
    K-TRONIC

  @File Name
    Mech_Buttons.c

  @Summary
    This is the C file for manage the 10 Mechanical Buttons

  @Description
    This C file provides APIs for manage the 10 Mechanical Buttons.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/PCA9685.h"
#include "mcc_generated_files/pin_manager.h"
#include <xc.h>
#include "mcc_generated_files/Tip_And_Tricks.h"
#include "mcc_generated_files/tmr0.h"
#include "mcc_generated_files/eusart1.h"




#define TASTO_M01_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define TASTO_M02_SetDigitalInput()    do { TRISDbits.TRISD6 = 0; } while(0)
#define NUM_BUTTMEC 10

unsigned char flag_enable_Mec = 'b';
unsigned char* buffer;
unsigned char buffer_mec[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
static unsigned short keyboard_actual_mec=0x0000;
static unsigned short keyboard_old_mec=0x0000;
unsigned short delay = 32000;
//extern  unsigned char channel_to_calibrate;
//extern  CAP_RARAM_en  capsense_param_change;

typedef enum{
    NULL_PARAM=0,
    DELAY_TIME_PARAM=1, 
    TRIM_CURRENT_PARAM=3, 
    NUMADC_RD_PARAM=5, 
    HYST_PARAM=6,
    OVHYST_TIME_PARAM=7,  
    MAX_ID_PARAM=8,
}CAP_RARAM_en;
//unsigned short MatrixCapSense[NUM_BUTTCAP][NUM_PARAMETERS];
extern unsigned short MatrixCapSense[NUM_BUTTCAP][NUM_PARAMETERS];
unsigned char channel_to_calibrate;
CAP_RARAM_en  capsense_param_change;
//void CTMU_IncrParam(void);
//void CTMU_DecrParam(void);

void CTMU_IncrParam(){
     MatrixCapSense[channel_to_calibrate][capsense_param_change]++;
}
void CTMU_DecrParam(){
  //if  (MatrixCapSense[channel_to_calibrate][capsense_param_change]>0){
     MatrixCapSense[channel_to_calibrate][capsense_param_change]--;
 // }
}
void Mech_Buttons_Get_Port(void)
{
    unsigned char check =0;
    
    buffer_mec[3] = 0x00;
    buffer_mec[4] = 0x00;
    
   if (flag_enable_Mec =='a'){
        if(TASTO_M01_GetValue()==0) {//Tasto not used
            buffer_mec[4] = buffer_mec[4] | 0x01;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xFE;}

        if(TASTO_M02_GetValue()==0) {//Tasto PORTAFILTRO
              buffer_mec[4] = buffer_mec[4] | 0x02;
            check =1;
           
        } else{
               buffer_mec[4] = buffer_mec[4] & 0xFD;
              }    

        if(TASTO_M03_GetValue()==0) { //Tasto DECREMENTO    
            buffer_mec[4] = buffer_mec[4] | 0x04;
            check =1;
           
        } else{buffer_mec[4] = buffer_mec[4] & 0xFB;}

        if(TASTO_M04_GetValue()==0) {//Tasto MANUALE 
            buffer_mec[4] = buffer_mec[4] | 0x08;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xF7;}

        if(TASTO_M05_GetValue()==0) {//Tasto TAZZE_2    
            buffer_mec[4] = buffer_mec[4] | 0x10;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xEF;}

        if(TASTO_M06_GetValue()==0) {//Tasto TAZZA_1    
            buffer_mec[4] = buffer_mec[4] | 0x20;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xDF;}    

        if(TASTO_M07_GetValue()==0) { //Tasto INCREMENTO
            buffer_mec[4] = buffer_mec[4] | 0x40;
            check =1;
    } else{buffer_mec[4] = buffer_mec[4] & 0xBF;}

        if(TASTO_M08_GetValue()==0) {//Tasto CONFERMA    
            buffer_mec[4] = buffer_mec[4] | 0x80;
            check =1;           
        } else{buffer_mec[4] = buffer_mec[4] & 0x7F;}

        if(TASTO_M09_GetValue()==0) {//Tasto NOT Used 
            buffer_mec[3] = buffer_mec[3] | 0x01;
            check =1;
        } else{buffer_mec[3] = buffer_mec[3] & 0xFE;}

        if(TASTO_M010_GetValue()==0) {//Tasto MENU
            buffer_mec[3] = buffer_mec[3] | 0x02;
            check =1;         
        } else{buffer_mec[3] = buffer_mec[3] & 0xFB;}   
   }//end flag_enable_Mec =='a'
    
    if (flag_enable_Mec =='3'){    
        if(TASTO_M04_GetValue()==0) {  
            buffer_mec[4] = buffer_mec[4] | 0x08;
            check =1;
        } else{}        
    } else{}  
    
    if(check & flag_enable_Mec =='a'){
        buffer_mec[0] = 'A';
        buffer_mec[1] = 'W';
        buffer_mec[2] = 0xA;
        buffer_mec[5] = buffer_mec[0]+buffer_mec[1]+buffer_mec[2]+buffer_mec[4]+buffer_mec[3]+0x20;
        buffer = buffer_mec;
        EUSART1_BufferWrite(buffer,6);
    }
}




void Mech_Buttons_Get_Port_debug(void)
{
    //unsigned char* buffer_mec;
    unsigned char check =0;
    unsigned char id=0;
    unsigned short mask,not_mask;
    
    buffer_mec[3] = 0x00;
    buffer_mec[4] = 0x00;
    

        if(TASTO_M01_GetValue()==0) {//Tasto not used
            buffer_mec[4] = buffer_mec[4] | 0x01;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xFE;}

        if(TASTO_M02_GetValue()==0) {//Tasto PORTAFILTRO
            buffer_mec[4] = buffer_mec[4] | 0x02;
            check =1;
           
        } else{
               buffer_mec[4] = buffer_mec[4] & 0xFD;
              }    

        if(TASTO_M03_GetValue()==0) { //Tasto DECREMENTO
            buffer_mec[4] = buffer_mec[4] | 0x04;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xFB;}

        if(TASTO_M04_GetValue()==0) {//Tasto MANUALE
            buffer_mec[4] = buffer_mec[4] | 0x08;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xF7;}

        if(TASTO_M05_GetValue()==0) {//Tasto TAZZE_2
            buffer_mec[4] = buffer_mec[4] | 0x10;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xEF;}

        if(TASTO_M06_GetValue()==0) {//Tasto TAZZA_1
            buffer_mec[4] = buffer_mec[4] | 0x20;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xDF;}    

        if(TASTO_M07_GetValue()==0) { //Tasto INCREMENTO
            buffer_mec[4] = buffer_mec[4] | 0x40;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xBF;}

        if(TASTO_M08_GetValue()==0) {//Tasto CONFERMA
            buffer_mec[4] = buffer_mec[4] | 0x80;
            check =1;
           
        } else{buffer_mec[4] = buffer_mec[4] & 0x7F;}

        if(TASTO_M09_GetValue()==0) {//Tasto NOT Used
            buffer_mec[3] = buffer_mec[3] | 0x01;
            check =1;
        } else{buffer_mec[3] = buffer_mec[3] & 0xFE;}

        if(TASTO_M010_GetValue()==0) {//Tasto MENU
            buffer_mec[3] = buffer_mec[3] | 0x02;
            check =1;
         
        } else{buffer_mec[3] = buffer_mec[3] & 0xFB;}   

   
    if (flag_enable_Mec =='3'){    
        if(TASTO_M04_GetValue()==0) {
            buffer_mec[4] = buffer_mec[4] | 0x08;
            check =1;
        } else{}
        

        
    } else{}  
    
    
    if ((capsense_param_change < DELAY_TIME_PARAM)||(capsense_param_change > MAX_ID_PARAM-1)){
        capsense_param_change=DELAY_TIME_PARAM; 
    }          
    keyboard_actual_mec=buffer_mec[3];
    keyboard_actual_mec<<=8;
    keyboard_actual_mec|=buffer_mec[4];
    mask=0x0001;
    not_mask=~mask;
    for (id=0;id < 10;id++){
        mask=0x0001;
        mask<<=id;
        not_mask=~mask;
        if ((keyboard_actual_mec & mask)!=0){//button_ pressed
           
        }else if ((keyboard_old_mec & mask) !=0){//button released
                        switch (id+1){
                            case(3)://Tasto DECREMENTO
                                     CTMU_DecrParam();
                                     break;
                            case(7)://Tasto INCREMENTO
                                      CTMU_IncrParam();
                                     break;
                            case(2)://Tasto PORTAFILTRO DELAY_TIME_PARAM
                                    if (capsense_param_change==DELAY_TIME_PARAM){
                                       capsense_param_change=TRIM_CURRENT_PARAM; 
                                    }else if (capsense_param_change==TRIM_CURRENT_PARAM){
                                       capsense_param_change=NUMADC_RD_PARAM; 
                                    }else if (capsense_param_change==NUMADC_RD_PARAM){
                                       capsense_param_change=HYST_PARAM; 
                                    }else if (capsense_param_change==HYST_PARAM){
                                              capsense_param_change=OVHYST_TIME_PARAM; 
                                         }else if (capsense_param_change==OVHYST_TIME_PARAM){
                                                 capsense_param_change=DELAY_TIME_PARAM; 
                                              }else capsense_param_change=DELAY_TIME_PARAM; 
                                   
                                     break;
                            case(10)://Tasto MENU
                                    channel_to_calibrate++;
                                    if (channel_to_calibrate > (NUM_BUTTCAP-1)){
                                        channel_to_calibrate=0;
                                    }
                                     break;
                                     
                            default:
                                     break;
                        }         
              }
       
    }
    keyboard_old_mec=keyboard_actual_mec;

}
