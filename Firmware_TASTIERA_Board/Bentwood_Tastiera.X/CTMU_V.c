/**
  Mechanical Buttons Driver File

  @Company
    K-TRONIC

  @File Name
    Mech_Buttons.c

  @Summary
    This is the C file for manage the 10 Mechanical Buttons

  @Description
    This C file provides APIs for manage the 10 Mechanical Buttons.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/eusart1.h"
#include "mcc_generated_files/tmr0.h"
#include <xc.h>


//void CTMU_Initialize(void)
//{
///**************************************************************************/
///*Set up CTMU *****************************************************************/
///**************************************************************************/
//
////CTMU continues to run when emulator is stopped,CTMU continues
////to run in idle mode,Time Generation mode disabled, Edges are blocked
////No edge sequence order, Analog current source not grounded, trigger
////output disabled, Edge2 polarity = positive level, Edge2 source =
////source 0, Edge1 polarity = positive level, Edge1 source = source 0   
//    
////CTMUCONH/1 - CTMU Control registers
//CTMUCONH = 0x00; //make sure CTMU is disabled
//CTMUCONL = 0x90;
//
////CTMUICON - CTMU Current Control Register
//CTMUICON = 0x01; //0.55uA, Nominal - No Adjustment
//    
//}

void CTMU_Inizialize_Port(void)
{

    TASTO_CAP_01_SetDigitalInput();
    TASTO_CAP_01_SetAnalogMode();  
 
    TASTO_CAP_02_SetDigitalInput();
    TASTO_CAP_02_SetAnalogMode();
    
    TASTO_CAP_03_SetDigitalInput();
    TASTO_CAP_03_SetAnalogMode();

    TASTO_CAP_04_SetDigitalInput();
    TASTO_CAP_04_SetAnalogMode();    
    
    TASTO_CAP_05_SetDigitalInput();
    TASTO_CAP_05_SetAnalogMode();    
    
    TASTO_CAP_06_SetDigitalInput();
    TASTO_CAP_06_SetAnalogMode();
    
    TASTO_CAP_07_SetDigitalInput();
    TASTO_CAP_07_SetAnalogMode();    
    
    TASTO_CAP_08_SetDigitalInput();
    TASTO_CAP_08_SetAnalogMode();    
    
    TASTO_CAP_09_SetDigitalInput();
    TASTO_CAP_09_SetAnalogMode();

    TASTO_CAP_10_SetDigitalInput();
    TASTO_CAP_10_SetAnalogMode();

    TASTO_CAP_11_SetDigitalInput();
    TASTO_CAP_11_SetAnalogMode();

    TASTO_CAP_12_SetDigitalInput();
    TASTO_CAP_12_SetAnalogMode();    
    
}

void CTMU_Inizialize_Caps(void)
{   
    int i;
    int j = 0; //index for loop
    uint8_t Vread = 0;
    double VTot = 0;
    float Vavg=0, Vcal=0, CTMUISrc = 0; //float values stored for calcs
    
    
    CTMUCONHbits.CTMUEN = 1; //Enable the CTMU
    CTMUCONLbits.EDG1STAT = 0; // Set Edge status bits to zero
    CTMUCONLbits.EDG2STAT = 0;
    
    for(j=0;j<1;j++)
    {

        //STATUS_LED_SetHigh();
        CTMUCONHbits.IDISSEN = 1; //drain charge on the circuit
        
        TMR0_Setup(0x0F, 0xFF);
        TMR0_StartTimer();
        INTCONbits.TMR0IF = 0;
        while(!(INTCONbits.TMR0IF));
        TMR0_StopTimer();

        //STATUS_LED_Toggle();
        
        CTMUCONHbits.IDISSEN = 0; //end drain of circuit
        CTMUCONLbits.EDG1STAT = 1; //Begin charging the circuit
        //using CTMU current source
     
        //STATUS_LED_SetLow();
        
        TMR0_Setup(0x0F, 0xFF);
        TMR0_StartTimer();
        INTCONbits.TMR0IF = 0;
        while(!(INTCONbits.TMR0IF));
        TMR0_StopTimer();
        
        //DELAY; //wait for 125us
        CTMUCONLbits.EDG1STAT = 0; //Stop charging circuit
        PIR1bits.ADIF = 0; //make sure A/D Int not set
        ADCON0bits.GO=1; //and begin A/D conv.
        while(!PIR1bits.ADIF); //Wait for A/D convert complete
        Vread = ADRES; //Get the value from the A/D
        PIR1bits.ADIF = 0; //Clear A/D Interrupt Flag
        VTot += Vread; //Add the reading to the total
        
        //__delay_ms(500);
        //printf("Media: %f",Vread);
        //printf("\n\r");
       // __delay_ms(500);
        //STATUS_LED_SetHigh();
        /*
            if(LATCbits.LATC0 == 1)
            {
            LATCbits.LATC0 = 0;
            } else {LATCbits.LATC0 = 1;}
        */
        //printf("%u",Vread);
        //printf("\n\r");
        //printf('V');
        
    }
    
    //Vavg = (float)(VTot/10.000); //Average of 10 readings
    
        //printf("Media: %f",Vavg);
        //printf("\n\r");
    //EUSART1_Write(2);
    //__delay_ms(500);
}




