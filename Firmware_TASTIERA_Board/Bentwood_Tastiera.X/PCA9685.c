/**
  PCA9865 Driver File

  @Company
    K-TRONIC

  @File Name
    PCA9865.c

  @Summary
    This is the C file for drive the PCA9865 driver using PIC18 MCUs

  @Description
    This C file provides APIs for PCA9865 LED driver.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/PCA9685.h"


unsigned char i=0;                                // Counter
unsigned char s=0;                                // Switch

//unsigned char readBuffer[10];                     // Dati letti dallo slave
unsigned char writeBuffer[10];                    // Dati da scivere verso lo slave
    
//I2C2_TRANSACTION_REQUEST_BLOCK TRB[2];      // Lista delle comunicazioni I2C 
I2C2_MESSAGE_STATUS status;                 // I2C status
    

    
    
    /*
    Let us consider an example of designing a delay time of 10% and duty cycle of 30% at output pin LED0.
    The total count is fixed at 4096.

    Delay = 10% of 4096 = 409.6 ~ 410.
    Count starts from 0 to 4095 (must be subtracted by 1), so ON count = 409 = 0x199h.
    Thus, LED0_ON_H = 0x01h and LED_ON_L = 0x99h

    Duty cycle = 30% of 4096 = 1228.8 ~ 1229
    So OFF count = Delay + Duty cycle = 409 + 1229 = 1638 = 0x666h
    Thus, LED0_OFF_H = 0x06h and LED_OFF_L = 0x66h
     */  


void PCA9685_SET_MODE1(unsigned char Serial_Config_Command, unsigned char PCA9685_Address)
{
    writeBuffer[0] = 0x00;                       // MODE 1 
    writeBuffer[1] = Serial_Config_Command;      // Configuiamo AI=1 e ALLCALL=1

    I2C2_MasterWrite(writeBuffer, 2, PCA9685_Address, &status);
    while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
 }

void PCA9685_SET_MODE2(unsigned char Serial_Config_Command, unsigned char PCA9685_Address)
{
    writeBuffer[0] = 0x01;                       // MODE 1 
    writeBuffer[1] = Serial_Config_Command;      // Configuiamo AI=1 e ALLCALL=1

    I2C2_MasterWrite(writeBuffer, 2, PCA9685_Address, &status);
    while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
}

void PCA9685_SET_PRESCALE(unsigned char Serial_Config_Command, unsigned char PCA9685_Address)
{
    writeBuffer[0] = 0xFE;                       // PRESCALE register 
    writeBuffer[1] = Serial_Config_Command;      // frequenza da impostare

    I2C2_MasterWrite(writeBuffer, 2, PCA9685_Address, &status);
    while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
}

void PCA9685_ALL_LED_BLUE_ON(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2)
{
        writeBuffer[0] = (0xFA);                        // Control register set to address LEDn_ON_L 
        writeBuffer[1] = (Serial_Config_Variable_2&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[2] = (Serial_Config_Variable_1&0x0F);

        I2C2_MasterWrite(writeBuffer, 3, PCA9685_BLUE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
    
}

void PCA9685_ALL_LED_WHITE_ON(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2)
{
        writeBuffer[0] = (0xFA);                        // Control register set to address LEDn_ON_L 
        writeBuffer[1] = (Serial_Config_Variable_2&0xFF);                   // Writing 8 LSB bits of ON count 
        //writeBuffer[2] = ((Serial_Config_Variable_1&0xF00)>>8);
        writeBuffer[2] = (Serial_Config_Variable_1&0x0F);

        I2C2_MasterWrite(writeBuffer, 3, PCA9685_WHITE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
}

void PCA9685_ALL_LED_WHITE_OFF(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2)
{
    // ADDRESS -DATA_L -DATA_H
        writeBuffer[0] = (0xFC);                            // Control register set to address LEDn_ON_L 
        writeBuffer[1] = (Serial_Config_Variable_2&0xFF);   // Writing 8 LSB bits of ON count 
        //writeBuffer[2] = ((Serial_Config_Variable_1&0xF00)>>8);
        writeBuffer[2] = (Serial_Config_Variable_1&0x0F);
        
        I2C2_MasterWrite(writeBuffer, 3, PCA9685_WHITE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
}
void PCA9685_ALL_LED_BLUE_OFF(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2)
{
        //Serial_Config_Variable_1 = dato H
        //Serial_Config_Variable_2 = dato L
        
        writeBuffer[0] = (0xFC);                        // Control register set to address LEDn_ON_L 
        writeBuffer[1] = (Serial_Config_Variable_2&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[2] = (Serial_Config_Variable_1&0x0F);

        I2C2_MasterWrite(writeBuffer, 3, PCA9685_BLUE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
}

void PCA9685_SET_LED_WHITE_ON(unsigned char Serial_Index_LED, unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2)
{
        
        writeBuffer[0] = ((Serial_Index_LED*4)+6);                     // Control register set to address LEDn_ON_L 
        writeBuffer[1] = 0x00; //(0x01&0xFF);           // Writing 8 LSB bits of ON count 
        writeBuffer[2] = 0x00; //((0x99&0xF00)>>8);
        writeBuffer[3] = (Serial_Config_Variable_2&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[4] = (Serial_Config_Variable_1&0x0F);

        I2C2_MasterWrite(writeBuffer, 5, PCA9685_WHITE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING); 
}

void PCA9685_SET_LED_BLUE_ON(unsigned char Serial_Index_LED, unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2)
{
        writeBuffer[0] = ((Serial_Index_LED*4)+6);                     // Control register set to address LEDn_ON_L 
        writeBuffer[1] = 0x00; //(0x01&0xFF);           // Writing 8 LSB bits of ON count 
        writeBuffer[2] = 0x00; //((0x99&0xF00)>>8);
        writeBuffer[3] = (Serial_Config_Variable_2&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[4] = (Serial_Config_Variable_1&0x0F);

        I2C2_MasterWrite(writeBuffer, 5, PCA9685_BLUE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING);
}





  
/**
  PCA9685_Init_Default(void): Questa funzione inizzializza i due PCA solo per il MODE1
 *                              il MODE2 non viene considerato (per una configurazione
 *                              custom si veda PCA9685_Init_Custom).
*/    

void PCA9685_Init_Default(void)
{
    writeBuffer[0] = 0x00;      // MODE 1 
    writeBuffer[1] = 0x21;      // Configuiamo AI=1 e ALLCALL=1

    I2C2_MasterWrite(writeBuffer, 2, PCA9685_WHITE_ADDRESS, &status);
    while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
    
    writeBuffer[0] = 0x00;      // MODE 1 
    writeBuffer[1] = 0x21;      // Configuiamo AI=1 e ALLCALL=1

    I2C2_MasterWrite(writeBuffer, 2, PCA9685_BLUE_ADDRESS, &status);
    while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione    
}

/**
  PCA9685_Init_LED_Default(void): Questa funzione effettua i giochi di luce iniziali sui LED
 *                                  in una configurazione prestabilita, se si vogliono modificare 
 *                                  gli effetti luminosi iniziali si veda PCA9685_Init_LED_Custom
*/    

void PCA9685_Init_LED_Default(void)
{
    for(i=0; i<12; i++) 
    {
        if(i==1) {
            s=2;
        } else{}
        
        if(i==2) {
            s=1;
        } else{}
        
        if(i==7) {
            s=8;
        } else{}
        
        if(i==8) {
            s=6;
        } else{}

        if(i==10) {
            s=7;
        } else{}

        if(i==11) {
            s=9;
        } else{}
        
        writeBuffer[0] = ((s*4)+6);                     // Control register set to address LEDn_ON_L 
        writeBuffer[1] = 0x00; //(0x01&0xFF);           // Writing 8 LSB bits of ON count 
        writeBuffer[2] = 0x00; //((0x99&0xF00)>>8);
        writeBuffer[3] = (4095&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[4] = ((4095&0xF00)>>8);

        I2C2_MasterWrite(writeBuffer, 5, PCA9685_WHITE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
            
        __delay_ms(80);
        
        s=i;
	}
    
    
    for(i=0; i<11; i++) 
    {
        s=i;
        
        if(i==1) {
            s=2;
        } else{}
        
        if(i==2) {
            s=1;
        } else{}
       
        if(i==6) {
            s=8;
        } else{}
        
        if(i==7) {
            s=6;
        } else{}
    
        if(i==9) {
            s=7;
        } else{}

        if(i==10) {
            s=9;
        } else{}
   
        writeBuffer[0] = ((s*4)+6);                     // Control register set to address LEDn_ON_L 
        writeBuffer[1] = 0x00; //(0x01&0xFF);           // Writing 8 LSB bits of ON count 
        writeBuffer[2] = 0x00; //((0x99&0xF00)>>8);
        writeBuffer[3] = (4095&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[4] = ((4095&0xF00)>>8);

        I2C2_MasterWrite(writeBuffer, 5, PCA9685_BLUE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING);         // Attende la fine della cominicazione
            
        __delay_ms(80);
        
        s=i;
	}
    
        __delay_ms(500);
    
        PCA9685_ALL_LED_BLUE_OFF(0,0);
        
        __delay_ms(500);
    
        PCA9685_ALL_LED_WHITE_OFF(0,0);
    
        __delay_ms(500);

}


void PCA9685_Init_LED_SW1(void)
{
        writeBuffer[0] = ((0*4)+6);                     // Control register set to address LEDn_ON_L 
        writeBuffer[1] = 0x00; //(0x01&0xFF);           // Writing 8 LSB bits of ON count 
        writeBuffer[2] = 0x00; //((0x99&0xF00)>>8);
        writeBuffer[3] = (4095&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[4] = ((4095&0xF00)>>8);

        I2C2_MasterWrite(writeBuffer, 5, PCA9685_WHITE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING); 
        
        writeBuffer[0] = ((0*4)+6);                     // Control register set to address LEDn_ON_L 
        writeBuffer[1] = 0x00; //(0x01&0xFF);           // Writing 8 LSB bits of ON count 
        writeBuffer[2] = 0x00; //((0x99&0xF00)>>8);
        writeBuffer[3] = (4095&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[4] = ((4095&0xF00)>>8);

        I2C2_MasterWrite(writeBuffer, 5, PCA9685_BLUE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING);
}

void PCA9685_Init_LED_SW2(void)
{
        writeBuffer[0] = ((2*4)+6);                     // Control register set to address LEDn_ON_L 
        writeBuffer[1] = 0x00; //(0x01&0xFF);           // Writing 8 LSB bits of ON count 
        writeBuffer[2] = 0x00; //((0x99&0xF00)>>8);
        writeBuffer[3] = (4095&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[4] = ((4095&0xF00)>>8);

        I2C2_MasterWrite(writeBuffer, 5, PCA9685_WHITE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING); 
        
        writeBuffer[0] = ((2*4)+6);                     // Control register set to address LEDn_ON_L 
        writeBuffer[1] = 0x00; //(0x01&0xFF);           // Writing 8 LSB bits of ON count 
        writeBuffer[2] = 0x00; //((0x99&0xF00)>>8);
        writeBuffer[3] = (4095&0xFF);                   // Writing 8 LSB bits of ON count 
        writeBuffer[4] = ((4095&0xF00)>>8);

        I2C2_MasterWrite(writeBuffer, 5, PCA9685_BLUE_ADDRESS, &status);
        while (status == I2C2_MESSAGE_PENDING);
}





