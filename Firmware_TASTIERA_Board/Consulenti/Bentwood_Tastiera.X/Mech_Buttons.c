/**
  Mechanical Buttons Driver File

  @Company
    K-TRONIC

  @File Name
    Mech_Buttons.c

  @Summary
    This is the C file for manage the 10 Mechanical Buttons

  @Description
    This C file provides APIs for manage the 10 Mechanical Buttons.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/PCA9685.h"
#include "mcc_generated_files/pin_manager.h"
#include <xc.h>
#include "mcc_generated_files/Tip_And_Tricks.h"
#include "mcc_generated_files/tmr0.h"
#include "mcc_generated_files/eusart1.h"


#define TASTO_M01_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define TASTO_M02_SetDigitalInput()    do { TRISDbits.TRISD6 = 0; } while(0)
#define NUM_BUTTMEC 10

unsigned char flag_enable_Mec = 'b';
unsigned char* buffer;
unsigned char buffer_mec[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
unsigned short delay = 32000;

void Mech_Buttons_Get_Port(void)
{
    //unsigned char* buffer_mec;
    unsigned char check =0;
    
    buffer_mec[3] = 0x00;
    buffer_mec[4] = 0x00;
    
    if (flag_enable_Mec =='a'){
        if(TASTO_M01_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('0');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[4] = buffer_mec[4] | 0x01;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xFE;}

        if(TASTO_M02_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('1');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[4] = buffer_mec[4] | 0x02;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xFD;}    

        if(TASTO_M03_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('2');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();            
            buffer_mec[4] = buffer_mec[4] | 0x04;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xFB;}

        if(TASTO_M04_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('3');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[4] = buffer_mec[4] | 0x08;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xF7;}

        if(TASTO_M05_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('4');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[4] = buffer_mec[4] | 0x10;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xEF;}

        if(TASTO_M06_GetValue()==0) {
    //        printf("\n\r");
           // EUSART1_Write('5');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[4] = buffer_mec[4] | 0x20;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xDF;}    

        if(TASTO_M07_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('6');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[4] = buffer_mec[4] | 0x40;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0xBF;}

        if(TASTO_M08_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('7');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[4] = buffer_mec[4] | 0x80;
            check =1;
        } else{buffer_mec[4] = buffer_mec[4] & 0x7F;}

        if(TASTO_M09_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('8');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[3] = buffer_mec[3] | 0x01;
            check =1;
        } else{buffer_mec[3] = buffer_mec[3] & 0xFE;}

        if(TASTO_M010_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('9');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[3] = buffer_mec[3] | 0x02;
            check =1;
        } else{buffer_mec[3] = buffer_mec[3] & 0xFB;}   
        
       
        //if(!check)EUSART1_Write('\0');
    }
    
    if (flag_enable_Mec =='3'){    
        if(TASTO_M04_GetValue()==0) {
    //        printf("\n\r");
            //EUSART1_Write('3');
//            TMR0_GetRegValues(delay);
//            TMR0_StartTimer();
//            INTCONbits.TMR0IF = 0;
//            while(!(INTCONbits.TMR0IF));
//            TMR0_StopTimer();
            buffer_mec[4] = buffer_mec[4] | 0x08;
            check =1;
        } else{}
        
        //if(!check)EUSART1_Write('\0');
        
    } else{}  
    
    if(check & flag_enable_Mec =='a'){
        buffer_mec[0] = 'A';
        buffer_mec[1] = 'W';
        buffer_mec[2] = 0xA;
        buffer_mec[5] = buffer_mec[0]+buffer_mec[1]+buffer_mec[2]+buffer_mec[4]+buffer_mec[3]+0x20;
        buffer = buffer_mec;
        EUSART1_BufferWrite(buffer,6);
    }
}





