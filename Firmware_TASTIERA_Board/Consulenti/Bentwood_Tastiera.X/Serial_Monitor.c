/**
  Serial Monitor Driver File

  @Company
    K-TRONIC

  @File Name
    Serial_Monitor.c

  @Summary
    This is the C file regarding the management of serial monitor

  @Description
    This C file provides APIs for the management of serial monitor.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/PCA9685.h"
#include "mcc_generated_files/Serial_Monitor.h"
#include "mcc_generated_files/Tip_And_Tricks.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/CTMU.h"
#include "mcc_generated_files/Mech_Buttons.h"
#include <xc.h>


#define NUM_PWM_LEDs 10 //numbers of PWM that you want apply
                       
#define NUM_DATA_SET_CTMU 10 //numbers of data that you want to set
                            //consider also flag to write data in EE

//#define PARAM_PWM_LEDs 4
unsigned char Serial_Config_Variable_1 = 0;
unsigned char Serial_Config_Variable_2 = 0;
unsigned char Serial_Index_LED = 0;
unsigned char Serial_Config_Command = 0;
unsigned char Serial_Config_Driver = 0;
unsigned char Serial_Config_Start = 0;
unsigned char Parameters_LEDs[NUM_PWM_LEDs];
unsigned char Multi_PWMLeds_enabled = 0;
unsigned char count_data_PWM = 0;
unsigned char data_CTMU[NUM_DATA_SET_CTMU];
unsigned char count_data_CTMU = 0;
unsigned char hys_CTMU = 0;
unsigned char val_hys = 0;

unsigned char* regs;

unsigned int PWM_1 = 0;
unsigned int PWM_2 = 0;
unsigned char PWM_1_LOW = 0;
unsigned char PWM_1_HIGH = 0;
unsigned char PWM_2_LOW = 0;
unsigned char PWM_2_HIGH = 0;


//unsigned short Vred = 0;
//unsigned short delay = 5000; 

void Serial_Monitor(void)
{
	char i = 0;
    char error = 'x';
    
    static unsigned char SM_State = 0;
    static unsigned char RXcode_1 = 0;
    static unsigned char RXcode_2 = 0;
    static unsigned char RXcode_3 = 0;
    static unsigned char RXcode_4 = 0;
    static unsigned char Index = 0;
    
    
    
    i = EUSART1_Read();
    
		switch(SM_State) {

			case 0:                
				switch(i) 
                {
					case 'G':
                        //EUSART1_Write(i);
                        //printf("\n\r echo --> '%c'",i);      // Da commentare, solo per debug                   
                        SM_State = 1;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Index = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        val_hys = 0;
						break;
                    
                    case 'N':
                        //EUSART1_Write(i);
                        //printf("\n\r echo --> '%c'",i);      // Da commentare, solo per debug                   
                        SM_State = 8;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Index = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        val_hys = 0;
						break; 
                        
                    case 'S':
                        //EUSART1_Write(i);
                        //printf("\n\r echo --> '%c'",i);      // Da commentare, solo per debug                   
                        SM_State = 10;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0; 
                        Index = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        val_hys = 0;
						break;
                    
                    case 'V':
                        //EUSART1_Write(i);
                        //printf("\n\r echo --> '%c'",i);      // Da commentare, solo per debug                   
                        SM_State = 1;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Multi_PWMLeds_enabled = 1;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        Index = 0;
                        val_hys = 0;
						break;
                        
                    case 'R':
                        //EUSART1_Write(i);
                        CTMU_Acquisition();                        
                    break; 
                        
                    case 'Q':
                        //EUSART1_Write(i);
                        CTMU_Print();
                    break;
                    
                    case 'K':
                        //EUSART1_Write(i);
                        CTMU_ReadMatrixEE();
                    break;
                        
                    case 'T':
                        //EUSART1_Write(i);
                        SM_State = 11;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0; 
                        Index = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        val_hys = 0;
                    break;       
                    
                    case 'J':
                        //EUSART1_Write(i);
                        CTMU_WriteMatrixEE();
                    break;                                        
                    
                    case 'M':
                        //EUSART1_Write(i);
                        SM_State = 14;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        val_hys = 0;
                        Index = 0;
						break;
                    
                    case 'C':
                        //EUSART1_Write(i);
                        SM_State = 15;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        val_hys = 0;
                        Index = 0;
						break; 
                    
                    case 'Z':
                        //EUSART1_Write(i);
                        SM_State = 16;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        val_hys = 0;
                        Index = 0;
                        //printf("\033[2J");
						break;
                    
                    case 'W':
                        //EUSART1_Write(i);
                        //printf("\n\r echo --> '%c'",i);      // Da commentare, solo per debug                   
                        //ShowVersion();
                        SM_State = 11;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 1;
                        val_hys = 0;
                        Index = 0;
						break;
                        
                    case '?':
                        //EUSART1_Write(i);
                        //printf("\n\r echo --> '%c'",i);      // Da commentare, solo per debug                   
                        ShowVersion();
                        SM_State = 0;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        val_hys = 0;
                        Index = 0;
						break;
                        
                    default:
                        //EUSART1_Write(error);
                        SM_State = 0;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        hys_CTMU = 0;
                        val_hys = 0;
                        Index = 0;
                        //printf("\n\r");
				} 
            break;

			case 1:
            
                if(i=='0' || i=='1' || i=='2' || i=='3' || i=='4' || i=='5' || i=='6' || i=='7' || i=='8' || i=='9' || i=='A' || i=='B' || i=='C' || i=='D' || i=='E' || i=='F')
                {
                //EUSART1_Write(i); 
				RXcode_1=i;
				SM_State=2;
				break;
                } else if (i=='U'){                    
                        //EUSART1_Write(i); 
                        SM_State=0;
                        LATCbits.LATC2 = 0;            // Daprevederlo come invio da parte del PIC24 pe abilitare i driver LED
                    
                        SM_State = 0;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Index = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        Serial_Config_Command=0;
                        Serial_Config_Driver=0;
                        Serial_Config_Start=0;
                        Serial_Config_Variable_1 = 0;
                        Serial_Config_Variable_2 = 0;
                    break;
                } else if (i=='Z') {
                        //EUSART1_Write(i);
                        SM_State=0;
                        LATCbits.LATC2 = 1;           // Assicura che i driver siano diasibilitati per evitare accensioni LED indesiderate
                                                      // prima che vengano configurati i due PCA9685 
                        SM_State = 0;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Index = 0;                    
                        Serial_Config_Command=0;
                        Serial_Config_Driver=0;
                        Serial_Config_Start=0;
                        Serial_Config_Variable_1=0;
                        Serial_Config_Variable_2=0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        //printf("\n\r");
                    break;
                } else{
                    //EUSART1_Write(error); 
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                }
            break;
           
			case 2:
                if(i=='0' || i=='1' || i=='2' || i=='3' || i=='4' || i=='5' || i=='6' || i=='7' || i=='8' || i=='9' || i=='A' || i=='B' || i=='C' || i=='D' || i=='E' || i=='F')
                {
                //EUSART1_Write(i);            
				RXcode_2=i;
                if(Multi_PWMLeds_enabled)
                {
                    Serial_Config_Variable_1 = ASCIItoHEX(RXcode_1,RXcode_2);

                    Parameters_LEDs[count_data_PWM]=Serial_Config_Variable_1;

                    count_data_PWM++;
                    
                    if(count_data_PWM >= NUM_PWM_LEDs){
                        SM_State=4;
                        Serial_Config_Command='y'; //generic value, unused in this case
                    }
                    else{
                        SM_State=1;
                    }
                }else{
                    SM_State=3;
                }
				break;
                } else 
                    {
                    //EUSART1_Write(error); 
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                    }
            break;
               
            case 3: 
                if(i=='i' || i=='l' || i=='m' || i=='p' || i=='q' || i=='o')
                {
                //EUSART1_Write(i);
                Serial_Config_Command=i;
                SM_State=4;
				break;
                } else 
                    {
                        //EUSART1_Write(error); 
                        SM_State = 0;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Index = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        Serial_Config_Command=0;
                        //printf("\n\r");
                    }                
            break;
            
            case 4: 
                if(i=='0' || i=='1')
                {
                //EUSART1_Write(i);
                Serial_Config_Driver=i;
                SM_State=5;
				break;
                } else 
                    {
                        //EUSART1_Write(error);
                        SM_State = 0;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Index = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        Serial_Config_Command=0;
                        Serial_Config_Driver=0;
                        //printf("\n\r");                    
                    }                
            break;
            
            case 5:                 
                switch(i) 
                {
					case 'H':
                        //EUSART1_Write(i);               
                        
                        if(Multi_PWMLeds_enabled){
                            Serial_Config_Start = 1;
                            
                        }else if(count_data_CTMU >= NUM_DATA_SET_CTMU){
                            if(data_CTMU[0]>0 & data_CTMU[0]<13)CTMU_SetCalibValue(data_CTMU[0],data_CTMU[1],data_CTMU[2],data_CTMU[3],data_CTMU[4],data_CTMU[5],data_CTMU[6],data_CTMU[7],data_CTMU[8],data_CTMU[9]);
                        }else if (hys_CTMU){
                            CTMU_SetHysteresis(val_hys);
                        }else{
                            
                            
                            Serial_Config_Variable_1 = ASCIItoHEX(RXcode_3,RXcode_4);

                            Serial_Config_Variable_2 = ASCIItoHEX(RXcode_1,RXcode_2);
                            
                            Serial_Index_LED = ASCIItoHEX(0,Index);
                            
                            Serial_Config_Start = 1;
                        }
                        SM_State = 0;
                        count_data_CTMU = 0;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Index = 0;                                             
						break;
                    
                    default:
                        //EUSART1_Write(error); 
                        SM_State = 0;
                        RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                        Index = 0;
                        Multi_PWMLeds_enabled = 0;
                        count_data_PWM= 0;
                        count_data_CTMU = 0;
                        Serial_Config_Command=0;
                        Serial_Config_Driver=0;
                        Serial_Config_Start=0;
                        //printf("\n\r");
				}
            break;
            
            
            case 6: 
            {       
//                    EUSART1_Write(i);
//                    LATCbits.LATC2 = 0;            // Daprevederlo come invio da parte del PIC24 pe abilitare i driver LED
//                    
//                    SM_State = 0;
//                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
//                    Index = 0;
//                    Multi_PWMLeds_enabled = 0;
//                    count_data_PWM= 0;
//                    count_data_CTMU = 0;
//                    Serial_Config_Command=0;
//                    Serial_Config_Driver=0;
//                    Serial_Config_Start=0;
//                    Serial_Config_Variable_1 = 0;
//                    Serial_Config_Variable_2 = 0;
                    //printf("\n\r");
                    break; 
            }
            
            case 7:    
            {
//                    EUSART1_Write(i);
//                    LATCbits.LATC2 = 1;           // Assicura che i driver siano diasibilitati per evitare accensioni LED indesiderate
//                                                // prima che vengano configurati i due PCA9685 
//                    SM_State = 0;
//                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
//                    Index = 0;                    
//                    Serial_Config_Command=0;
//                    Serial_Config_Driver=0;
//                    Serial_Config_Start=0;
//                    Serial_Config_Variable_1=0;
//                    Serial_Config_Variable_2=0;
//                    Multi_PWMLeds_enabled = 0;
//                    count_data_PWM= 0;
//                    count_data_CTMU = 0;
//                    //printf("\n\r");
                    break;
            }       
            
            case 8:
                if(i=='0' || i=='1' || i=='2' || i=='3' || i=='4' || i=='5' || i=='6' || i=='7' || i=='8' || i=='9' || i=='A' || i=='B' || i=='C' || i=='D' || i=='E' || i=='F')
                {
               //EUSART1_Write(i);
                RXcode_3=i;
                SM_State=9; 
				
				break;
                } else 
                    {
                    //EUSART1_Write(error);  
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");                   
                    }
            break;

            case 9:
                if(i=='0' || i=='1' || i=='2' || i=='3' || i=='4' || i=='5' || i=='6' || i=='7' || i=='8' || i=='9' || i=='A' || i=='B' || i=='C' || i=='D' || i=='E' || i=='F')
                {
                //EUSART1_Write(i);
				RXcode_4=i;
				SM_State=1;

                
				break;
                } else 
                    {
                    //EUSART1_Write(error); 
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                    }
            break;            
            
            
            case 10:
                if(i=='0' || i=='1' || i=='2' || i=='3' || i=='4' || i=='5' || i=='6' || i=='7' || i=='8' || i=='9' || i=='A' || i=='B' || i=='C' || i=='D' || i=='E' || i=='F')
                {
                //EUSART1_Write(i);
				Index=i;
				SM_State=8;
				break;
                } else 
                    {
                    //EUSART1_Write(error);
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                    }
            break;
            
            case 11:
                if(i=='0' || i=='1' || i=='2' || i=='3' || i=='4' || i=='5' || i=='6' || i=='7' || i=='8' || i=='9' || i=='A' || i=='B' || i=='C' || i=='D' || i=='E' || i=='F')
                {
                //EUSART1_Write(i);
				RXcode_1=i;
				SM_State=12;
				break;
                } else 
                    {
                    //EUSART1_Write(error);
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                    }
            break;
            
            case 12:
                if(i=='0' || i=='1' || i=='2' || i=='3' || i=='4' || i=='5' || i=='6' || i=='7' || i=='8' || i=='9' || i=='A' || i=='B' || i=='C' || i=='D' || i=='E' || i=='F')
                {
                //EUSART1_Write(i);
				RXcode_2=i;
                if (hys_CTMU){
                    val_hys=ASCIItoHEX(RXcode_1,RXcode_2);
                }
                else{
                    data_CTMU[count_data_CTMU] = ASCIItoHEX(RXcode_1,RXcode_2);
                    count_data_CTMU++;
                }
                
                if (count_data_CTMU >= NUM_DATA_SET_CTMU-1){
                    SM_State = 13;
                    //for(i=0;i<NUM_DATA_SET_CTMU; i++)printf(" %u", data_CTMU[i]);
                }else if(hys_CTMU){
                    SM_State=5;
                    
                }else{
                    SM_State=11;
                }
                
				break;
                } else 
                    {
                    //EUSART1_Write(error);
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                    }
            break;
            
            case 13: //EE
                if(i=='0' || i=='1')
                {
                //EUSART1_Write(i);
 
                data_CTMU[count_data_CTMU] = i;
                count_data_CTMU++;

                
                
                SM_State = 5;

				break;
                } else 
                    {
                    //EUSART1_Write(error);
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                    }
            break;
            
            case 14:
                if(i=='a' || i=='b' || i=='3')
                {
                //EUSART1_Write(i);
                flag_enable_Mec = i;
                SM_State = 0;
				break;
                } else 
                    {
                    //EUSART1_Write(error);
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                    }
            break;
            
            case 15:
                if(i=='0' || i=='1')
                {
                //EUSART1_Write(i);
                flag_enable_CapSense = i;
                SM_State = 0;
				break;
                } else 
                    {
                    //EUSART1_Write(error);
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                    }
            break;
            
            case 16:
                if(i=='0' || i=='1')
                {
                //EUSART1_Write(i);
                flag_analysis_CapSense = i;
                SM_State = 0;
				break;
                } else 
                    {
                    //EUSART1_Write(error);
                    SM_State = 0;
                    RXcode_1 = RXcode_2 = RXcode_3 = RXcode_4 = 0;
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                    count_data_CTMU = 0;
                    Index = 0;
                    //printf("\n\r");
                    }
            break;
 
        }
        
}



void Serial_Config_PCA9685(void){
    unsigned char row;
    
    //
    if (Serial_Config_Command!=0 && Serial_Config_Driver!=0 && Serial_Config_Start!=0){
        switch(Serial_Config_Driver) {
            case '0':
                if(Multi_PWMLeds_enabled) {
                    //Matrix so defined:
                    //Parameters_LEDs[row][0]=Serial_Config_Variable_1;
                    //Parameters_LEDs[row][1]=Serial_Config_Variable_2;    
                    //PrintMatrix(Parameters_LEDs,NUM_PWM_LEDs, PARAM_PWM_LEDs);
                    
                    for(row=0; row<NUM_PWM_LEDs; row++){

                    PWM_1 = (4095 /100) * (Parameters_LEDs[row]);
                    
                    PWM_1_HIGH = (PWM_1>>8) & 0xFF;
                    PWM_1_LOW = (unsigned char) PWM_1;
                    
                    PCA9685_SET_LED_BLUE_ON(row,PWM_1_HIGH,PWM_1_LOW);
                    
                    PWM_1 = 0;
                    PWM_1_LOW = 0;
                    PWM_1_HIGH = 0;

                    }
                }
                else
                {
                    if(Serial_Config_Command == 'i')
                    {
                        PCA9685_SET_MODE1(Serial_Config_Command, PCA9685_BLUE_ADDRESS);
                    } else {}

                    if(Serial_Config_Command == 'l')
                    {
                        PCA9685_SET_MODE2(Serial_Config_Command, PCA9685_BLUE_ADDRESS);
                    } else {}

                    if(Serial_Config_Command == 'm')
                    {
                        PCA9685_SET_PRESCALE(Serial_Config_Command, PCA9685_BLUE_ADDRESS);
                    } else {}

                    if(Serial_Config_Command == 'p')
                    {
                        PCA9685_ALL_LED_BLUE_OFF(Serial_Config_Variable_1, Serial_Config_Variable_2);
                    } else {}

                    if(Serial_Config_Command == 'q')
                    {
                        PCA9685_ALL_LED_BLUE_ON(Serial_Config_Variable_1, Serial_Config_Variable_2);
                    } else {}

                    if(Serial_Config_Command == 'o')
                    {                
                        
                        if(Serial_Index_LED==1) {
                            Serial_Index_LED=2;
                        } else if(Serial_Index_LED==2) {
                            Serial_Index_LED=1;
                        } else if(Serial_Index_LED==6) {
                            Serial_Index_LED=8;
                        } else if(Serial_Index_LED==7) {
                            Serial_Index_LED=6;
                        } else if(Serial_Index_LED==9) {
                            //Serial_Index_LED=7;
                        } else if(Serial_Index_LED==8) {
                            Serial_Index_LED=7;
                        } else{}
                        
                        PCA9685_SET_LED_BLUE_ON(Serial_Index_LED, Serial_Config_Variable_1, Serial_Config_Variable_2);
                    
                    } else {}
                }
                
                Serial_Config_Command=0;
                Serial_Config_Driver=0;
                Serial_Config_Start=0;
            break;
            
            case '1':
                if(Multi_PWMLeds_enabled) {
                    //Matrix so defined:
                    //Parameters_LEDs[row][0]=Serial_Config_Variable_2;
                    //Parameters_LEDs[row][1]=Serial_Config_Variable_1;
                    //PrintMatrix(Parameters_LEDs,NUM_PWM_LEDs, PARAM_PWM_LEDs);                  
                    
                    for(row=0; row<NUM_PWM_LEDs; row++){

                    PWM_1 = (4095 /100) * (Parameters_LEDs[row]);
                    
                    PWM_1_HIGH = (PWM_1>>8) & 0xFF;
                    PWM_1_LOW = (unsigned char) PWM_1;                
                    
                    PCA9685_SET_LED_WHITE_ON(row, PWM_1_HIGH, PWM_1_LOW);

                    PWM_1 = 0;
                    PWM_1_LOW = 0;
                    PWM_1_HIGH = 0;
       
                    }
                    Multi_PWMLeds_enabled = 0;
                    count_data_PWM= 0;
                }
                else
                {
                    if(Serial_Config_Command == 'i')
                    {
                        PCA9685_SET_MODE1(Serial_Config_Command, PCA9685_WHITE_ADDRESS);
                    } else {}

                    if(Serial_Config_Command == 'l')
                    {
                        PCA9685_SET_MODE2(Serial_Config_Command, PCA9685_WHITE_ADDRESS);
                    } else {}

                    if(Serial_Config_Command == 'm')
                    {
                        PCA9685_SET_PRESCALE(Serial_Config_Command, PCA9685_WHITE_ADDRESS);
                    } else {}

                    if(Serial_Config_Command == 'p')
                    {
                        //ton
                        PCA9685_ALL_LED_WHITE_OFF(Serial_Config_Variable_1, Serial_Config_Variable_2);
                    } else {}

                    if(Serial_Config_Command == 'q')
                    {
                        PCA9685_ALL_LED_WHITE_ON(Serial_Config_Variable_1, Serial_Config_Variable_2);
                    } else {}

                    if(Serial_Config_Command == 'o')
                    {
                        
                        if(Serial_Index_LED==1) {
                            Serial_Index_LED=2;
                        } else if(Serial_Index_LED==2) {
                            Serial_Index_LED=1;
                        } else if(Serial_Index_LED==6) {
                            Serial_Index_LED=8;
                        } else if(Serial_Index_LED==7) {
                            Serial_Index_LED=6;
                        } else if(Serial_Index_LED==9) {
                            //Serial_Index_LED=7;
                        } else if(Serial_Index_LED==8) {
                            Serial_Index_LED=7;
                        } else{}
                        
                        PCA9685_SET_LED_WHITE_ON(Serial_Index_LED, Serial_Config_Variable_1, Serial_Config_Variable_2);
                    } else {}  
                }
            Serial_Config_Command=0;
            Serial_Config_Driver=0;
            Serial_Config_Start=0;    
            break;

            default:
                
                Serial_Config_Variable_1 = 0;
                Serial_Config_Variable_2 = 0;
                Serial_Index_LED = 0;
                Serial_Config_Command=0;
                Serial_Config_Driver=0;
                Serial_Config_Start=0;
        }
    }
    
}


void ShowVersion(void)
{
    int i;
    char version[]="$Id:BENTWOOD (C) K-TRONIC s.r.l.;  Ver 1.0.0 - 15.05.19\xf";
    
    for(i=0;i<sizeof(version);i++)
    {
        EUSART1_Write(version[i]);
    }

}