/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set TASTO_CAP_06 aliases
#define TASTO_CAP_06_TRIS                 TRISAbits.TRISA0
#define TASTO_CAP_06_LAT                  LATAbits.LATA0
#define TASTO_CAP_06_PORT                 PORTAbits.RA0
#define TASTO_CAP_06_ANS                  ANSELAbits.ANSA0
#define TASTO_CAP_06_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define TASTO_CAP_06_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define TASTO_CAP_06_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define TASTO_CAP_06_GetValue()           PORTAbits.RA0
#define TASTO_CAP_06_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define TASTO_CAP_06_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define TASTO_CAP_06_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define TASTO_CAP_06_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set TASTO_CAP_07 aliases
#define TASTO_CAP_07_TRIS                 TRISAbits.TRISA1
#define TASTO_CAP_07_LAT                  LATAbits.LATA1
#define TASTO_CAP_07_PORT                 PORTAbits.RA1
#define TASTO_CAP_07_ANS                  ANSELAbits.ANSA1
#define TASTO_CAP_07_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define TASTO_CAP_07_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define TASTO_CAP_07_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define TASTO_CAP_07_GetValue()           PORTAbits.RA1
#define TASTO_CAP_07_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define TASTO_CAP_07_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define TASTO_CAP_07_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define TASTO_CAP_07_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set TASTO_CAP_08 aliases
#define TASTO_CAP_08_TRIS                 TRISAbits.TRISA2
#define TASTO_CAP_08_LAT                  LATAbits.LATA2
#define TASTO_CAP_08_PORT                 PORTAbits.RA2
#define TASTO_CAP_08_ANS                  ANSELAbits.ANSA2
#define TASTO_CAP_08_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define TASTO_CAP_08_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define TASTO_CAP_08_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define TASTO_CAP_08_GetValue()           PORTAbits.RA2
#define TASTO_CAP_08_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define TASTO_CAP_08_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define TASTO_CAP_08_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define TASTO_CAP_08_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set TASTO_CAP_09 aliases
#define TASTO_CAP_09_TRIS                 TRISAbits.TRISA3
#define TASTO_CAP_09_LAT                  LATAbits.LATA3
#define TASTO_CAP_09_PORT                 PORTAbits.RA3
#define TASTO_CAP_09_ANS                  ANSELAbits.ANSA3
#define TASTO_CAP_09_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define TASTO_CAP_09_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define TASTO_CAP_09_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define TASTO_CAP_09_GetValue()           PORTAbits.RA3
#define TASTO_CAP_09_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define TASTO_CAP_09_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define TASTO_CAP_09_SetAnalogMode()      do { ANSELAbits.ANSA3 = 1; } while(0)
#define TASTO_CAP_09_SetDigitalMode()     do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set TASTO_M09 aliases
#define TASTO_M09_TRIS                 TRISAbits.TRISA4
#define TASTO_M09_LAT                  LATAbits.LATA4
#define TASTO_M09_PORT                 PORTAbits.RA4
#define TASTO_M09_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define TASTO_M09_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define TASTO_M09_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define TASTO_M09_GetValue()           PORTAbits.RA4
#define TASTO_M09_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define TASTO_M09_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)

// get/set TASTO_CAP_12 aliases
#define TASTO_CAP_12_TRIS                 TRISBbits.TRISB0
#define TASTO_CAP_12_LAT                  LATBbits.LATB0
#define TASTO_CAP_12_PORT                 PORTBbits.RB0
#define TASTO_CAP_12_WPU                  WPUBbits.WPUB0
#define TASTO_CAP_12_ANS                  ANSELBbits.ANSB0
#define TASTO_CAP_12_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define TASTO_CAP_12_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define TASTO_CAP_12_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define TASTO_CAP_12_GetValue()           PORTBbits.RB0
#define TASTO_CAP_12_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define TASTO_CAP_12_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define TASTO_CAP_12_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define TASTO_CAP_12_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define TASTO_CAP_12_SetAnalogMode()      do { ANSELBbits.ANSB0 = 1; } while(0)
#define TASTO_CAP_12_SetDigitalMode()     do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set TASTO_CAP_01 aliases
#define TASTO_CAP_01_TRIS                 TRISBbits.TRISB1
#define TASTO_CAP_01_LAT                  LATBbits.LATB1
#define TASTO_CAP_01_PORT                 PORTBbits.RB1
#define TASTO_CAP_01_WPU                  WPUBbits.WPUB1
#define TASTO_CAP_01_ANS                  ANSELBbits.ANSB1
#define TASTO_CAP_01_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define TASTO_CAP_01_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define TASTO_CAP_01_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define TASTO_CAP_01_GetValue()           PORTBbits.RB1
#define TASTO_CAP_01_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define TASTO_CAP_01_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define TASTO_CAP_01_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define TASTO_CAP_01_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define TASTO_CAP_01_SetAnalogMode()      do { ANSELBbits.ANSB1 = 1; } while(0)
#define TASTO_CAP_01_SetDigitalMode()     do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set TASTO_CAP_02 aliases
#define TASTO_CAP_02_TRIS                 TRISBbits.TRISB2
#define TASTO_CAP_02_LAT                  LATBbits.LATB2
#define TASTO_CAP_02_PORT                 PORTBbits.RB2
#define TASTO_CAP_02_WPU                  WPUBbits.WPUB2
#define TASTO_CAP_02_ANS                  ANSELBbits.ANSB2
#define TASTO_CAP_02_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define TASTO_CAP_02_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define TASTO_CAP_02_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define TASTO_CAP_02_GetValue()           PORTBbits.RB2
#define TASTO_CAP_02_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define TASTO_CAP_02_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define TASTO_CAP_02_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define TASTO_CAP_02_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define TASTO_CAP_02_SetAnalogMode()      do { ANSELBbits.ANSB2 = 1; } while(0)
#define TASTO_CAP_02_SetDigitalMode()     do { ANSELBbits.ANSB2 = 0; } while(0)

// get/set TASTO_CAP_03 aliases
#define TASTO_CAP_03_TRIS                 TRISBbits.TRISB3
#define TASTO_CAP_03_LAT                  LATBbits.LATB3
#define TASTO_CAP_03_PORT                 PORTBbits.RB3
#define TASTO_CAP_03_WPU                  WPUBbits.WPUB3
#define TASTO_CAP_03_ANS                  ANSELBbits.ANSB3
#define TASTO_CAP_03_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define TASTO_CAP_03_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define TASTO_CAP_03_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define TASTO_CAP_03_GetValue()           PORTBbits.RB3
#define TASTO_CAP_03_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define TASTO_CAP_03_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define TASTO_CAP_03_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define TASTO_CAP_03_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define TASTO_CAP_03_SetAnalogMode()      do { ANSELBbits.ANSB3 = 1; } while(0)
#define TASTO_CAP_03_SetDigitalMode()     do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set TASTO_CAP_04 aliases
#define TASTO_CAP_04_TRIS                 TRISBbits.TRISB4
#define TASTO_CAP_04_LAT                  LATBbits.LATB4
#define TASTO_CAP_04_PORT                 PORTBbits.RB4
#define TASTO_CAP_04_WPU                  WPUBbits.WPUB4
#define TASTO_CAP_04_ANS                  ANSELBbits.ANSB4
#define TASTO_CAP_04_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define TASTO_CAP_04_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define TASTO_CAP_04_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define TASTO_CAP_04_GetValue()           PORTBbits.RB4
#define TASTO_CAP_04_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define TASTO_CAP_04_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define TASTO_CAP_04_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define TASTO_CAP_04_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define TASTO_CAP_04_SetAnalogMode()      do { ANSELBbits.ANSB4 = 1; } while(0)
#define TASTO_CAP_04_SetDigitalMode()     do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set TASTO_CAP_05 aliases
#define TASTO_CAP_05_TRIS                 TRISBbits.TRISB5
#define TASTO_CAP_05_LAT                  LATBbits.LATB5
#define TASTO_CAP_05_PORT                 PORTBbits.RB5
#define TASTO_CAP_05_WPU                  WPUBbits.WPUB5
#define TASTO_CAP_05_ANS                  ANSELBbits.ANSB5
#define TASTO_CAP_05_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define TASTO_CAP_05_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define TASTO_CAP_05_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define TASTO_CAP_05_GetValue()           PORTBbits.RB5
#define TASTO_CAP_05_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define TASTO_CAP_05_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define TASTO_CAP_05_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define TASTO_CAP_05_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define TASTO_CAP_05_SetAnalogMode()      do { ANSELBbits.ANSB5 = 1; } while(0)
#define TASTO_CAP_05_SetDigitalMode()     do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set TASTO_M07 aliases
#define TASTO_M07_TRIS                 TRISBbits.TRISB6
#define TASTO_M07_LAT                  LATBbits.LATB6
#define TASTO_M07_PORT                 PORTBbits.RB6
#define TASTO_M07_WPU                  WPUBbits.WPUB6
#define TASTO_M07_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define TASTO_M07_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define TASTO_M07_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define TASTO_M07_GetValue()           PORTBbits.RB6
#define TASTO_M07_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define TASTO_M07_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define TASTO_M07_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define TASTO_M07_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)

// get/set TASTO_M08 aliases
#define TASTO_M08_TRIS                 TRISBbits.TRISB7
#define TASTO_M08_LAT                  LATBbits.LATB7
#define TASTO_M08_PORT                 PORTBbits.RB7
#define TASTO_M08_WPU                  WPUBbits.WPUB7
#define TASTO_M08_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define TASTO_M08_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define TASTO_M08_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define TASTO_M08_GetValue()           PORTBbits.RB7
#define TASTO_M08_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define TASTO_M08_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define TASTO_M08_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define TASTO_M08_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)

// get/set STATUS_LED aliases
#define STATUS_LED_TRIS                 TRISCbits.TRISC0
#define STATUS_LED_LAT                  LATCbits.LATC0
#define STATUS_LED_PORT                 PORTCbits.RC0
#define STATUS_LED_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define STATUS_LED_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define STATUS_LED_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define STATUS_LED_GetValue()           PORTCbits.RC0
#define STATUS_LED_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define STATUS_LED_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)

// get/set TASTO_M010 aliases
#define TASTO_M010_TRIS                 TRISCbits.TRISC1
#define TASTO_M010_LAT                  LATCbits.LATC1
#define TASTO_M010_PORT                 PORTCbits.RC1
#define TASTO_M010_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define TASTO_M010_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define TASTO_M010_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define TASTO_M010_GetValue()           PORTCbits.RC1
#define TASTO_M010_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define TASTO_M010_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)

// get/set OE_Neg aliases
#define OE_Neg_TRIS                 TRISCbits.TRISC2
#define OE_Neg_LAT                  LATCbits.LATC2
#define OE_Neg_PORT                 PORTCbits.RC2
#define OE_Neg_ANS                  ANSELCbits.ANSC2
#define OE_Neg_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define OE_Neg_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define OE_Neg_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define OE_Neg_GetValue()           PORTCbits.RC2
#define OE_Neg_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define OE_Neg_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define OE_Neg_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define OE_Neg_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set RC6 procedures
#define RC6_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define RC6_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define RC6_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define RC6_GetValue()              PORTCbits.RC6
#define RC6_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define RC6_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define RC6_SetAnalogMode()         do { ANSELCbits.ANSC6 = 1; } while(0)
#define RC6_SetDigitalMode()        do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set RC7 procedures
#define RC7_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define RC7_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define RC7_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RC7_GetValue()              PORTCbits.RC7
#define RC7_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define RC7_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define RC7_SetAnalogMode()         do { ANSELCbits.ANSC7 = 1; } while(0)
#define RC7_SetDigitalMode()        do { ANSELCbits.ANSC7 = 0; } while(0)

// get/set SCL2 aliases
#define SCL2_TRIS                 TRISDbits.TRISD0
#define SCL2_LAT                  LATDbits.LATD0
#define SCL2_PORT                 PORTDbits.RD0
#define SCL2_ANS                  ANSELDbits.ANSD0
#define SCL2_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define SCL2_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define SCL2_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define SCL2_GetValue()           PORTDbits.RD0
#define SCL2_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define SCL2_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define SCL2_SetAnalogMode()      do { ANSELDbits.ANSD0 = 1; } while(0)
#define SCL2_SetDigitalMode()     do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set SDA2 aliases
#define SDA2_TRIS                 TRISDbits.TRISD1
#define SDA2_LAT                  LATDbits.LATD1
#define SDA2_PORT                 PORTDbits.RD1
#define SDA2_ANS                  ANSELDbits.ANSD1
#define SDA2_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define SDA2_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define SDA2_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define SDA2_GetValue()           PORTDbits.RD1
#define SDA2_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define SDA2_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define SDA2_SetAnalogMode()      do { ANSELDbits.ANSD1 = 1; } while(0)
#define SDA2_SetDigitalMode()     do { ANSELDbits.ANSD1 = 0; } while(0)

// get/set TASTO_M06 aliases
#define TASTO_M06_TRIS                 TRISDbits.TRISD2
#define TASTO_M06_LAT                  LATDbits.LATD2
#define TASTO_M06_PORT                 PORTDbits.RD2
#define TASTO_M06_ANS                  ANSELDbits.ANSD2
#define TASTO_M06_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define TASTO_M06_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define TASTO_M06_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define TASTO_M06_GetValue()           PORTDbits.RD2
#define TASTO_M06_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define TASTO_M06_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define TASTO_M06_SetAnalogMode()      do { ANSELDbits.ANSD2 = 1; } while(0)
#define TASTO_M06_SetDigitalMode()     do { ANSELDbits.ANSD2 = 0; } while(0)

// get/set TASTO_M05 aliases
#define TASTO_M05_TRIS                 TRISDbits.TRISD3
#define TASTO_M05_LAT                  LATDbits.LATD3
#define TASTO_M05_PORT                 PORTDbits.RD3
#define TASTO_M05_ANS                  ANSELDbits.ANSD3
#define TASTO_M05_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define TASTO_M05_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define TASTO_M05_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define TASTO_M05_GetValue()           PORTDbits.RD3
#define TASTO_M05_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define TASTO_M05_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define TASTO_M05_SetAnalogMode()      do { ANSELDbits.ANSD3 = 1; } while(0)
#define TASTO_M05_SetDigitalMode()     do { ANSELDbits.ANSD3 = 0; } while(0)

// get/set TASTO_M04 aliases
#define TASTO_M04_TRIS                 TRISDbits.TRISD4
#define TASTO_M04_LAT                  LATDbits.LATD4
#define TASTO_M04_PORT                 PORTDbits.RD4
#define TASTO_M04_ANS                  ANSELDbits.ANSD4
#define TASTO_M04_SetHigh()            do { LATDbits.LATD4 = 1; } while(0)
#define TASTO_M04_SetLow()             do { LATDbits.LATD4 = 0; } while(0)
#define TASTO_M04_Toggle()             do { LATDbits.LATD4 = ~LATDbits.LATD4; } while(0)
#define TASTO_M04_GetValue()           PORTDbits.RD4
#define TASTO_M04_SetDigitalInput()    do { TRISDbits.TRISD4 = 1; } while(0)
#define TASTO_M04_SetDigitalOutput()   do { TRISDbits.TRISD4 = 0; } while(0)
#define TASTO_M04_SetAnalogMode()      do { ANSELDbits.ANSD4 = 1; } while(0)
#define TASTO_M04_SetDigitalMode()     do { ANSELDbits.ANSD4 = 0; } while(0)

// get/set TASTO_M03 aliases
#define TASTO_M03_TRIS                 TRISDbits.TRISD5
#define TASTO_M03_LAT                  LATDbits.LATD5
#define TASTO_M03_PORT                 PORTDbits.RD5
#define TASTO_M03_ANS                  ANSELDbits.ANSD5
#define TASTO_M03_SetHigh()            do { LATDbits.LATD5 = 1; } while(0)
#define TASTO_M03_SetLow()             do { LATDbits.LATD5 = 0; } while(0)
#define TASTO_M03_Toggle()             do { LATDbits.LATD5 = ~LATDbits.LATD5; } while(0)
#define TASTO_M03_GetValue()           PORTDbits.RD5
#define TASTO_M03_SetDigitalInput()    do { TRISDbits.TRISD5 = 1; } while(0)
#define TASTO_M03_SetDigitalOutput()   do { TRISDbits.TRISD5 = 0; } while(0)
#define TASTO_M03_SetAnalogMode()      do { ANSELDbits.ANSD5 = 1; } while(0)
#define TASTO_M03_SetDigitalMode()     do { ANSELDbits.ANSD5 = 0; } while(0)

// get/set TASTO_M02 aliases
#define TASTO_M02_TRIS                 TRISDbits.TRISD6
#define TASTO_M02_LAT                  LATDbits.LATD6
#define TASTO_M02_PORT                 PORTDbits.RD6
#define TASTO_M02_ANS                  ANSELDbits.ANSD6
#define TASTO_M02_SetHigh()            do { LATDbits.LATD6 = 1; } while(0)
#define TASTO_M02_SetLow()             do { LATDbits.LATD6 = 0; } while(0)
#define TASTO_M02_Toggle()             do { LATDbits.LATD6 = ~LATDbits.LATD6; } while(0)
#define TASTO_M02_GetValue()           PORTDbits.RD6
#define TASTO_M02_SetDigitalInput()    do { TRISDbits.TRISD6 = 1; } while(0)
#define TASTO_M02_SetDigitalOutput()   do { TRISDbits.TRISD6 = 0; } while(0)
#define TASTO_M02_SetAnalogMode()      do { ANSELDbits.ANSD6 = 1; } while(0)
#define TASTO_M02_SetDigitalMode()     do { ANSELDbits.ANSD6 = 0; } while(0)

// get/set TASTO_M01 aliases
#define TASTO_M01_TRIS                 TRISDbits.TRISD7
#define TASTO_M01_LAT                  LATDbits.LATD7
#define TASTO_M01_PORT                 PORTDbits.RD7
#define TASTO_M01_ANS                  ANSELDbits.ANSD7
#define TASTO_M01_SetHigh()            do { LATDbits.LATD7 = 1; } while(0)
#define TASTO_M01_SetLow()             do { LATDbits.LATD7 = 0; } while(0)
#define TASTO_M01_Toggle()             do { LATDbits.LATD7 = ~LATDbits.LATD7; } while(0)
#define TASTO_M01_GetValue()           PORTDbits.RD7
#define TASTO_M01_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define TASTO_M01_SetDigitalOutput()   do { TRISDbits.TRISD7 = 0; } while(0)
#define TASTO_M01_SetAnalogMode()      do { ANSELDbits.ANSD7 = 1; } while(0)
#define TASTO_M01_SetDigitalMode()     do { ANSELDbits.ANSD7 = 0; } while(0)

// get/set TASTO_CAP_10 aliases
#define TASTO_CAP_10_TRIS                 TRISEbits.TRISE0
#define TASTO_CAP_10_LAT                  LATEbits.LATE0
#define TASTO_CAP_10_PORT                 PORTEbits.RE0
#define TASTO_CAP_10_ANS                  ANSELEbits.ANSE0
#define TASTO_CAP_10_SetHigh()            do { LATEbits.LATE0 = 1; } while(0)
#define TASTO_CAP_10_SetLow()             do { LATEbits.LATE0 = 0; } while(0)
#define TASTO_CAP_10_Toggle()             do { LATEbits.LATE0 = ~LATEbits.LATE0; } while(0)
#define TASTO_CAP_10_GetValue()           PORTEbits.RE0
#define TASTO_CAP_10_SetDigitalInput()    do { TRISEbits.TRISE0 = 1; } while(0)
#define TASTO_CAP_10_SetDigitalOutput()   do { TRISEbits.TRISE0 = 0; } while(0)
#define TASTO_CAP_10_SetAnalogMode()      do { ANSELEbits.ANSE0 = 1; } while(0)
#define TASTO_CAP_10_SetDigitalMode()     do { ANSELEbits.ANSE0 = 0; } while(0)

// get/set TASTO_CAP_11 aliases
#define TASTO_CAP_11_TRIS                 TRISEbits.TRISE1
#define TASTO_CAP_11_LAT                  LATEbits.LATE1
#define TASTO_CAP_11_PORT                 PORTEbits.RE1
#define TASTO_CAP_11_ANS                  ANSELEbits.ANSE1
#define TASTO_CAP_11_SetHigh()            do { LATEbits.LATE1 = 1; } while(0)
#define TASTO_CAP_11_SetLow()             do { LATEbits.LATE1 = 0; } while(0)
#define TASTO_CAP_11_Toggle()             do { LATEbits.LATE1 = ~LATEbits.LATE1; } while(0)
#define TASTO_CAP_11_GetValue()           PORTEbits.RE1
#define TASTO_CAP_11_SetDigitalInput()    do { TRISEbits.TRISE1 = 1; } while(0)
#define TASTO_CAP_11_SetDigitalOutput()   do { TRISEbits.TRISE1 = 0; } while(0)
#define TASTO_CAP_11_SetAnalogMode()      do { ANSELEbits.ANSE1 = 1; } while(0)
#define TASTO_CAP_11_SetDigitalMode()     do { ANSELEbits.ANSE1 = 0; } while(0)

// get/set RE2 procedures
#define RE2_SetHigh()            do { LATEbits.LATE2 = 1; } while(0)
#define RE2_SetLow()             do { LATEbits.LATE2 = 0; } while(0)
#define RE2_Toggle()             do { LATEbits.LATE2 = ~LATEbits.LATE2; } while(0)
#define RE2_GetValue()              PORTEbits.RE2
#define RE2_SetDigitalInput()    do { TRISEbits.TRISE2 = 1; } while(0)
#define RE2_SetDigitalOutput()   do { TRISEbits.TRISE2 = 0; } while(0)
#define RE2_SetAnalogMode()         do { ANSELEbits.ANSE2 = 1; } while(0)
#define RE2_SetDigitalMode()        do { ANSELEbits.ANSE2 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/