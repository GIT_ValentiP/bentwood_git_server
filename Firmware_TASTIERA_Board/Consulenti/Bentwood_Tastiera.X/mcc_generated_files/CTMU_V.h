/**
  CTMU Header File

  @Company
    K-TRONIC

  @File Name
    CTMU_V.h

  @Summary
    This is the header file for manage the CTMU module

  @Description
    This header file provides APIs for manage the CTMU module.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/

//void CTMU_Initialize(void);
void CTMU_Inizialize_Port(void);
void CTMU_Inizialize_Caps(void);




