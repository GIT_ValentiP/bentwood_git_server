/**
  Tip and Tricks Header File

  @Company
    K-TRONIC

  @File Name
    Tip_And_Tricks.h

  @Summary
    This is the header file for general applications

  @Description
    This header file provides APIs for general applications.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC18F46K22
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


unsigned char ASCIItoHEX(unsigned char c1, unsigned char c2);  
void PrintMatrix(unsigned short** matrix, int row, int column);
unsigned char HEXtoRAW(unsigned char MSB, unsigned char LSB);
void Toggle_Led_Status(void);
void PrintArray(unsigned char* array, unsigned char num_element);



